import React, { PropTypes, findDOMNode } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import R from 'ramda';
import DocMeta from 'react-document-meta';
import stripHtml from '../utils/stripHtml';

import CategoryLinks from '../components/CategoryLinks';
import SocialButtons from '../components/SocialButtons';
import LikeButton from '../components/LikeButton';
import User from '../components/User';
import Feed from '../components/Feed';
import PrivacyDropdown from '../components/PrivacyDropdown';
import RelicFeedItem from '../components/Feed/RelicFeedItem';
import Preloader from '../components/Preloader';
import Datetime from '../components/Datetime';

import { loadCollection, setCollectionPrivacy, deleteCollection } from '../reducers/collection';
import { loadSharingCounter } from '../reducers/sharings';
import { setLike } from '../reducers/app';

const RELICS_PER_PAGE = 10;

@connect(
    (state) => ({
        permalink: state.router.params.permalink,
        currentUser: state.app.currentUser,
        collection: state.collection.collection,
        relics: state.collection.relics,
        state: state.states.collection,
        sharing: state[state.router.location.pathname],
        pathname: state.router.location.pathname
    }),
    (dispatch) => bindActionCreators({
        loadCollection,
        loadSharingCounter,
        setLike,
        setCollectionPrivacy
    }, dispatch)
)
export class Collection extends React.Component {
    componentWillMount() {
        this.props
            .loadCollection(this.props.permalink, RELICS_PER_PAGE, 1)
            .then(action => {
                if (action.error && action.payload.status === 404) {
                    this.props.history.pushState(null, '/not-found');
                }
            });
        this.props.loadSharingCounter(this.props.pathname);
    }

    loadNextItems() {
        const collection = this.props.state;
        if (!collection.self.isFetching && collection.paginated.next_page) {
            this.props.loadCollection(this.props.permalink, RELICS_PER_PAGE, collection.paginated.next_page);
        }
    }

    render() {
        const { collection, relics, state, setLike, setCollectionPrivacy, pathname, currentUser } = this.props;

        const isInitialLoading = !state.paginated.next_page;

        if (state.self.isFetching && isInitialLoading) {
            // Прелоадер нужен только во время первоначальной загрузки.
            // Во время последующих загрузок заменять страницу на прелоадер не нужно,
            // т.к. это будет подгрузка реликов, а не коллекции.
            // TODO исправить после разделения API загрузки коллекции и реликов
            return <div className="collection-page container">
                <Preloader />
            </div>;
        }

        if (state.self.errors.length) {
            return <div className="popUp show relic_popup">
                <div className="relic_popup_container">
                    <p>Ошибки при загрузке коллекции:</p>
                    {state.self.errors.map((index, key) => <p key={key}>{index}</p>)}
                </div>
            </div>;
        }

        if (R.isEmpty(R.values(collection))) {
            return null;
        }

        const isOwnCollection = currentUser.url === collection.user.url;

        const onPrivacyChange = (privacy) => setCollectionPrivacy(collection.id, privacy);
        const onCollectionLike = () => setLike('collection', collection.id, !collection.like);
        const relicsItems = relics.map((item, key) =>
            <RelicFeedItem key={key} relic={item} onLike={() => setLike('relic', item.id, !item.like)}/>);

        return <article className="collection-page container">
            <DocMeta title={stripHtml(collection.title)} />
            <header className="collection-page__header">
                <h1 className="collection-page__header__title" dangerouslySetInnerHTML={{__html: collection.title}}></h1>
                <div className="collection-page__header__wrapper">
                    <div className="collection-page__header__col collection-page__header__margin">
                        <User user={collection.user} />
                        <Datetime className="relic_popup_date time_tag" datetime={collection.created_at} />
                    </div>
                    <LikeButton className="collection-page__header__margin"
                                count={collection.likes_count}
                                isLiked={collection.like}
                                onLike={onCollectionLike} />
                    <div className="collection-page__header__margin">
                        <SocialButtons {...this.props.sharing} title={collection.title} url={collection.url} />
                    </div>
                    {isOwnCollection && <div className="pull-right">
                        <Link className="relic-popup__edit"
                              to={`${pathname}${pathname.substr(-1) === '/' ? '' : '/'}edit`}>
                            Редактировать
                        </Link>
                        <span className="relic-popup__dot">⋅</span>
                        <PrivacyDropdown style={{display: 'inline-block'}}
                                         privacy={collection.privacy}
                                         onSelect={onPrivacyChange}/>
                    </div>}
                </div>
            </header>
            <div className="collection-page__description">
                <p dangerouslySetInnerHTML={{__html: collection.description}}></p>
            </div>
            <Feed {...state.self}
                className="collection-page__relic-list"
                onLoadNextItems={() => this.loadNextItems()}>
                {relicsItems}
            </Feed>
        </article>;
    }
}
