import React, { PropTypes, cloneElement } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import R from 'ramda';

import Popup from './../components/Popup';
import Header from '../components/Header';
import { loadCurrentUser, closeModal, logout } from '../reducers/app';
import { setTimelineType } from '../reducers/timeline';
import {
    loadActivityFeedItems,
    markActivityFeedItems,
    updateActivityFeedCounter,
    clearActivityFeedItems
} from '../reducers/activityFeed';

@connect(
    (state) => ({
        currentUser: state.app.currentUser,
        timelineFeedType: state.timeline.feedType,
        modals: state.app.modals,
        activityFeed: state.activityFeed,
        activityFeedState: state.states.activityFeed,
        pathname: state.router.location.pathname
    }),
    (dispatch) => R.merge(
        bindActionCreators({
            setTimelineType,
            loadCurrentUser,
            closeModal,
            logout
        }, dispatch),
        {
            activityFeedActions: bindActionCreators({
                loadActivityFeedItems,
                markActivityFeedItems,
                updateActivityFeedCounter,
                clearActivityFeedItems
            }, dispatch)
        }
    )
)
export class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            background: null
        }
    }

    static contextTypes = {
        store: PropTypes.object
    };

    static childContextTypes = {
        currentUser: PropTypes.object
    };

    getChildContext() {
        return {
            currentUser: this.props.currentUser
        }
    }

    componentWillMount() {
        this.props.loadCurrentUser();
    }

    componentWillReceiveProps(nextProps) {
        if (
            nextProps.location.key !== this.props.location.key &&
            nextProps.location.state &&
            nextProps.location.state.modal
        ) {
            this.setState({
                background: this.props.children
            });
        }
    }

    onLogout() {
        this.props
            .logout()
            .then(action => {
                if (!action.error) {
                    document.location = '/';
                }
            });
    }

    render() {
        var { timelineFeedType, currentUser, setTimelineType, activityFeedActions, activityFeed, activityFeedState,
            location, modals, history, closeModal, pathname } = this.props;

        if (R.isEmpty(R.keys(currentUser))) {
            return <div>Загрузка пользователя</div>
        }

        const isModal = !!(location.state && location.state.modal && this.state.background);
        document.querySelector('body').style.overflow = (isModal || modals.length) ? 'hidden' : '';

        return <div>
            <Header feedType={timelineFeedType}
                    onSetTimelineType={setTimelineType}
                    onLogout={() => this.onLogout()}
                    isTimeline={pathname === '/' || isModal}
                    activityFeed={activityFeed}
                    activityFeedState={activityFeedState}
                    activityFeedActions={activityFeedActions}/>

            <div className="header-placeholder">
                <div className="container"></div>
            </div>

            {isModal ? this.state.background : this.props.children}

            {isModal && (
                <Popup history={this.props.history}
                       returnTo={location.state.returnTo}
                       scrollTo={location.state.scrollTo}>
                    {this.props.children}
                </Popup>
            )}

            {modals.map((item, key) => <Popup key={key} history={history} onOverlayClick={closeModal}>
                    {item}
                </Popup>
            )}
        </div>;
    }
}