import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import R from 'ramda';
import DocMeta from 'react-document-meta';
import stripHtml from '../utils/stripHtml';

import Feed from '../components/Feed';
import UserProfile from '../components/UserProfile';
import RelicFeedItem from '../components/Feed/RelicFeedItem';
import CollectionFeedItem from '../components/Feed/CollectionFeedItem';
import InvestigationFeedItem from '../components/Feed/InvestigationFeedItem';
import FeedTypeSwitch, { USER_FEED_TYPE_COLLECTIONS, USER_FEED_TYPE_RELICS } from '../components/FeedTypeSwitch';

const ITEMS_PER_PAGE = 10;

import { loadUser, loadUserRelics, loadUserCollections, clearUserFeed } from '../reducers/userFeed'
import { setSubscribe, setLike } from '../reducers/app';
//TODO блокировка на время isFetching?
@connect(
    (state) => ({
        userFeed: state.userFeed,
        username: state.router.params.username,
        state: state.states.userFeed
    }),
    (dispatch) => bindActionCreators({
        loadUser,
        loadUserRelics,
        loadUserCollections,
        clearUserFeed,
        setLike,
        setSubscribe
    }, dispatch)
)
export class User extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userFeedType: USER_FEED_TYPE_RELICS
        };
    }

    componentWillUnmount() {
        this.props.clearUserFeed();
    }

    componentWillMount() {
        this.props
            .loadUser(this.props.username)
            .then(action => {
                if (action.error && action.payload.status === 404) {
                    this.props.history.pushState(null, '/not-found');
                }
            });
        this.props.loadUserRelics(this.props.username, ITEMS_PER_PAGE, 1);
        this.props.loadUserCollections(this.props.username, ITEMS_PER_PAGE, 1);
    }

    loadNextElements() {
        if (this.state.userFeedType === USER_FEED_TYPE_COLLECTIONS) {
            const collections = this.props.state.collections;
            if (!collections.self.isFetching && collections.paginated.next_page) {
                this.props.loadUserCollections(this.props.username, ITEMS_PER_PAGE, collections.paginated.next_page);
            }
        } else {
            const relics = this.props.state.relics;
            if (!relics.self.isFetching && relics.paginated.next_page) {
                this.props.loadUserRelics(this.props.username, ITEMS_PER_PAGE, relics.paginated.next_page);
            }
        }
    }

    onSelectFeedType(feedType) {
        this.setState({userFeedType: feedType})
    }

    render() {
        const { userFeed, state, setSubscribe, setLike } = this.props;

        const isRelics = this.state.userFeedType === USER_FEED_TYPE_RELICS;
        const currentFeed = isRelics ? 'relics' : 'collections';
        const user = userFeed.user;

        let items = [];
        if (userFeed && userFeed[currentFeed]) {
            items = userFeed[currentFeed].map((item, key) => {
                const onLike = () => setLike(item._type, item.id, !item.like);
                switch (item._type) {
                    case 'relic':
                        return <RelicFeedItem key={key} relic={item} onLike={onLike}/>;
                    case 'collection':
                        return <CollectionFeedItem key={key} collection={item} onLike={onLike}/>;
                    case 'investigation':
                        return <InvestigationFeedItem key={key} investigation={item} onLike={onLike}/>;
                    default:
                        console.error('Unknown feed item type');
                        return null;
                }
            });
        }

        return <div className="user-page container">
            <DocMeta title={stripHtml(user.handle)} />
            <div className="user-page__sidebar">
                <UserProfile {...state.user}
                    key="profile"
                    user={user}
                    onSubscribe={() => setSubscribe(user.handle, !user.follow)} />
            </div>
            <div className="user-page__content">
                <FeedTypeSwitch
                    feedType={this.state.userFeedType}
                    user={user}
                    onCollectionsClick={() => this.onSelectFeedType(USER_FEED_TYPE_COLLECTIONS)}
                    onRelicsClick={() => this.onSelectFeedType(USER_FEED_TYPE_RELICS)}/>

                <Feed {...state[currentFeed]}
                    className="user-page__content__items"
                    ref="feed"
                    onLoadNextItems={() => this.loadNextElements()}>
                    {items}
                </Feed>
            </div>
        </div>;
    }
}
