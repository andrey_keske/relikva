import React, { PropTypes, findDOMNode } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import R from 'ramda';

import { loadSelfCollections, clearSelfCollections } from '../reducers/addToCollection';
import { addRelicToCollection, deleteRelicFromCollection } from '../reducers/relic';
import Dropdown from '../components/Dropdown';
import Preloader from '../components/Preloader';
import { closeModal } from '../reducers/app';
import CollectionsList from '../components/CollectionsList';
import { CollectionNew } from './CollectionNew';

const COLLECTIONS_PER_PAGE = 20;

@connect(
    (state) => ({
        collections: state.addToCollection.collections,
        response: state.addToCollection.response,
        relic: state.relic,
        state: state.states.addToCollection
    }),
    (dispatch) => bindActionCreators({
        loadSelfCollections,
        addRelicToCollection,
        deleteRelicFromCollection,
        clearSelfCollections,
        closeModal
    }, dispatch)
)
export class AddToCollection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showAddCollection: false,
        };
    }

    componentWillMount() {
        this.props.clearSelfCollections();
        this.props.loadSelfCollections(COLLECTIONS_PER_PAGE);
    }

    toggleRelicFromCollection(collectionId) {
        if (this.collectionIsActive(collectionId)) {
            this.props.deleteRelicFromCollection(this.props.relic.permalink, collectionId)
        } else {
            this.props.addRelicToCollection(this.props.relic.permalink, collectionId)
        }
    }

    collectionIsActive(collectionId) {
        return R.contains(collectionId, this.props.relic.collection_ids || [])
    }

    render() {
        const {
            state: { list: { isFetching }, paginated },
            collections,
            closeModal,
            loadSelfCollections } = this.props;

        const loadMore = <button key="more"
                                 onClick={() => loadSelfCollections(COLLECTIONS_PER_PAGE, paginated.next_page)}>
            Загрузить еще
        </button>;

        const renderItems = (side) => {
            const leftSideItems = collections.slice(0, ((collections.length / 2) + 1));
            const rightSideItems = collections.slice((collections.length / 2 + 1), collections.length);
            const collectionsSliced = side === 'left' ? leftSideItems : rightSideItems;

            return collectionsSliced.map((item, key) =>
                <li key={key}
                    className={'relic-add-to-collection__li ' + (this.collectionIsActive(item.id) ? 'active' : '')}
                    onClick={() => this.toggleRelicFromCollection(item.id)}>
                    <div dangerouslySetInnerHTML={{__html: item.title}}></div>
                </li>)
                .concat(isFetching
                    ? <li key="loading"><Preloader /></li>
                    : (!isFetching && paginated.next_page && loadMore));
        };

        return <div className="popup-wrapper__modal-page">
            <div className="relic-popup">
                <div className="collection_popup relic-add-to-collection">
                    <div className="relic_popup_container relic_popup_container__close-bg">
                        <div className="relic-popup-close" onClick={() => closeModal()}></div>
                        <div className="relic-popup-back" onClick={() => closeModal()}>
                            Вернуться
                        </div>
                        <h4>Добавить в коллекцию</h4>
                        {
                            collections.length > 0 ? (
                                <div>
                                    <div className="relic-add-to-collection__col">
                                        <ul className="relic-add-to-collection__ul">
                                            {renderItems('left')}
                                        </ul>
                                    </div>
                                    <div className="relic-add-to-collection__col">
                                        <ul className="relic-add-to-collection__ul">
                                            {renderItems('right')}
                                        </ul>
                                    </div>
                                </div>
                                ) : (
                                    <p className="relic-add-to-collection__empty">
                                        У вас пока нет ни одной коллекции.
                                    </p>
                                )
                        }

                        <div className={ `relic-add-to-collection__add-block ${this.state.showAddCollection ? 'relic-add-to-collection__add-block--show' : '' }` }>
                            <div className="relic-add-to-collection__close"
                                 onClick={() => this.setState({showAddCollection: false})} />
                            <CollectionNew />
                        </div>
                        <div
                              className={ `btn-action relic-add-to-collection__new ${!this.state.showAddCollection ? 'relic-add-to-collection__new--show' : '' }` }
                              onClick={() => this.setState({showAddCollection: true})}>
                            Новая коллекция
                        </div>
                        <button
                              className="btn-action relic-add-to-collection__add"
                              disabled={this.props.relic.collection_ids.length === 0 ? 'disabled' : ''}
                              onClick={() => closeModal()}>
                            Добавить
                        </button>
                    </div>
                </div>
            </div>
        </div>;
    }
}