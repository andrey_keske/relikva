import React, { PropTypes, cloneElement } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import R from 'ramda';
import DocMeta from 'react-document-meta';

import Feed from '../components/Feed';
import RelicFeedItem from '../components/Feed/RelicFeedItem';
import { setLike } from '../reducers/app';
import { loadRelics } from '../reducers/notFound';

const RELICS_PER_PAGE = 12;

@connect(
    (state) => ({
        relics: state.notFound.relics,
        state: state.states.notFound
    }),
    (dispatch) => bindActionCreators({
        loadRelics,
        setLike
    }, dispatch)
)
export class NotFound extends React.Component {
    componentWillMount() {
        this.props.loadRelics(RELICS_PER_PAGE);
    }

    render() {
        const { state, relics, setLike } = this.props;

        return <div>
            <DocMeta title="Страница не найдена" />
            <section className="container error_page ng-scope">
                <h1>Ошибка 404</h1>
                <h1>Страница не найдена</h1>

                <p>Возможно страница удалена или перемещена на новый адрес.</p>

                <p>Если вы уверены что причина в другом, то <a className="feedback" href="mailto:feedback@relikva.com">сообщите
                    нам</a> об ошибке</p>

                <div className="button">
                    <Link className="btn-rectangle big" to="/">Вернуться на главную</Link>
                </div>

                <h1>Реликвы:</h1>
                <Feed {...state.relics}>
                    {relics.map((item, key) =>
                        <RelicFeedItem key={key} relic={item} onLike={() => setLike('relic', item.id, !item.like)}/>
                    )}
                </Feed>
            </section>
        </div>
    }
}