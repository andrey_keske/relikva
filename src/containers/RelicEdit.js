import React, { PropTypes, findDOMNode } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import R from 'ramda';
import classes from 'classnames';
import debounce from 'lodash.debounce';
import DocMeta from 'react-document-meta';
import stripHtml from '../utils/stripHtml';

import {
    apiCreateBlankUser,
    apiUpdateBlankUser,
    apiCreateImage,
    apiRotateImage,
    apiDeleteImage,
    apiCreateCategory,
    apiSearchCategories,
    apiSearchUsers
} from '../utils/api.js'
import { loadRelic, updateRelic, deleteRelic } from '../reducers/relicEdit';

import PrivacyDropdown from '../components/PrivacyDropdown';
import CategoriesSelect from '../components/RelicForm/CategoriesSelect';
import PeoplesSelect from '../components/RelicForm/PeoplesSelect';
import ImagesList from '../components/RelicForm/ImagesList';

const CATEGORIES_PER_PAGE = 10;

@connect(
    (state) => ({
        state: state.states.relicEdit,
        relic: state.relicEdit,
        permalink: state.router.params.permalink,
        currentUser: state.app.currentUser
    }),
    (dispatch) => bindActionCreators({
        loadRelic,
        updateRelic,
        deleteRelic
    }, dispatch)
)
export class RelicEdit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            relic: {
                title: '',
                description: '',
                privacy: 'public',
                venue: '',
                lat: '',
                lon: '',

                date_start_century: '',
                date_start_decade: '',
                date_start_year: '',
                date_start_month: '',
                date_start_day: ''
            },
            blankUsers: [],
            users: [],
            collections: [],
            images: [],

            selectedCategories: [],
            categorySearch: '',

            selectedBlankUsers: [],
            selectedUsers: [],
            peopleSearch: ''
        }
    }

    loadRelic() {
        this.props
            .loadRelic(this.props.permalink)
            .then(action => {
                if (action.error && action.payload.status === 404) {
                    this.props.history.pushState(null, '/not-found');
                }
                if (!action.error) {
                    const relic = action.payload.relic;
                    if (relic.owner.id !== this.props.currentUser.id) {
                        this.props.history.pushState(null, '/not-found');
                    } else {
                        this.setState(
                            R.compose(
                                R.assoc('relic', relic),
                                R.assoc('images', relic.images),
                                R.assoc('selectedUsers', relic.peoples),
                                R.assoc('selectedUsers', relic.peoples),
                                R.assoc('selectedCategories', relic.categories)
                            )(this.state)
                        )
                    }
                }
            });
    }

    componentWillMount() {
        this.loadRelic();

        this.prevCategoryQuery = '';
        this.searchCategoriesDebounce = debounce(
            (query, callback) => {
                apiSearchCategories(query, CATEGORIES_PER_PAGE)
                    .then(response => callback(null, response.categories));
                this.prevCategoryQuery = query;
            },
            500
        );
        this.prevPeopleQuery = '';
        this.searchPeoplesDebounce = debounce(
            (query, callback) => {
                if (query && query !== this.prevPeopleQuery) {
                    apiSearchUsers(query, true, true, true)
                        .then(response => {
                            const { users, blank_users } = response;
                            callback(null, users.concat(blank_users))
                        });
                    this.prevPeopleQuery = query;
                }
            },
            500
        );
    }

    onSaveClick() {
        const relic = this.state.relic;
        this.props.updateRelic(relic.id, {
            ...relic,
            image_ids: R.pluck('id', this.state.images),
            blank_person_ids: R.pluck('id', this.state.selectedBlankUsers),
            person_ids: R.pluck('id', this.state.selectedUsers),
            category_ids: R.pluck('id', this.state.selectedCategories)
        });
    }

    onInputChange(key, value) {
        if (value !== R.path(['relic', key], this.state)) {
            this.setState(R.assocPath(['relic', key], value, this.state));
        }
    }

    renderCategories() {
        const onChangeSearch = (query) => this.setState(R.assoc('categorySearch', query, this.state));
        const onCategorySelect = (category) => this.setState(
            R.compose(
                R.over(R.lensProp('selectedCategories'), R.compose(R.uniq, R.append(category))),
                R.set(R.lensProp('categorySearch'), '')
            )(this.state)
        );
        const onCategoryDeselect = (id) => this.setState(
            R.over(R.lensProp('selectedCategories'), R.reject(R.propEq('id', id)), this.state)
        );
        const onCategoryCreate = () => apiCreateCategory(this.state.categorySearch).then(onCategorySelect);

        return <CategoriesSelect selectedCategories={this.state.selectedCategories}
                                 categorySearch={this.state.categorySearch}
                                 onGetSuggestions={(query, callback) => this.searchCategoriesDebounce(query, callback)}
                                 onChangeSearch={(value) => onChangeSearch(value)}
                                 onCreate={() => onCategoryCreate()}
                                 onSelect={(suggestion) => onCategorySelect(suggestion)}
                                 onDelete={(id) => onCategoryDeselect(id)}/>
    }

    renderPeoples() {
        const onChangeSearch = (query) => this.setState(R.assoc('peopleSearch', query, this.state));
        const onPeopleSelect = (people) => this.setState(
            R.compose(
                R.over(
                    R.lensProp(people.handle ? 'selectedUsers' : 'selectedBlankUsers'),
                    R.compose(R.uniq, R.append(people))
                ),
                R.set(R.lensProp('peopleSearch'), '')
            )(this.state)
        );
        const onPeopleDeselect = (id) => this.setState(
            R.over(R.lensProp('selectedUsers'), R.reject(R.propEq('id', id)), this.state)
        );
        const onBlankPeopleDeselect = (id) => this.setState(
            R.over(R.lensProp('selectedBlankUsers'), R.reject(R.propEq('id', id)), this.state)
        );
        const onPeopleCreate = () => apiCreateBlankUser(this.state.peopleSearch).then(onPeopleSelect);

        return <PeoplesSelect selectedUsers={this.state.selectedUsers}
                              selectedBlankUsers={this.state.selectedBlankUsers}
                              peopleSearch={this.state.peopleSearch}
                              onGetSuggestions={(query, callback) => this.searchPeoplesDebounce(query, callback)}
                              onChangeSearch={(value) => onChangeSearch(value)}
                              onCreate={() => onPeopleCreate()}
                              onSelect={(people) => onPeopleSelect(people)}
                              onDeleteBlank={(id) => onBlankPeopleDeselect(id)}
                              onDelete={(id) => onPeopleDeselect(id)}/>
    }

    renderImages() {
        const LImages = R.lensProp('images');
        const onDropFile = (file) => apiCreateImage(file)
            .then(image => this.setState(
                R.over(
                    LImages,
                    R.append(image),
                    this.state
                )
            ));
        const onChangeImageOrder = (image, offset) => {
            const prevIndex = R.findIndex(R.equals(image), this.state.images);
            const nextIndex = (prevIndex + offset) < 0 ? 0 : prevIndex + offset;
            this.setState(
                R.over(
                    LImages,
                    R.compose(
                        R.insert(nextIndex, image),
                        R.reject(R.equals(image))
                    ),
                    this.state
                )
            );
        };
        const getImageIndexLensById = (imageId, images = []) => R.lensIndex(R.findIndex(R.propEq('id', imageId), images));
        const onRotateImage = (id, angle) => apiRotateImage(id, angle)
            .then(image => this.setState(
                R.set(
                    R.compose(LImages, getImageIndexLensById(id, this.state.images)),
                    image,
                    this.state
                )
            ));
        const onDeleteImage = (id) => apiDeleteImage(id)
            .then(image => this.setState(
                R.over(
                    LImages,
                    R.reject(R.propEq('id', id)),
                    this.state
                )
            ));

        return <ImagesList images={this.state.images}
                           onDropFile={onDropFile}
                           onMoveImage={onChangeImageOrder}
                           onRotateImage={onRotateImage}
                           onDeleteImage={onDeleteImage}/>
    }

    render() {
        const { relic } = this.state;
        const { state } = this.props;
        const isFetching = state.self.isFetching;

        const classNames = classes({
            [this.props.className]: true,
            'container': true,
            'is-fetching': isFetching
        });

        let errorsItems = null;
        if (state.self.errors.length > 0) {
            errorsItems = <div className="form-group">
                {state.self.errors.map((item, key) => <p key={key}>{item}</p>)}
            </div>
        }

        return <div className={classNames}>
            <DocMeta title={`Редактирование ${stripHtml(relic.title)}`} />
            <h4>Редактирование реликвы</h4>

            <PrivacyDropdown privacy={relic.privacy}
                             disabled={isFetching}
                             onSelect={(privacy) => !isFetching && this.onInputChange('privacy', privacy)}/>

            <div className="form-group">
                <label>Название</label>
                <input className="form-control"
                       disabled={isFetching}
                       value={relic.title}
                       onChange={(e) => !isFetching && this.onInputChange('title', e.target.value)}
                       type="text"/>
            </div>

            <div className="form-group">
                <label>Описание</label>
                <textarea className="form-control"
                          disabled={isFetching}
                          value={relic.description}
                          onChange={(e) => !isFetching && this.onInputChange('description', e.target.value)}
                          rows="3"></textarea>
            </div>

            <div className="form-group">
                <label>Фотографии</label>

                <div className="form-control">
                    {this.renderImages()}
                </div>
            </div>

            <div className="form-group">
                <label>Категории</label>

                <div className="form-control">
                    {this.renderCategories()}
                </div>
            </div>

            <div className="form-group">
                <label>Дата</label>

                <div className="form-control">
                </div>
            </div>

            <div className="form-group">
                <label>Место</label>

                <div className="form-control">
                    <input type="text"
                           value={this.state.relic.venue}
                           onChange={(e) => this.onInputChange('venue', e.target.value)}/>
                </div>
            </div>

            <div className="form-group">
                <label>Люди</label>

                <div className="form-control">
                    {this.renderPeoples()}
                </div>
            </div>

            {errorsItems}

            <button disabled={isFetching}
                    className="btn btn-rectangle big"
                    onClick={() => !isFetching && this.onSaveClick()}>
                Сохранить
            </button>
        </div>
    }
}
