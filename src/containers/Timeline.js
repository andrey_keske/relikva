import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import R from 'ramda';
import DocMeta from 'react-document-meta';

import Feed from '../components/Feed';
import RecommendedFriends from '../components/RecommendedFriends';
import RelicFeedItem from '../components/Feed/RelicFeedItem';
import CollectionFeedItem from '../components/Feed/CollectionFeedItem';
import InvestigationFeedItem from '../components/Feed/InvestigationFeedItem';

import { FEED_TYPE_ALL, FEED_TYPE_SUBSCRIPTIONS, FEED_TYPE_POPULAR, loadTimelineItems, clearTimelineItems } from '../reducers/timeline';
import { loadRecommendedFriends, setSubscribe, setLike } from '../reducers/app';

@connect(
    (state) => ({
        timeline: state.timeline.items,
        recommendedFriends: state.app.recommendedFriends,
        timelineFeedType: state.timeline.feedType,
        state: state.states.timeline
    }),
    (dispatch) => bindActionCreators({
        loadTimelineItems,
        clearTimelineItems,
        loadRecommendedFriends,
        setLike,
        setSubscribe
    }, dispatch)
)
export class Timeline extends React.Component {
    componentWillUnmount() {
        this.props.clearTimelineItems();
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.timelineFeedType !== nextProps.timelineFeedType) {
            this.refs.feed.setPage(1);
            this.loadNextElements(1, nextProps);
        }
    }

    loadNextElements(page = 1, props = this.props) {
        // Параметр props нужен для вызова этой функции из componentWillReceiveProps.
        
        if (!props.state.isFetching) {
            props.loadTimelineItems(
                page, 
                props.timelineFeedType === FEED_TYPE_SUBSCRIPTIONS,
                props.timelineFeedType === FEED_TYPE_POPULAR
            );
        }
    }

    render() {
        const { timeline, recommendedFriends, loadRecommendedFriends, state, setLike, setSubscribe } = this.props;

        const items = timeline.map((item, key) => {
            const onLike = () => setLike(item._type, item.id, !item.like);
            switch (item._type) {
                case 'relic':
                    return <RelicFeedItem key={key} relic={item} onLike={onLike}/>;
                case 'collection':
                    return <CollectionFeedItem key={key} collection={item} onLike={onLike}/>;
                case 'investigation':
                    return <InvestigationFeedItem key={key} investigation={item} onLike={onLike}/>;
                default:
                    console.error('Unknown feed item type');
                    return null;
            }
        });

        return <div>
            <DocMeta title="Relikva" />
            <Feed {...state.feed} ref="feed" onLoadNextItems={(page) => this.loadNextElements(page)}>
                {R.prepend(
                    <RecommendedFriends {...state.recommendedFriends}
                        key="friends"
                        users={recommendedFriends}
                        onSubscribe={setSubscribe}
                        onLoadRecommendedFriends={loadRecommendedFriends}/>,
                    items
                )}
            </Feed>
        </div>;
    }
}