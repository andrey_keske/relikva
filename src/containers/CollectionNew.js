import React, { PropTypes, findDOMNode } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import R from 'ramda';
import classes from 'classnames';
import DocMeta from 'react-document-meta';

import { createCollection } from '../reducers/collectionNew';
import { closeModal } from '../reducers/app';
import PrivacyDropdown from '../components/PrivacyDropdown';
import declension from '../utils/declension';

const RELICS_PER_PAGE = 20;

@connect(
    (state) => ({
        state: state.states.collectionNew,
        collection: state.collectionNew.collection
    }),
    (dispatch) => bindActionCreators({
        createCollection
    }, dispatch)
)
export class CollectionNew extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            collection: {
                privacy: 'public',
                title: '',
                description: ''
            }
        }
    }

    onCreateClick() {
        this.props
            .createCollection(this.state.collection)
            .then(action => {
                if (!action.error && this.props.history) {
                    this.props.history.pushState(null, `/@${action.payload.collection.user.handle}/c/${action.payload.collection.permalink}`);
                }
            })
        this.setState({
            title: '',
            description: '',
        })
    }

    onInputChange(key, value) {
        if (value !== R.path(['collection', key], this.state)) {
            this.setState(R.assocPath(['collection', key], value, this.state));
        }
    }

    render() {
        const { collection } = this.state;
        const { state } = this.props;
        const isFetching = state.self.isFetching;

        const maxTitleLength = 100;
        const maxDescriptionLength = 100;

        const classNames = classes({
            [this.props.className]: true,
            'new-collection': true,
            'container': true,
            'is-fetching': isFetching
        });

        let errorsItems = null;
        if (state.self.errors.length > 0) {
            errorsItems = <div className="form-group">
                {state.self.errors.map((item, key) => <p key={key}>{item}</p>)}
            </div>
        }

        const titleMaxLength = 70;
        const descriptionMaxLength = 1500;

        const titleLengthLeft = titleMaxLength - collection.title.length;
        const descriptionLengthLeft = descriptionMaxLength - collection.description.length;

        return <div className={classNames}>
            <DocMeta title="Новая коллекция" />
            
            <h4>Новая коллекция</h4>

            <PrivacyDropdown privacy={collection.privacy}
                             disabled={state.self.isFetching}
                             onSelect={(privacy) => !isFetching && this.onInputChange('privacy', privacy)}/>

            <div className="form-group">
                <label>Название</label>
                <label className="counter">
                    {`${titleLengthLeft} ${declension(titleLengthLeft, ['символ', 'символа', 'символов'])}`}
                </label>
                <input value={this.state.title}
                       disabled={state.self.isFetching}
                       className="form-control"
                       type="text"
                       maxLength={maxTitleLength}
                       onChange={(e) => this.onInputChange('title', e.target.value)} />
            </div>

            <div className="form-group">
                <label>Описание</label>
                <label className="counter">
                    {`${descriptionLengthLeft} ${declension(descriptionLengthLeft, ['символ', 'символа', 'символов'])}`}
                </label>
                <textarea value={this.state.description}
                          disabled={state.self.isFetching}
                          className="form-control"
                          rows="3"
                          maxLength={maxDescriptionLength}
                          onChange={(e) => this.onInputChange('description', e.target.value)} />
            </div>

            {errorsItems}

            <button disabled={state.self.isFetching || !collection.title || !collection.description}
                    className="relic-add-to-collection__add"
                    onClick={() => !isFetching && this.onCreateClick()}>
                Создать
            </button>
        </div>
    }
}
