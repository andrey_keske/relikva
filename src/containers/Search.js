import React, { PropTypes } from 'react';
import { findDOMNode } from 'react-dom';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import R from 'ramda';
import DebounceInput from 'react-debounce-input';
import DocMeta from 'react-document-meta';
import debounce from 'lodash.debounce';

import Feed from '../components/Feed';
import RelicFeedItem from '../components/Feed/RelicFeedItem';
import CollectionFeedItem from '../components/Feed/CollectionFeedItem';
import InvestigationFeedItem from '../components/Feed/InvestigationFeedItem';

import { search } from '../reducers/search';
import { setLike } from '../reducers/app';
import declension from '../utils/declension';

const ITEMS_PER_PAGE = 20;

@connect(
    (state) => ({
        state: state.states.search,
        feeds: state.search,
        query: R.path(['router', 'location', 'query', 'q'], state) || '',
        pathname: state.router.location.pathname
    }),
    (dispatch) => bindActionCreators({
        search,
        setLike
    }, dispatch)
)
export class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            query: props.query
        }
    }

    search(query) {
        if (query && query !== this.props.query) {
            this.props.history.pushState(null, this.props.pathname, {q: query});
            this.props.search(query, ITEMS_PER_PAGE, 1);
        }
    }

    componentWillMount() {
        if (this.props.query) {
            this.props.search(this.props.query, ITEMS_PER_PAGE, 1);
        }
    }

    onLoadNextItems() {
        const { query, feeds: { relics, collections }, state } = this.props;

        const relicsPaginated = state.relics.paginated;
        const collectionsPaginated = state.collections.paginated;

        const hasItems = relics.length || collections.length;
        const nextPage = relicsPaginated.next_page || collectionsPaginated.next_page;

        if (query && hasItems && nextPage)  {
            this.props.search(query, ITEMS_PER_PAGE, nextPage);
        }
    }

    render() {
        const { feeds: { relics, collections }, state, setLike } = this.props;
        const { query } = this.state;

        const isFetching = state.self.isFetching;

        const items = []
            .concat(relics.map((item, key) => {
                const onLike = () => setLike('relic', item.id, !item.like);
                return <RelicFeedItem key={`relic-${key}`} relic={item} onLike={onLike}/>;
            }))
            .concat(collections.map((item, key) => {
                const onLike = () => setLike('collection', item.id, !item.like);
                return <CollectionFeedItem key={`collection-${key}`} collection={item} onLike={onLike}/>;
            }));

        let foundLabel;
        if (!isFetching) {
            const itemsCount = relics.length + collections.length;
            if (itemsCount) {
                foundLabel = `Найдено ${itemsCount} ${declension(itemsCount, ['запись', 'записи', 'записей'])}`;
            } else {
                foundLabel = `Ничего не найдено`;
            }
        }

        return <div className="container search-page">
            <DocMeta title="Поиск" />
            <div className="search-page__wrapper">
                <div className="search-page__fixed container">
                    <input className="form-control search-page__input"
                           minLength={2}
                           focused={true}
                           value={query}
                           onKeyDown={(e) => e.key === 'Enter' && this.search(query)}
                           onChange={(e) => this.setState({query: e.target.value})} />
                    <div className="search-page__button" onClick={() => this.search(query)}>Поиск</div>
                </div>
                <p className="container search-page__found-label">
                    {foundLabel}
                </p>
            </div>
            <Feed {...state.self}
                className="search-page__content"
                onLoadNextItems={(page) => !isFetching && this.onLoadNextItems(page)}>
                {items}
            </Feed>
        </div>;
    }
}
