import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import R from 'ramda';
import classes from 'classnames';
import Dropzone from 'react-dropzone';
import DocMeta from 'react-document-meta';

import { loadSelfProfile, updateSelfProfile, updatePassword, updateAvatar, deleteAvatar } from '../reducers/selfProfile'
import NewPassword from '../components/NewPassword';

@connect(
    (state) => ({
        profile: state.selfProfile,
        state: state.states.selfProfile
    }),
    (dispatch) => bindActionCreators({
        loadSelfProfile,
        updateSelfProfile,
        updatePassword,
        updateAvatar,
        deleteAvatar
    }, dispatch)
)
export class SelfProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        this.props
            .loadSelfProfile()
            .then(action => {
                if (!action.error) {
                    this.setState(action.payload.user);
                }
            });
    }

    onProfileSaveClick() {
        this.props
            .updateSelfProfile(this.state)
            .then(action => {
                if (!action.error) {
                    this.setState(action.payload.user);
                }
            })
    }

    onNotificationItemToggle(key) {
        this.setState(
            R.over(
                R.compose(R.lensProp('subscription'), R.lensProp(key)),
                R.not,
                this.state
            )
        )
    }

    onInputChange(key, value) {
        this.setState(R.assoc(key, value, this.state));
    }

    onChangeAvatar(fileData) {
        this.props
            .updateAvatar(fileData)
            .then(action => {
                if (!action.error) {
                    this.setState(action.payload.user);
                }
            })
    }

    onDeleteAvatar() {
        this.props
            .deleteAvatar()
            .then(action => {
                if (!action.error) {
                    this.setState(action.payload.user);
                }
            })
    }

    renderNotifications() {
        const subscription = this.state.subscription || {};
        const isFetching = this.props.state.self.isFetching;

        const renderCheckBox = (key, label) =>
            <li className={!subscription[key] ? 'active checkbox' : 'checkbox'}
                onClick={() => !isFetching && this.onNotificationItemToggle(key)}>
                <label>
                    {label}
                </label>
            </li>;

        return <div className="form-group">
            <p className="edit-profile__label">Оповещения по электронной почте</p>
            <ul className="form-group">
                {renderCheckBox('disabled_follows', 'Новые подписчики')}
                {renderCheckBox('disabled_likes', 'Новые лайки')}
                {renderCheckBox('disabled_discussions', 'Новые комментарии к реликве')}
                {renderCheckBox('disabled_investigations', 'Новые комментарии к расследованию')}
                {renderCheckBox('disabled_weekly', 'Еженедельная рассылка')}
            </ul>
        </div>
    }

    onDrop(files) {
        const file = files[0];
        if (FileReader && file && (/^image\/(png|jpg|jpeg)$/i).test(file.type)) {
            let reader = new FileReader();
            reader.onload = (e) => this.onChangeAvatar(e.target.result);
            reader.readAsDataURL(file);
        }
    }

    renderUserpicNotificationSide() {
        const profile = this.state;
        const { state } = this.props;
        const isFetching = state.self.isFetching;

        const maleActive = profile.gender === 'male' ? 'edit-profile__button--active' : '';
        const femaleActive = profile.gender === 'female' ? 'edit-profile__button--active' : '';

        const classNames = classes({
            'account_user_form': true,
            'is-fetching': isFetching
        });

        const genderFields = <div>
            <p className="edit-profile__label">Пол</p>

            <div className={`edit-profile__button edit-profile__button--toggle ${maleActive}`}
                 onClick={() => this.onInputChange('gender', 'male')}>
                М
            </div>
            <div className={`edit-profile__button edit-profile__button--toggle ${femaleActive}`}
                 onClick={() => this.onInputChange('gender', 'female')}>
                Ж
            </div>
        </div>;

        return <div className={classNames}>
            <div>
                <p className="edit-profile__label">Фотография</p>

                <div className="edit-profile__account_user_uploader" name="file">
                    <Dropzone ref="dropzone"
                              className="wrapper"
                              disableClick={isFetching}
                              onDrop={(files) => !isFetching && this.onDrop(files)}
                              multiple={false}>
                        <img src={profile.avatar} />
                        <ul>
                            <li className="change" onClick={() => !isFetching && this.refs['dropzone'].open()}>
                                Изменить
                            </li>
                            <li className="remove" onClick={() => !isFetching && this.onDeleteAvatar()}>
                                Удалить
                            </li>
                        </ul>
                    </Dropzone>
                </div>

                <div className="form-group">
                    <div className="edit-profile__isorganization">
                        <li className={profile.type !== 'User' ? 'active checkbox' : 'checkbox'}
                            onClick={() => this.onInputChange('type', profile.type === 'User' ? 'Organization' : 'User')}>
                            <label>
                                <span>Организация</span>
                            </label>
                        </li>
                    </div>
                </div>

                <div className="form-group">
                    {profile.type === 'User' && genderFields}
                </div>

                <div className="form-group">
                    {this.renderNotifications()}
                </div>
            </div>
        </div>;
    }

    renderAboutProfile() {
        const profile = this.state;
        const { state } = this.props;
        const isFetching = state.self.isFetching;

        const userNameFields = <div>
            <div className="form-group">
                <p className="edit-profile__label">Имя</p>
                <input value={profile.first_name}
                       disabled={isFetching}
                       onChange={(e) => !isFetching && this.onInputChange('first_name', e.target.value)}
                       autofocus={true}
                       className="required form-control"
                       id="relic_first_name"
                       maxLength="255"
                       required={true}
                       type="text"/>
            </div>

            <div className="form-group">
                <p className="edit-profile__label">Фамилия</p>
                <input value={profile.last_name}
                       disabled={isFetching}
                       onChange={(e) => !isFetching && this.onInputChange('last_name', e.target.value)}
                       className="required form-control"
                       id="relic_last_name"
                       maxLength="255"
                       required={true}
                       type="text"/>
            </div>
        </div>;

        const organizationNameField = <div className="form-group">
            <p className="edit-profile__label">Название</p>
            <input value={profile.name}
                   disabled={isFetching}
                   onChange={(e) => !isFetching && this.onInputChange('name', e.target.value)}
                   className="required form-control"
                   id="relic_organization_name"
                   maxLength="255"
                   required={true}
                   type="text"/>
        </div>;

        const isUser = profile.type === 'User';

        return <div>

            {isUser ? userNameFields : organizationNameField}

            <div className="form-group">
                <p className="edit-profile__label">{isUser ? 'О себе' : 'Об организации'}</p>
                <textarea className="form-control"
                          disabled={isFetching}
                          value={profile.about}
                          onChange={(e) => !isFetching && this.onInputChange('about', e.target.value)}
                          rows="3">
                </textarea>
            </div>

            <div className="form-group">
                <p className="edit-profile__label">Адрес профиля на реликве</p>

                <input value={profile.handle}
                       disabled={isFetching}
                       onChange={(e) => !isFetching && this.onInputChange('handle', e.target.value)}
                       className="required form-control"
                       id="relic_handle" maxLength="255"
                       minLength="3" required="" type="text"/>
            </div>
        </div>
    }

    renderNewPassword() {
        const state = this.props.state.password;

        return <NewPassword onPasswordSave={this.props.updatePassword} {...state}/>;
    }

    render() {
        const { state } = this.props;
        const profile = this.state;
        const isFetching = state.self.isFetching;

        if (R.isEmpty(R.values(profile))) {
            return null;
        }

        return <div className="container edit-profile">
            <DocMeta title="Профиль" />
            <h1>Редактирование профиля</h1>

            <div className="edit-profile__col">
                <div className="account_user_form">
                    {this.renderUserpicNotificationSide()}
                </div>
            </div>
            <div className="edit-profile__col">
                {this.renderAboutProfile()}
            </div>
            <div className="edit-profile__col password_updater">
                {this.renderNewPassword()}
            </div>

            <hr />

            <div className="form-group edit-profile__actions">
                <div className="edit-profile__button edit-profile__button--active edit-profile__button--action"
                     onClick={() => !isFetching && this.onProfileSaveClick()}>
                    Сохранить
                </div>
                {/*<div className="edit-profile__button edit-profile__button--violet"
                     onClick={() => alert('close?')}>
                    Закрыть
                </div>*/}
            </div>
        </div>
    }
}