import React, { PropTypes, findDOMNode } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import R from 'ramda';
import classes from 'classnames';
import debounce from 'lodash.debounce';
import DocMeta from 'react-document-meta';

import {
    apiCreateBlankUser,
    apiUpdateBlankUser,
    apiCreateImage,
    apiRotateImage,
    apiDeleteImage,
    apiCreateCategory,
    apiSearchCategories,
    apiSearchUsers
} from '../utils/api.js'

import {
    createRelic,
    clearRelic
} from '../reducers/relicNew';
import declension from '../utils/declension';

import PrivacyDropdown from '../components/PrivacyDropdown';
import CategoriesSelect from '../components/RelicForm/CategoriesSelect';
import PeoplesSelect from '../components/RelicForm/PeoplesSelect';
import ImagesList from '../components/RelicForm/ImagesList';
import CenturySelect from '../components/RelicForm/CenturySelect';
import YearSelect from '../components/RelicForm/YearSelect';
import MonthSelect from '../components/RelicForm/MonthSelect';
import DaySelect from '../components/RelicForm/DaySelect';

const CATEGORIES_PER_PAGE = 10;

@connect(
    (state) => ({
        state: state.states.relicNew,
        relic: state.relicNew
    }),
    (dispatch) => bindActionCreators({
        createRelic,
        clearRelic
    }, dispatch)
)
export class RelicNew extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            relic: {
                title: '',
                description: '',
                privacy: 'public',
                venue: '',
                lat: '',
                lon: '',

                date_start_century: '',
                date_start_year: '',
                date_start_month: '',
                date_start_day: ''
            },
            blankUsers: [],
            users: [],
            collections: [],
            images: [],

            selectedCategories: [],
            categorySearch: '',

            selectedBlankUsers: [],
            selectedUsers: [],
            peopleSearch: ''
        }
    }

    componentWillMount() {
        this.props.clearRelic();

        this.prevCategoryQuery = '';
        this.searchCategoriesDebounce = debounce(
            (query, callback) => {
                if (query && query !== this.prevCategoryQuery) {
                    apiSearchCategories(query, CATEGORIES_PER_PAGE)
                        .then(response => callback(null, response.categories));
                    this.prevCategoryQuery = query;
                }
            },
            500
        );
        this.prevPeopleQuery = '';
        this.searchPeoplesDebounce = debounce(
            (query, callback) => {
                if (query && query !== this.prevPeopleQuery) {
                    apiSearchUsers(query, true, true, true)
                        .then(response => {
                            const { users, blank_users } = response;
                            callback(null, users.concat(blank_users))
                        });
                    this.prevPeopleQuery = query;
                }
            },
            500
        );
    }

    onCreateClick() {
        const relic = this.state.relic;
        this.props
            .createRelic({
                ...relic,
                image_ids: R.pluck('id', this.state.images),
                blank_person_ids: R.pluck('id', this.state.selectedBlankUsers),
                person_ids: R.pluck('id', this.state.selectedUsers),
                category_ids: R.pluck('id', this.state.selectedCategories)
            })
            .then(action => {
                if (!action.error) {
                    this.props.history.pushState(null, `/@${action.payload.relic.owner.handle}/r/${action.payload.relic.permalink}`);
                }
            })
    }

    onInputChange(key, value) {
        if (value !== R.path(['relic', key], this.state)) {
            this.setState(R.assocPath(['relic', key], value, this.state));
        }
    }

    onChangeDate(changedPart, value) {
        if (value === '') {
            const parts = ['century', 'year', 'month', 'day'];
            const getDatePartLens = (key) => R.compose(R.lensProp('relic'), R.lensProp(`date_start_${key}`));
            this.setState(
                R.reduce(
                    (accum, partsItem) => R.over(
                        getDatePartLens(partsItem),
                        R.ifElse(
                            () => parts.indexOf(partsItem) >= parts.indexOf(changedPart),
                            R.always(''),
                            R.identity
                        ),
                        accum
                    ),
                    this.state,
                    parts
                )
            );
        } else {
            this.setState(
                R.assocPath(
                    ['relic', `date_start_${changedPart}`],
                    value,
                    this.state
                )
            );
        }
    }

    renderCategories() {
        const onChangeSearch = (query) => this.setState(R.assoc('categorySearch', query, this.state));
        const onCategorySelect = (category) => this.setState(
            R.compose(
                R.over(R.lensProp('selectedCategories'), R.compose(R.uniq, R.append(category))),
                R.set(R.lensProp('categorySearch'), '')
            )(this.state)
        );
        const onCategoryDeselect = (id) => this.setState(
            R.over(R.lensProp('selectedCategories'), R.reject(R.propEq('id', id)), this.state)
        );
        const onCategoryCreate = (query) => apiCreateCategory(query).then(onCategorySelect);

        return <CategoriesSelect searchCategories={(query) => apiSearchCategories(query, CATEGORIES_PER_PAGE)}
                                 selectedCategories={this.state.selectedCategories}
                                 categorySearch={this.state.categorySearch}
                                 onChangeSearch={onChangeSearch}
                                 onCreate={onCategoryCreate}
                                 onSelect={onCategorySelect}
                                 onDelete={onCategoryDeselect}/>
    }

    renderPeoples() {
        const onChangeSearch = (query) => this.setState(R.assoc('peopleSearch', query, this.state));
        const onPeopleSelect = (people) => this.setState(
            R.compose(
                R.over(
                    R.lensProp(people.handle ? 'selectedUsers' : 'selectedBlankUsers'),
                    R.compose(R.uniq, R.append(people))
                ),
                R.set(R.lensProp('peopleSearch'), '')
            )(this.state)
        );
        const onPeopleDeselect = (id) => this.setState(
            R.over(R.lensProp('selectedUsers'), R.reject(R.propEq('id', id)), this.state)
        );
        const onBlankPeopleDeselect = (id) => this.setState(
            R.over(R.lensProp('selectedBlankUsers'), R.reject(R.propEq('id', id)), this.state)
        );
        const onPeopleCreate = () => apiCreateBlankUser(this.state.peopleSearch).then(onPeopleSelect);

        return <PeoplesSelect selectedUsers={this.state.selectedUsers}
                              selectedBlankUsers={this.state.selectedBlankUsers}
                              peopleSearch={this.state.peopleSearch}
                              onGetSuggestions={(query, callback) => this.searchPeoplesDebounce(query, callback)}
                              onChangeSearch={(value) => onChangeSearch(value)}
                              onCreate={() => onPeopleCreate()}
                              onSelect={(people) => onPeopleSelect(people)}
                              onDeleteBlank={(id) => onBlankPeopleDeselect(id)}
                              onDelete={(id) => onPeopleDeselect(id)}/>
    }

    renderImages() {
        const LImages = R.lensProp('images');
        const onDropFile = (file) => apiCreateImage(file)
            .then(image => this.setState(
                R.over(
                    LImages,
                    R.append(image),
                    this.state
                )
            ));
        const onChangeImageOrder = (image, offset) => {
            const prevIndex = R.findIndex(R.equals(image), this.state.images);
            const nextIndex = (prevIndex + offset) < 0 ? 0 : prevIndex + offset;
            this.setState(
                R.over(
                    LImages,
                    R.compose(
                        R.insert(nextIndex, image),
                        R.reject(R.equals(image))
                    ),
                    this.state
                )
            );
        };
        const getImageIndexLensById = (imageId, images = []) => R.lensIndex(R.findIndex(R.propEq('id', imageId), images));
        const onRotateImage = (id, angle) => apiRotateImage(id, angle)
            .then(image => this.setState(
                R.set(
                    R.compose(LImages, getImageIndexLensById(id, this.state.images)),
                    image,
                    this.state
                )
            ));
        const onDeleteImage = (id) => apiDeleteImage(id)
            .then(image => this.setState(
                R.over(
                    LImages,
                    R.reject(R.propEq('id', id)),
                    this.state
                )
            ));

        return <div className="relic_new__add-photos">
            <ImagesList images={this.state.images}
                        onDropFile={onDropFile}
                        onMoveImage={onChangeImageOrder}
                        onRotateImage={onRotateImage}
                        onDeleteImage={onDeleteImage}/>
        </div>
    }

    render() {
        const { relic } = this.state;
        const { state } = this.props;
        const isFetching = state.self.isFetching;

        const classNames = classes({
            [this.props.className]: true,
            'relic_new': true,
            'is-fetching': isFetching
        });

        let errorsItems = null;
        if (state.self.errors.length > 0) {
            errorsItems = <div className="form-group">
                {state.self.errors.map((item, key) => <p key={key}>{item}</p>)}
            </div>
        }

        const titleMaxLength = 70;
        const descriptionMaxLength = 1500;

        const titleLengthLeft = titleMaxLength - relic.title.length;
        const descriptionLengthLeft = descriptionMaxLength - relic.description.length;

        return <div className={classNames}>
            <Link to="/">
                <div className="relic-popup-logo"/>
            </Link>

            <div className="relic_popup_container relic_popup_container__close-bg">
                <div className="relic-popup-close" onClick={() => this.props.history.go(-1)}></div>
                <div className="relic-popup-back" onClick={() => this.props.history.go(-1)}>
                    Вернуться
                </div>
                <div className="relic_popup_header">
                    <h1>Создание реликвы</h1>
                </div>

                <PrivacyDropdown privacy={relic.privacy}
                                 disabled={isFetching}
                                 onSelect={(privacy) => !isFetching && this.onInputChange('privacy', privacy)}/>

                <div className="form-group">
                    <label>Название</label>
                    <label className="counter">
                        {`${titleLengthLeft} ${declension(titleLengthLeft, ['символ', 'символа', 'символов'])}`}
                    </label>
                    <input className="form-control"
                           disabled={isFetching}
                           value={relic.title}
                           onChange={(e) => !isFetching && this.onInputChange('title', e.target.value)}
                           type="text"
                           ref="title"
                           maxLength={titleMaxLength}/>
                </div>

                <div className="form-group">
                    <label>Описание</label>
                    <label className="counter">
                        {`${descriptionLengthLeft} ${declension(descriptionLengthLeft, ['символ', 'символа', 'символов'])}`}
                    </label>
                    <textarea className="form-control"
                              disabled={isFetching}
                              value={relic.description}
                              onChange={(e) => !isFetching && this.onInputChange('description', e.target.value)}
                              rows="3"
                              ref="description"
                              maxLength={descriptionMaxLength}></textarea>
                </div>

                <div className="form-group">
                    <label>Фотографии</label>

                    <div>
                        {this.renderImages()}
                    </div>
                </div>

                <div className="form-group">
                    <label>Категории</label>

                    <div className="form-control">
                        {this.renderCategories()}
                    </div>
                </div>

                <div className="form-group">
                    <label>Дата</label>

                    <div className="form-control date">
                        <CenturySelect value={relic.date_start_century}
                                       onChange={(value) => this.onChangeDate('century', value)} />

                        <YearSelect value={relic.date_start_year}
                                    disabled={!relic.date_start_century}
                                    century={relic.date_start_century}
                                    onChange={(value) => this.onChangeDate('year', value)} />

                        <MonthSelect value={relic.date_start_month}
                                     disabled={!relic.date_start_year}
                                     onChange={(value) => this.onChangeDate('month', value)} />

                        <DaySelect value={relic.date_start_day}
                                   disabled={!relic.date_start_month}
                                   century={relic.date_start_century}
                                   year={relic.date_start_year}
                                   month={relic.date_start_month}
                                   onChange={(value) => this.onChangeDate('day', value)} />
                    </div>
                </div>

                <div className="form-group">
                    <label>Место</label>

                    <div className="form-control">
                        <input type="text"
                               value={this.state.relic.venue}
                               onChange={(e) => this.onInputChange('venue', e.target.value)}/>
                    </div>
                </div>

                <div className="form-group">
                    <label>Люди</label>

                    <div className="form-control">
                        {this.renderPeoples()}
                    </div>
                </div>

                {errorsItems}

                <button disabled={isFetching}
                        className="relic_new__button relic_new__button--violet"
                        onClick={() => !isFetching && this.onCreateClick()}>
                    Создать
                </button>
            </div>
        </div>
    }
}
