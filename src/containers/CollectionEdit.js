import React, { PropTypes, findDOMNode } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import R from 'ramda';
import classes from 'classnames';
import DocMeta from 'react-document-meta';
import stripHtml from '../utils/stripHtml';

import { loadCollection, updateCollection, deleteCollection } from '../reducers/collectionEdit';
import { closeModal } from '../reducers/app';
import stripHTML from '../utils/stripHtml';
import PrivacyDropdown from '../components/PrivacyDropdown';

// FIXME !!! рефакторинг. Вынести получение данных в редьюсер.
// 1) Решить проблему с пагинацией реликв и списком отмеченых реликв.
// Если с сервера приходит страница, то редьюсер в компонент передает уже объединенный списки реликв.
// Так что отметки или сбросятся или не проставятся только что полученным реликвам.
// 2) Если не все страницы реликв загружены, то незагруженные будут считаться неотмеченными
// В итоге список реликв пока убран

const RELICS_PER_PAGE = 20;

@connect(
    (state) => ({
        state: state.states.collectionEdit,
        collection: state.collectionEdit.collection,
        permalink: state.router.params.permalink,
        currentUser: state.app.currentUser
    }),
    (dispatch) => bindActionCreators({
        loadCollection,
        updateCollection,
        deleteCollection
    }, dispatch)
)
export class CollectionEdit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            collection: {
                title: '',
                description: '',
                privacy: 'public'
            }
        }
    }

    componentWillMount() {
        this.props
            .loadCollection(this.props.permalink, RELICS_PER_PAGE)
            .then(action => {
                if (action.error && action.payload.status === 404) {
                    this.props.history.pushState(null, '/not-found');
                }
                if (!action.error) {
                    const collection = action.payload.collection.collection;
                    if (collection.owner.id !== this.props.currentUser.id) {
                        this.props.history.pushState(null, '/not-found');
                    } else {
                        this.setState({collection})
                    }
                }
            });
    }

    onSaveClick() {
        this.props
            .updateCollection(this.props.permalink, this.state.collection)
            .then(action => {
                if (!action.error) {
                    this.setState({collection: action.payload.collection})
                }
            })
    }

    onDeleteClick() {
        if (confirm(`Вы уверены что хотите удалить коллекцию "${stripHTML(this.props.collection.title)}"?`)) {
            this.props
                .deleteCollection(this.props.permalink)
                .then(action => {
                    if (!action.error) {
                        this.props.history.pushState(null, `/@${this.props.currentUser.handle}`);
                    }
                })
        }
    }

    onInputChange(key, value) {
        this.setState(R.assocPath(['collection', key], value, this.state));
    }

    render() {
        const { collection } = this.state;
        const { state } = this.props;
        const isFetching = state.self.isFetching;

        const classNames = classes({
            [this.props.className]: true,
            'new-collection': true,
            'is-fetching': isFetching
        });

        let errorsItems = null;
        if (state.self.errors.length > 0) {
            errorsItems = <div className="form-group">
                {state.self.errors.map((item, key) => <p key={key}>{item}</p>)}
            </div>
        }

        const onChangePrivacy = (privacy) => this.setState(R.assocPath(['collection', 'privacy'], privacy, this.state));

        return <div className={classNames}>
            <DocMeta title={`Редактирование ${stripHtml(collection.title)}`} />
            <h4>Редактирование коллекции "<span dangerouslySetInnerHTML={{__html: this.props.collection.title}}></span>"</h4>

            <PrivacyDropdown privacy={collection.privacy}
                             disabled={isFetching}
                             onSelect={(privacy) => onChangePrivacy(privacy)}/>

            <div className="form-group">
                <label>Название</label>
                <input className="form-control"
                       type="text"
                       value={stripHTML(collection.title)}
                       disabled={isFetching}
                       onChange={(e) => !isFetching && this.onInputChange('title', e.target.value)}/>
            </div>

            <div className="form-group">
                <label>Описание</label>
                <textarea className="form-control"
                          rows="3"
                          value={stripHTML(collection.description)}
                          disabled={isFetching}
                          onChange={(e) => !isFetching && this.onInputChange('description', e.target.value)}></textarea>
            </div>

            {errorsItems}

            <button className="btn btn-rectangle big" onClick={() => !isFetching && this.onSaveClick()}>
                Сохранить
            </button>
            <button className="btn btn-rectangle big" onClick={() => !isFetching && this.onDeleteClick()}>
                Удалить
            </button>
        </div>
    }
}