import React, { PropTypes, findDOMNode } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import R from 'ramda';
import DocMeta from 'react-document-meta';
import stripHtml from '../utils/stripHtml';

import PrivacyDropdown from '../components/PrivacyDropdown';
import CategoryLinks from '../components/CategoryLinks';
import RelicNumber from '../components/RelicNumber';
import SocialButtons from '../components/SocialButtons';
import LikeButton from '../components/LikeButton';
import CommentsList from './../components/CommentsList';
import InvestigationsList from './../components/InvestigationsList';
import User from '../components/User';
import PhotoGallery from './../components/PhotoGallery';
import NewInvestigation from './../components/NewInvestigation';
import { AddToCollection } from './AddToCollection';
import Preloader from '../components/Preloader';
import Datetime from '../components/Datetime';

import {
    loadRelic,
    postRelicComment,
    loadInvestigation,
    postInvestigationComment,
    createInvestigation,
    deleteInvestigation,
    setRelicPrivacy
} from '../reducers/relic';
import { loadSharingCounter } from '../reducers/sharings';
import { openModal, setLike } from '../reducers/app';

@connect(
    (state) => ({
        permalink: state.router.params.permalink,
        commentReplyErrors: state.app.commentReplyErrors,
        newInvestigationErrors: state.app.newInvestigationErrors,
        currentUser: state.app.currentUser,
        relic: state.relic,
        state: state.states.relic,
        sharing: state[state.router.location.pathname],
        pathname: state.router.location.pathname
    }),
    (dispatch) => bindActionCreators({
        loadRelic,
        postRelicComment,
        postInvestigationComment,
        createInvestigation,
        deleteInvestigation,
        openModal,
        loadSharingCounter,
        setLike,
        setRelicPrivacy
    }, dispatch)
)
export class Relic extends React.Component {
    componentWillMount() {
        this.props
            .loadRelic(this.props.routeParams.permalink)
            .then(action => {
                if (action.error && action.payload.status === 404) {
                    this.props.history.pushState(null, '/not-found');
                }
            });
        this.props.loadSharingCounter(this.props.pathname);
    }

    render() {
        const { relic, state, pathname } = this.props;

        if (state.self.isFetching) {
            return <div className="popUp relic_popup">
                <div className="relic_popup_container relic_popup_container__close-bg">
                    <Preloader />
                </div>
            </div>;
        }

        if (state.self.errors.length) {
            return <div className="popUp relic_popup">
                <div className="relic_popup_container relic_popup_container__close-bg">
                    <p>Ошибки при загрузке реликвы:</p>
                    {state.self.errors.map((index, key) => <p key={key}>{index}</p>)}
                </div>
            </div>;
        }

        if (R.isEmpty(R.values(relic))) {
            return <div>Реликва пуста</div>;
        }

        const onOpenAddToCollectionModal = () => this.props.openModal(<AddToCollection />);

        const onPostComment = (body) => this.props.postRelicComment(relic.id, false, body);
        const onPostReply = (parentId, body) => this.props.postRelicComment(relic.id, parentId, body);
        const onToggleRelicLike = () => this.props.setLike('relic', relic.id, !relic.like);
        const commentators = relic.commentators.concat(this.props.currentUser);
        const commentReplyState = state.discussionReply;

        const onDeleteInvestigation = (id) => this.props.deleteInvestigation(id);
        const onCreateInvestigation = (body) => this.props.createInvestigation(relic.id, body);
        const onPostInvestigationComment = (invId, body) => this.props.postInvestigationComment(invId, body);
        const onToggleInvestigationLike = (invId, like) => this.props.setLike('investigation', invId, like);
        const isOwnRelic = this.props.currentUser.url === relic.owner.url;
        const newInvestigationStatus = state.newInvestigation;
        const investigationsStatuses = state.investigations;

        const onPrivacyChange = (privacy) => this.props.setRelicPrivacy(relic.id, privacy);

        return <div className="popup-wrapper__modal-page">
            <DocMeta title={stripHtml(relic.title)} />
            <div className="relic-popup">
                <Link to="/">
                    <div className="relic-popup-logo" />
                </Link>
                <div className="relic_popup_container relic_popup_container__close-bg">
                    <div className="relic-popup-close" onClick={() => this.props.history.go(-1)}></div>
                    <div className="relic-popup-back" onClick={() => this.props.history.go(-1)}>
                        Вернуться
                    </div>
                    <div className="relic_popup_header">
                        <CategoryLinks categories={relic.categories}/>
                        <RelicNumber number={relic.number}/>
                        <h1 className="relic_popup_title" dangerouslySetInnerHTML={{__html: relic.title}}></h1>
                        <Link className="media autor user" rel="autor" to={`/@${relic.owner.handle}`}>
                            <img className="pull-left x60" alt={relic.owner.url.substring(1, relic.owner.url.length)}
                                 src={relic.owner.avatar} width="30" height="30"/>
                            <strong className="media-body">{relic.owner.name}</strong>
                        </Link>

                        <Datetime className="relic_popup_date" datetime={relic.created_at} />

                        {isOwnRelic && <div className="pull-right">
                            <Link className="relic-popup__edit"
                                  to={`${pathname}${pathname.substr(-1) === '/' ? '' : '/'}edit`}>
                                Редактировать
                            </Link>
                            <span className="relic-popup__dot">⋅</span>
                            <PrivacyDropdown style={{display: 'inline-block'}}
                                             privacy={relic.privacy}
                                             onSelect={onPrivacyChange}/>
                        </div>}
                    </div>

                    <PhotoGallery images={relic.images}/>

                    <div className="relic_show_description relic_popup_description">
                        <p dangerouslySetInnerHTML={{__html: relic.description}}></p>
                    </div>
                    <div className="relic_popup_info">
                        <ul className="relic_show_details">
                            {this.renderCategories()}
                            {this.renderPeoples()}
                            {this.renderDateAndPlace()}
                        </ul>
                        {this.renderCollections()}
                    </div>
                    <ul className="relic-popup-actions">
                        <li className="relic-popup-actions__item relic-popup-actions__like">
                            <LikeButton count={relic.likes_count}
                                        isLiked={relic.like}
                                        onLike={onToggleRelicLike}/>
                        </li>
                        <li className="relic-popup-actions__item relic-popup-actions__social">
                            <SocialButtons {...this.props.sharing} title={relic.title} url={relic.url}/>
                        </li>
                        <li className="relic-popup-actions__item relic-popup-actions__collection">
                        <span onClick={onOpenAddToCollectionModal}
                              className="btn-add-to-collection">Добавить в коллекцию
                        </span>
                        </li>
                    </ul>
                </div>
            </div>

            <div className="relic-popup-hr" />

            <div className="relic-popup">
                <div className="relic_popup_container">
                    <section className="relic_show_discussions">
                        <h3 className="relic_show_header_l">
                            Комментарии
                        </h3>

                        <CommentsList users={commentators}
                                      comments={relic.discussions}
                                      replyState={commentReplyState}
                                      onPostComment={onPostComment}
                                      onPostReply={onPostReply}/>
                    </section>
                </div>
            </div>

            <div className="relic-popup-hr" />

            <div className="relic-popup">
                <div className="relic_popup_container">
                    <section className="relic_show_investigations">
                        <h3 className="relic_show_header_l">
                            Расследования
                        </h3>

                        {isOwnRelic &&
                        <NewInvestigation {...newInvestigationStatus}
                            onCreateInvestigation={onCreateInvestigation}/>}

                        <InvestigationsList users={commentators}
                                            comments={relic.investigation_comments}
                                            investigations={relic.investigations}
                                            investigationsStatuses={investigationsStatuses}
                                            onToggleInvestigationLike={onToggleInvestigationLike}
                                            onPostComment={onPostInvestigationComment}
                                            onDeleteInvestigation={onDeleteInvestigation}/>
                    </section>
                </div>
            </div>
            {this.props.children}
        </div>;
    }

    renderCollections() {
        if (!this.props.relic.collections || !this.props.relic.collections.length) {
            return null
        }

        return <div className="relic_show_details second_row">
            <span className="name">Коллекции:</span>

            <div className="relic_show_details_content">
            </div>
        </div>
    }

    renderDateAndPlace() {
        const { relic } = this.props;

        const venue = relic.venue;
        const date = relic.date;

        let content = false;

        if (!venue && !date) {
            return null;
        } else if (!venue) {
            content = <div className="relic_show_details_content">
                <Link to={`/search?q=${date}`}>{date}</Link>
            </div>;
        } else if (!date) {
            content = <div className="relic_show_details_content">
                <Link to={`/search?q=${venue}`}>{venue}</Link>
            </div>;
        } else {
            content = <div className="relic_show_details_content">
                <Link to={`/search?q=${date}`}>
                    {`${date}, `}
                </Link>
                <Link to={`/search?q=${venue}`}>
                    {venue}
                </Link>
            </div>;
        }

        return <li>
            <span className="name">Дата и место: </span>
            {content}
        </li>;
    }

    renderCategories() {
        if (!this.props.relic.categories || !this.props.relic.categories.length) {
            return null
        }

        return <li>
            <span className="name">Категории: </span>

            <div className="relic_show_details_content">
                <CategoryLinks categories={this.props.relic.categories}/>
            </div>
        </li>
    }

    renderPeoples() {
        const { relic } = this.props;

        if (relic.peoples.length == 0) {
            return null
        }
        return <li>
            <span className="name">Люди: </span>

            <div className="relic_show_details_content">
                {relic.peoples.map((people, key) => {
                    return (<Link to={`/search?q=${people.name}`} key={key}>
                        {people.name}
                        { key === relic.peoples.length - 1 ? null : ", "}
                    </Link>
                    )

                })}
            </div>
        </li>;
    }
}
