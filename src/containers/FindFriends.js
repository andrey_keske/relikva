import React, { PropTypes, findDOMNode } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import R from 'ramda';
import DebounceInput from 'react-debounce-input';
import DocMeta from 'react-document-meta';

import SubscriptionButton from '../components/SubscriptionButton';
import { searchUsers, loadRecommended } from '../reducers/findFriends';
import { setSubscribe } from '../reducers/app';
import User from '../components/User';

const INITIAL_FRIENDS_COUNT = 10;

@connect(
    (state) => ({
        users: state.findFriends.users,
        query: R.path(['router', 'location', 'query', 'q'], state) || '',
        pathname: state.router.location.pathname
    }),
    (dispatch) => bindActionCreators({
        loadRecommended,
        searchUsers,
        setSubscribe
    }, dispatch)
)
export class FindFriends extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1
        };
    }

    componentWillMount() {
        const currentQuery = this.props.query;
        if (currentQuery) {
            this.props.searchUsers(currentQuery, 1);
        } else {
            this.props.loadRecommended(INITIAL_FRIENDS_COUNT);
        }
    }

    componentWillReceiveProps(nextProps) {
        const newQuery = nextProps.query;
        const currentQuery = this.props.query;
        if (newQuery && currentQuery !== newQuery) {
            this.props.searchUsers(newQuery, 1);
            this.setState({
                page: 1
            });
        }
    }

    onKeyDown(e) {
        if (e.which === 13) {
            const currentQuery = this.props.query;
            const newQuery = findDOMNode(this.refs.query).value;
            if (currentQuery !== newQuery) {
                this.props.history.pushState(null, this.props.pathname, {q: newQuery})
            }
        }
    }

    onMoreClick() {
        const query = this.props.query;
        if (query) {
            this.props.searchUsers(query, this.state.page + 1);
            this.setState({
                page: this.state.page + 1
            });
        } else {
            this.props.loadRecommended(INITIAL_FRIENDS_COUNT);
        }
    }

    onQueryChange(value) {
        this.props.history.pushState(null, this.props.pathname, {q: value})
    }

    render() {
        const { setSubscribe, query, users } = this.props;

        const usersList = users.map((user, key) =>
                <div key={key} className="subscription_block__user user-follow__item">
                    <User user={user} />
                    <SubscriptionButton onSubscribe={() => setSubscribe(user.handle, !user.follow)} user={user} />
                </div>
        );

        return <div className="container user-follow you-know-them">
            <DocMeta title="Поиск по людям" />
            <h1 className="user-follow__header">Возможно, вы их знаете:</h1>

            <div className="icon-search user-follow__search">
                <DebounceInput ref="query"
                               onKeyDown={(e) => this.onKeyDown(e)}
                               className="form-control"
                               minLength={2}
                               placeholder="Поиск по людям"
                               value={query}
                               debounceTimeout={500}
                               onChange={(value) => this.onQueryChange(value)} />
            </div>
            <div className="user-follow__items">
                {usersList}
            </div>

            <div className="bottom-buttons">
                <button className="btn-rectangle big" onClick={() => this.onMoreClick()}>
                    Посмотреть еще
                </button>
                <Link className="btn-rectangle big" to="/">
                    Перейти на главную
                </Link>
            </div>
        </div>
    }
}
