import React, { PropTypes } from 'react';

import DocMeta from 'react-document-meta';
import stripHtml from '../utils/stripHtml';

import { apiGetAboutPage } from '../utils/api';

export class About extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            content: ''
        }
    }

    componentWillMount() {
        apiGetAboutPage().then(response => this.setState(response));
    }

    render() {
        return <div>
            <DocMeta title={stripHtml(this.state.title)} />
            <div dangerouslySetInnerHTML={{ __html: this.state.content }}></div>
        </div>;
    }
}
