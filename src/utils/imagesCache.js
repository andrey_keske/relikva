export default class ImagesCache {
    constructor() {
        this.collection = new Set()
    }

    add(src, callback) {
        let img = new Image();

        img.onload = () => {
            this.collection.add(src);
            callback();
        };

        img.src = src;

        if (img.complete) {
            img.onload = undefined;
            callback();
        }
    }

    touch(src, callback) {
        if (this.collection.has(src)) {
            callback()
        } else {
            this.add(src, callback)
        }
    }
}