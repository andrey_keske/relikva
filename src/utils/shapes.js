import { PropTypes } from 'react';

export const userShortShape = PropTypes.shape({
    id: PropTypes.number,
    avatar: PropTypes.string,
    can_follow: PropTypes.bool,
    follow: PropTypes.oneOf([true, false, null]),
    handle: PropTypes.string,
    name: PropTypes.string,
    url: PropTypes.string
});

export const commentShape = PropTypes.shape({
    id: PropTypes.number,
    body: PropTypes.string,
    commentator_id: PropTypes.number,
    created_at: PropTypes.string,
    likes_count: PropTypes.number,
    parent_id: PropTypes.any
});

export const collectionShortShape = PropTypes.shape({
    teaser: PropTypes.string,
    title: PropTypes.string,
    user: userShortShape,
    likes_count: PropTypes.number
});

export const categoryLinkShape = PropTypes.shape({
    title: PropTypes.string,
    url: PropTypes.string
});

export const investigationShortShape = PropTypes.shape({
    id: PropTypes.number,
    url: PropTypes.string,
    title: PropTypes.string,
    teaser: PropTypes.string,
    childrens_count: PropTypes.number,
    user: userShortShape
});

export const galleryImageShape = PropTypes.shape({
    x100: PropTypes.string,
    x600: PropTypes.string
});

export const relicShortShape = PropTypes.shape({
    id: PropTypes.number,
    url: PropTypes.string,
    title: PropTypes.string,
    teaser: PropTypes.string,
    user: userShortShape,
    categories: PropTypes.arrayOf(categoryLinkShape),
    description: PropTypes.string,
    likes_count: PropTypes.number,
    discussions_count: PropTypes.number
});

export const userShape = PropTypes.shape({
    id: PropTypes.number,
    avatars: PropTypes.object,
    can_follow: PropTypes.bool,
    follow: PropTypes.oneOf([true, false, null]),
    handle: PropTypes.string,
    name: PropTypes.string,
    url: PropTypes.string,
    followers_count: PropTypes.number,
    following_count: PropTypes.number
});

export const componentStateShape = PropTypes.shape({
    isFetching: PropTypes.bool,
    errors: PropTypes.arrayOf(PropTypes.string)
});

export const activityFeedItemShape = PropTypes.shape({
    id: PropTypes.number,
    description: PropTypes.string,
    created_at: PropTypes.string,
    newest: PropTypes.bool,
    sender: userShortShape,
    resource: PropTypes.shape({
        id: PropTypes.number,
        type: PropTypes.string,
        url: PropTypes.string,
        teaser: PropTypes.string
    })
});

export const paginatedShape = PropTypes.shape({
    newest_items: PropTypes.any,
    all_items: PropTypes.any,
    current_page: PropTypes.any,
    next_page: PropTypes.any,
    prev_page: PropTypes.any,
    total_pages: PropTypes.any,
    total_count: PropTypes.any
});