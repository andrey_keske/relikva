export function getElementOffset(element, parent = document.body) {
    var parentRect = parent.getBoundingClientRect();
    var elemRect = element.getBoundingClientRect();
    return elemRect.top - parentRect.top;
}