import Promise from 'bluebird';

const isPromise = obj => obj && typeof obj.then === 'function';
const hasPromiseProps = (obj = {}) => Object.keys(obj).some(key => isPromise(obj[key]));

const resolveProps = obj => {
    const props = Object.keys(obj);
    const values = props.map(prop => obj[prop]);

    return Promise.all(values).then(resolvedArray => {
        return props.reduce((acc, prop, index) => {
            acc[prop] = resolvedArray[index];
            return acc;
        }, {});
    });
};

const getNonPromiseProperties = obj => {
    return Object.keys(obj).filter(key => !isPromise(obj[key])).reduce((acc, key) => {
        acc[key] = obj[key];
        return acc;
    }, {});
};

export default function promisePropsMiddleware() {
    return next => action => {
        const { type, payload, meta } = action;
        if (!type || !hasPromiseProps(action.payload)) {
            return next(action);
        }

        const nonPromiseProperties = getNonPromiseProperties(payload);

        next({
            type: type.concat('_REQUEST'),
            meta: {...meta, ...nonPromiseProperties}
        });
        return resolveProps(payload).then(
            (results) => next({
                type: type.concat('_SUCCESS'),
                payload: {...results},
                meta: {...meta, ...nonPromiseProperties}
            }),
            (error) => next({
                type: type.concat('_FAILURE'),
                error: true,
                payload: error,
                meta: {...meta, ...nonPromiseProperties}
            })
        );
    };
}