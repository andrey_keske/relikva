import request from 'superagent-bluebird-promise';
import Promise from 'bluebird';

let csrfToken = document.querySelector('meta[name="csrf-token"]').content;

export const apiGetRelics = (count, page, friends, popular) =>
    request
        .get('/api/v1/timelines/relics')
        .query({count, page, friends, popular})
        .promise()
        .then(response => response.body.relics);

export const apiGetCollections = (count, page, friends, popular) =>
    request
        .get('/api/v1/timelines/collections')
        .query({count, page, friends, popular})
        .promise()
        .then(response => response.body.collections);

export const apiGetInvestigations = (count, page, friends, popular) =>
    request
        .get('/api/v1/timelines/investigations')
        .query({count, page, friends, popular})
        .promise()
        .then(response => response.body.investigations);

export const apiGetRelic = (id) =>
    request
        .get(`/api/v1/relics/${id}`)
        .promise()
        .then(response => response.body.relic)
        .error(err => {
            if (err.status === 404) {
                err.message = 'Реликва не найдена';
                throw err;
            }
            throw err;
        });

export const apiGetInvestigation = (id) =>
    request
        .get(`/api/v1/investigations/${id}`)
        .promise()
        .then(response => response.body.investigation);

export const apiPostInvestigationComment = (id, body) =>
    request
        .post(`/api/v1/investigations/${id}/replay`)
        .set('X-CSRF-TOKEN', csrfToken)
        .send({investigation_comment: { body }})
        .promise()
        .then(response => response.body.investigation_comment);

export const apiCreateInvestigation = (id, body) =>
    request
        .post(`/api/v1/relics/${id}/investigations`)
        .set('X-CSRF-TOKEN', csrfToken)
        .send({investigation: { body }})
        .promise()
        .then(response => response.body.investigation);

export const apiDeleteInvestigation = (id) =>
    request
        .del(`/api/v1/investigations/${id}`)
        .set('X-CSRF-TOKEN', csrfToken)
        .promise();

export const apiGetSelfCollections = (count, page) =>
    request
        .get(`/api/v1/self/collections?page=${page}&count=${count}`)
        .promise()
        .then(response => response.body);

export const apiCreateCollection = (params) =>
    request
        .post(`/api/v1/collections/`)
        .set('X-CSRF-TOKEN', csrfToken)
        .send({collection: params})
        .promise()
        .then(response => response.body.collection);

export const apiUpdateCollection = (id, params) =>
    request
        .put(`/api/v1/collections/${id}`)
        .set('X-CSRF-TOKEN', csrfToken)
        .send({collection: params})
        .promise()
        .then(response => response.body.collection);

export const apiUpdateRelic = (id, params) =>
    request
        .put(`/api/v1/relics/${id}`)
        .set('X-CSRF-TOKEN', csrfToken)
        .send({relic: params})
        .promise()
        .then(response => response.body.relic);

export const apiAddRelicToCollection = (permalink, collectionId) =>
    request
        .post(`/api/v1/collections/${collectionId}/selected`)
        .query({ permalink })
        .set('X-CSRF-TOKEN', csrfToken)
        .promise()
        .then(response => response.body);

export const apiDeleteRelicFromCollection = (permalink, collectionId) =>
    request
        .del(`/api/v1/collections/${collectionId}/selected`)
        .query({ permalink })
        .set('X-CSRF-TOKEN', csrfToken)
        .promise()
        .then(response => response.body);

export const apiGetRecommendedFriends = (count) =>
    request
        .get(`/api/v1/find_friends/recommendation?count=${count}`)
        .promise()
        .then(response => response.body.users || response.body.organizations);

export const apiSearchFriends = (q, page) =>
    request
        .get(`/api/v1/find_friends/search`)
        .query({ q, page })
        .promise()
        .then(response => response.body.users);

export const apiPostRelicComment = (id, body) =>
    request
        .post(`/api/v1/relics/${id}/comments`)
        .set('X-CSRF-TOKEN', csrfToken)
        .send({comment: { body }})
        .promise()
        .then(response => response.body.comment);

export const apiPostCommentReply = (parentId, body) =>
    request
        .post(`/api/v1/comments/${parentId}/replay`)
        .set('X-CSRF-TOKEN', csrfToken)
        .send({comment: { body }})
        .promise()
        .then(response => response.body.comment);

export const apiLoadUser = (username) =>
    request
        .get(`/api/v1/users/${username}`)
        .promise()
        .then(response => response.body.user);

export const apiLoadUserFollowing = (username, count, page) =>
    request
        .get(`/api/v1/users/:user_handle/followers`)
        .query({ page, count })
        .promise()
        .then(response => response.body.user);

export const apiGetUserRelics = (username, count, page) =>
    request
        .get(`/api/v1/users/${username}/relics`)
        .query({ page, count })
        .promise()
        .then(response => response.body);

export const apiGetUserCollections = (username, count, page) =>
    request
        .get(`/api/v1/users/${username}/collections`)
        .query({ page, count })
        .promise()
        .then(response => response.body);

export const apiGetCollection = (permalink, count, page) =>
    request
        .get(`/api/v1/collections/${permalink}`)
        .query({ page, count })
        .promise()
        .then(response => response.body)
        .error(err => {
            if (err.status === 404) {
                err.message = 'Коллекция не найдена';
                throw err;
            }
            if (err.status === 500) {
                throw new Error('Ошибка на сервере')
            }
            return err;
        });

export const apiSetLike = (type, id, nextValue) => {
    const entityToPath = {
        relic: 'relics',
        investigation: 'investigations',
        collection: 'collections'
    };
    const path = entityToPath[type];
    if (!path) {
        return Promise.reject('Неправильно указана запись для лайка');
    }
    return request[nextValue ? 'post' : 'del'](`/api/v1/${path}/${id}/like`)
        .set('X-CSRF-TOKEN', csrfToken)
        .promise()
        .then(response => response.body);
};

export const apiSetSubscribe = (username, nextValue) =>
    request[nextValue ? 'post' : 'del'](`/api/v1/users/${username}/follow`)
        .set('X-CSRF-TOKEN', csrfToken)
        .promise()
        .then(response => response.body);

export const apiLoadSharingCounter = (url) =>
    request
        .get(`/sharing`)
        .query({ url })
        .promise()
        .then(response => response.body);

export const apiLoadActivityFeedItems = (count, page, newest) =>
    request
        .get(`/api/v1/activity_feed`)
        .query({ page, count, newest })
        .promise()
        .then(response => response.body);

export const apiMarkActivityFeedItems = (ids) =>
    request
        .post(`/api/v1/activity_feed/reviewed`)
        .set('X-CSRF-TOKEN', csrfToken)
        .send({ ids })
        .promise()
        .then(response => response.body);

export const apiUpdateActivityFeedCounter = () =>
    request
        .get(`/api/v1/activity_feed/count`)
        .promise()
        .then(response => response.body);

export const apiGetSelfRelics = (count, page) =>
    request
        .get(`/api/v1/self/relics`)
        .query({ page, count })
        .promise()
        .then(response => response.body);

export const apiGetSelfProfile = () =>
    request
        .get(`/api/v1/self`)
        .promise()
        .then(response => response.body.user);

export const apiUpdateSelfProfile = (user) =>
    request
        .put(`/api/v1/self`)
        .set('X-CSRF-TOKEN', csrfToken)
        .send({ user })
        .promise()
        .then(response => response.body.user);

export const apiUpdatePassword = (password, newPassword, newPasswordConfirm) =>
    request
        .post(`/api/v1/self/update_password`)
        .set('X-CSRF-TOKEN', csrfToken)
        .send({
            user: {
                password,
                new_password: newPassword,
                new_password_confirm: newPasswordConfirm
            }
        })
        .promise()
        .then(response => response.body);


export const apiUpdateAvatar = (avatar) => {
    let form = new FormData();
    form.append('file', avatar);

    return request
        .post(`/api/v1/self/avatar`)
        .set('X-CSRF-TOKEN', csrfToken)
        .send(form)
        .promise()
        .then(response => response.body.user);
};

export const apiDeleteAvatar = () =>
    request
        .del(`/api/v1/self/avatar`)
        .set('X-CSRF-TOKEN', csrfToken)
        .promise()
        .then(response => response.body.user);

export const apiDeleteCollection = (id) =>
    request
        .del(`/api/v1/collections/${id}`)
        .set('X-CSRF-TOKEN', csrfToken)
        .promise();

export const apiLogout = () =>
    request
        .del(`/logout`)
        .set('X-CSRF-TOKEN', csrfToken)
        .promise();

export const apiSearchRelics = (query, count, page, friends, from, to) =>
    request
        .get(`/api/v1/search/relics`)
        .query({
            q: query,
            f: from,
            t: to,
            count,
            page,
            friends
        })
        .promise()
        .then(response => response.body);

export const apiSearchCollections = (query, count, page, friends) =>
    request
        .get(`/api/v1/search/collections`)
        .query({
            q: query,
            count,
            page,
            friends
        })
        .promise()
        .then(response => response.body);

export const apiSearchCategories = (query, count, page) =>
    request
        .get(`/api/v1/search/categories`)
        .query({
            q: query,
            count,
            page
        })
        .promise()
        .then(response => response.body);

export const apiSearchUsers = (query, friends, exclude_self, include_blank) =>
    request
        .get(`/api/v1/search/users`)
        .query({
            q: query,
            friends,
            exclude_self,
            include_blank
        })
        .promise()
        .then(response => response.body);

export const apiCreateRelic = (params) =>
    request
        .post(`/api/v1/relics`)
        .set('X-CSRF-TOKEN', csrfToken)
        .send({ relic: params })
        .promise()
        .then(response => response.body.relic);

export const apiCreateBlankUser = (name, email) =>
    request
        .post(`/api/v1/blank_users`)
        .set('X-CSRF-TOKEN', csrfToken)
        .send({ blank_user: { name, email } })
        .promise()
        .then(response => response.body.blank_user);

export const apiUpdateBlankUser = (id, name, email) =>
    request
        .put(`/api/v1/blank_users/${id}`)
        .set('X-CSRF-TOKEN', csrfToken)
        .send({ blank_user: params })
        .promise()
        .then(response => response.body);

export const apiCreateImage = (file) => {
    let form = new FormData();
    form.append('image[file]', file);
    form.append('image[rotate_degrees]', 0);

    return request
        .post(`/api/v1/images`)
        .set('X-CSRF-TOKEN', csrfToken)
        .send(form)
        .promise()
        .then(response => response.body.image)
};

export const apiRotateImage = (id, angle) =>
    request
        .patch(`/api/v1/images/${id}`)
        .set('X-CSRF-TOKEN', csrfToken)
        .send({image: { rotate_degrees: angle }})
        .promise()
        .then(response => response.body.image);

export const apiDeleteImage = (id) =>
    request
        .del(`/api/v1/images/${id}`)
        .set('X-CSRF-TOKEN', csrfToken)
        .promise()
        .then(response => response.body);

export const apiCreateCategory = (name) =>
    request
        .post(`/api/v1/categories`)
        .set('X-CSRF-TOKEN', csrfToken)
        .send({ category: { name }})
        .promise()
        .then(response => response.body.category);

export const apiDeleteRelic = (id) =>
    request
        .del(`/api/v1/relics/${id}`)
        .set('X-CSRF-TOKEN', csrfToken)
        .promise()
        .then(response => response.body);

export const apiGetAboutPage = () =>
    request
        .get(`/about.json`)
        .promise()
        .then(response => response.body);