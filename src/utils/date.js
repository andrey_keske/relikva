import moment from 'moment';

export default (date, format = 'LL') => {
    const momentDate = moment(date);
    const daysDiff = moment().diff(momentDate, 'days');

    if (daysDiff == 0) {
        return 'Сегодня';
    } else if (daysDiff == 1) {
        return 'Вчера'
    } else {
        return momentDate.format(format);
    }
}