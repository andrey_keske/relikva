import React from 'react';
import { Route, IndexRoute } from 'react-router';
import { ReduxRouter } from 'redux-router';

import { App } from './containers/App';
import { Timeline } from './containers/Timeline';
import { User } from './containers/User';
import { Relic } from './containers/Relic'
import { Collection } from './containers/Collection'
import { FindFriends } from './containers/FindFriends'
import { CollectionNew } from './containers/CollectionNew'
import { CollectionEdit } from './containers/CollectionEdit'
import { AddToCollection } from './containers/AddToCollection';
import { SelfProfile } from './containers/SelfProfile';
import { About } from './containers/About';
import { NotFound } from './containers/NotFound';
import { Search } from './containers/Search';
import { RelicNew } from './containers/RelicNew';
import { RelicEdit } from './containers/RelicEdit';

export class Router extends React.Component {
    render() {
        return <ReduxRouter>
            <Route path="/" component={App}>
                <IndexRoute component={Timeline}/>
                <Route path="/new/collection" component={CollectionNew}/>
                <Route path="/new/relic" component={RelicNew}/>
                <Route path="/find-friends" component={FindFriends}/>
                <Route path="/@:username" component={User}/>
                <Route path="/(@:username/)c/:permalink" component={Collection}/>
                <Route path="/(@:username/)c/:permalink/edit" component={CollectionEdit}/>
                <Route path="/(@:username/)r/:permalink" component={Relic}/>
                <Route path="/(@:username/)r/:permalink/edit" component={RelicEdit}/>
                <Route path="/profile" component={SelfProfile}/>
                <Route path="/about" component={About}/>
                <Route path="/search" component={Search}/>
                <Route path="/not-found" component={NotFound}/>
                <Route path="*" component={NotFound}/>
            </Route>
        </ReduxRouter>;
    }
}