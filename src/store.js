import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import thunk from 'redux-thunk';
import async from './utils/asyncMiddleware';
import createLogger from 'redux-logger';
import { routerStateReducer, reduxReactRouter } from 'redux-router';
import createPushStateHistory from 'history/lib/createBrowserHistory';
import createHashHistory from 'history/lib/createHashHistory';
import R from 'ramda';

let createStoreWithMiddleware;

if (__PROD__) {
    createStoreWithMiddleware = applyMiddleware(
        thunk,
        async
    )(createStore);
} else {
    createStoreWithMiddleware = applyMiddleware(
        thunk,
        async,
        createLogger({
            level: 'info',
            collapsed: true
        })
    )(createStore);
}

import * as reducers from './reducers';

const finalReducers = combineReducers(
    R.mergeAll([
        {},
        reducers,
        {router: routerStateReducer}
    ])
);

const createHistory = document.location.pathname.indexOf('/react') === 0 ? createHashHistory : createPushStateHistory;

export default compose(
    reduxReactRouter({ createHistory })
)(createStoreWithMiddleware)(finalReducers);