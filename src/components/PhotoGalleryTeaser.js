import React, { PropTypes, findDOMNode } from 'react';

import ImagesCache from '../utils/imagesCache';
import { galleryImageShape } from '../utils/shapes';

const galleryCache = new ImagesCache;

export default class PhotoGalleryTeaser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            teaser: props.image.x600,
            preview: props.image.x100,
            teaserHeight: 0
        };
    }

    componentWillReceiveProps(nextProps) {
        const teaser = nextProps.image.x600;
        const preview = nextProps.image.x100;

        this.setState({
            teaser: preview,
            preview: preview
        });

        galleryCache.touch(teaser, () => this.setState({teaser: teaser}));
    }

    componentWillMount() {
        this.timer = setTimeout(() => this.teaserWillUpdate(), 100);
    }

    componentWillUnmount() {
        clearTimeout(this.timer);
    }

    componentWillUpdate() {
        clearTimeout(this.timer);
        this.timer = setTimeout(() => this.teaserWillUpdate(), 100);
    }

    teaserWillUpdate() {
        galleryCache.touch(this.props.image.x100, () => {
            const preview = this.refs.imgPreview;

            if (this.state.teaserHeight && (Math.abs(this.state.teaserHeight - preview.offsetHeight) < 10)) {
                return
            }
            this.setState({
                teaserHeight: preview.offsetHeight
            })
        })
    }

    render() {
        const { teaser, preview, teaserHeight } = this.state;

        return <div className="relic_gallery_teaser" ref="imgTeaser" style={{ height: teaserHeight }}>
            <div className="relic_gallery_teaser__box"
                 style={{backgroundImage: `url(${teaser})`}}>
            </div>
            <img ref="imgPreview" src={preview} alt="relikva.com"/>
        </div>
    }
};

PhotoGalleryTeaser.propTypes = {
    image: galleryImageShape.isRequired
};
