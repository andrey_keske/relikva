import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import classes from 'classnames';

import Waypoint from 'react-waypoint';

import Masonry from './Masonry';
import Preloader from './Preloader';

export default class Feed extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1
        };
    }

    loadNextElements() {
        if (this.props.isFetching || !this.props.onLoadNextItems) {
            return;
        }

        this.props.onLoadNextItems(this.state.page);
        this.setState({
            page: this.state.page + 1
        });
    }

    setPage(page) {
        this.setState({page});
    }

    render() {
        const masonryOptions = {
            transitionDuration: 0,
            columnWidth: 314
        };

        const { isFetching } = this.props;

        const classNames = classes({
            [this.props.className]: true,
            'container': true,
            'is-fetching': isFetching
        });

        return <div className={classNames}>
            <Masonry className={'relic-discover-v2'} options={masonryOptions} disableImagesLoaded={false}>
                {this.props.children}
            </Masonry>
            <Waypoint onEnter={() => this.loadNextElements()} threshold={0.3}/>
            {isFetching && <Preloader />}
        </div>;
    }
}

Feed.propTypes = {
    onLoadNextItems: PropTypes.func,
    children: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.arrayOf(PropTypes.element)
    ])
};
