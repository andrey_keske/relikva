import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import R from 'ramda';

import SubscriptionButton from './SubscriptionButton';
import { userShape } from '../utils/shapes';
import overlay from './FetchingOverlay';

class UserProfile extends React.Component {
    render() {
        const { user, onSubscribe } = this.props;

        if (R.isEmpty(R.values(user))) {
            return null;
        }

        const avatar = R.path(['avatars', 'x200', 'url'], user);

        return <article className="user_profile_info">
            <figure className="user_profile_info__avatar">
                <img alt={user.handle} className="avatar" src={avatar} width="100" height="100"/>
            </figure>

            <h1 className="user_profile_info__title">
                {user.name}
            </h1>

            <SubscriptionButton onSubscribe={onSubscribe} user={user} />

            <p className="user_profile_info__followers">
                <strong>Подписчики: </strong>{user.followers_count}<br/>
                <strong>Подписки: </strong>{user.following_count}
            </p>
            {user.about.length > 0 &&
                <p className="user_profile_info__about">
                    <strong>О себе: </strong>
                    <span dangerouslySetInnerHTML={{__html: user.about}}></span>
                </p>}
        </article>
    }
}

UserProfile.propTypes = {
    user: userShape,
    onSubscribe: PropTypes.func.isRequired
};

export default overlay(UserProfile);
