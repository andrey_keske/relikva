import React, { PropTypes } from 'react';
import R from 'ramda';

import overlay from './FetchingOverlay';

class SocialButtons extends React.Component {
    static defaultProps = {
        facebook: 0,
        twitter: 0,
        vk: 0,
        ok: 0
    };

    handleClick(event) {
        event.stopPropagation();
        event.preventDefault();

        const windowProps = {
            toolbar: false,
            location: false,
            directories: false,
            status: false,
            menubar: false,
            scrollbars: false,
            resizable: false,
            copyhistory: false,
            width: 500,
            height: 300,
            left: (screen.width / 2) - (500 / 2),
            top: (screen.height / 2) - (300 / 2)
        };
        const propsToString = (props) => R.compose(R.join(', '), R.map(R.join('=')));

        window.open(
            event.currentTarget.href,
            this.props.title,
            propsToString(R.toPairs(windowProps))
        )
    }

    render() {
        const { url } = this.props;
        const shareUrl = location.origin + url;
        const networks = [
            {name: 'vk', url: 'http://www.vk.com/share.php?url='},
            {name: 'facebook', url: 'http://www.facebook.com/sharer/sharer.php?u='},
            {name: 'twitter', url: 'https://twitter.com/intent/tweet'},
            {name: 'ok', url: 'http://www.ok.ru/dk?st.cmd=addShare&st.s=1&st._surl='}
        ];

        return <ul className="sharing-counter">
            {networks.map(({ name, url }, key) =>
                <li className="sharing-counter__li" key={name}>
                    <a className={`sharing-counter__${name} icon-${name}`}
                       href={url + shareUrl}
                       rel="nofollow"
                       target="_blank"
                       onClick={(e) => this.handleClick(e)}>
                        <span className="sharing-counter__count">{this.props[name]}</span>
                    </a>
                </li>)}
        </ul>
    }
}

SocialButtons.propTypes = {
    title: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    facebook: PropTypes.number,
    twitter: PropTypes.number,
    vk: PropTypes.number,
    ok: PropTypes.number
};

export default overlay(SocialButtons);