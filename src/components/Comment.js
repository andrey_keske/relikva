import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import R from 'ramda';

import { userShortShape, commentShape } from '../utils/shapes';
import Datetime from './Datetime';

export default class Comment extends React.Component {
    render() {
        const { comment, user, onReplyClick } = this.props;

        return <div className="comment">
            <Link className="comment__handle" to={`/@${user.handle}`}>
                <img className="avatar comment__avatar" src={user.avatar}/>
                {user.name}
                <Datetime className="comment__date" datetime={comment.created_at} />
            </Link>

            <div className="comment__body" id={`comment_${comment.id}`}>
                <p dangerouslySetInnerHTML={{__html: comment.body}}></p>

                {onReplyClick && <span className="btn-comment__replay" onClick={() => onReplyClick(comment.id)}>
                    Ответить
                </span>}
            </div>

            <div className="comment__children">
                {this.props.children}
            </div>
        </div>
    }
}
Comment.propTypes = {
    children: PropTypes.arrayOf(PropTypes.element),
    comment: commentShape.isRequired,
    user: userShortShape.isRequired,
    onReplyClick: PropTypes.func
};
