import React, { PropTypes } from 'react';

import { galleryImageShape } from '../utils/shapes';
import PhotoGalleryTeaser from './PhotoGalleryTeaser';

export default class PhotoGallery extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 0,
            teaser: props.images[0]
        };
    }

    onNextClick() {
        const nextId = this.state.active + 1;
        const index = this.props.images[nextId] ? nextId : 0;
        this.onPreviewClick(index);
    }

    onPrevClick() {
        const prevId = this.state.active - 1;
        const index = this.props.images[prevId] ? prevId : this.props.images.length - 1;
        this.onPreviewClick(index);
    }

    onPreviewClick(id) {
        this.setState({
            active: id,
            teaser: this.props.images[id]
        });
    }

    render() {
        const { images } = this.props;
        const { active, teaser } = this.state;

        if (images.length === 0) {
            return <div className="relic-gallery empty-gallery">
                <div className="empty-gallery-dummybox">
                </div>
                <div className="empty-gallery-desc">
                    Пока вы не загрузите хотя бы одну
                    фотографию, никто не сможет увидеть вашу
                    реликву
                </div>
            </div>;
        } else if (images.length === 1) {
            return <div className="relic-gallery">
                <div className="teaser iw">
                    <PhotoGalleryTeaser ref="teaser" image={teaser}/>
                </div>
            </div>;
        } else {
            return <div className="relic-gallery">
                <div className="teaser iw">
                    <PhotoGalleryTeaser ref="teaser" image={teaser}/>
                    <span className="prev control-arrow" onClick={() => this.onPrevClick()}></span>
                    <span className="next control-arrow" onClick={() => this.onNextClick()}></span>
                </div>
                <div className="relic-gallery__listing">
                    <div className='relic-gallery__listing__counter'>
                        { active + 1 } из { images.length }
                    </div>
                    <ul>
                        { images.map((item, key) =>
                                <li key={key}
                                    className={`iw ${key === active ? 'active' : ''}`}
                                    onClick={() => this.onPreviewClick(key)}>
                                    <img src={item.x100}/>
                                </li>)}
                    </ul>
                </div>
            </div>;
        }
    }
}

PhotoGallery.propTypes = {
    images: PropTypes.arrayOf(galleryImageShape).isRequired
};