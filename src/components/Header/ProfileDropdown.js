import React, { PropTypes } from 'react';
import { Link } from 'react-router';

import Dropdown from './../Dropdown';

export default class ProfileDropdown extends React.Component {
    static contextTypes = {
        currentUser: PropTypes.object.isRequired
    };

    render() {
        const { currentUser } = this.context;

        const items = <ul className="dropdown-menu user-menu__ul">
            <li className="user-menu__li">
                <Link onClick={() => this.refs.dropdown.toggleDropdown()} to={`/@${currentUser.handle}`} className="current-user-link">Музей</Link>
            </li>
            <li className="user-menu__li">
                <Link onClick={() => this.refs.dropdown.toggleDropdown()} to="/profile">Настройки</Link>
            </li>
            <li className="user-menu__li">
                <a onClick={this.props.onLogout}>Выход</a>
            </li>
        </ul>;

        const button = <span className="dropdown-toggle user-menu-button current-user-link">
            <img src={currentUser.avatar}
                 alt={currentUser.name}
                 className="current-user-avatar"/>
            <b className="caret"></b>
        </span>;

        return <Dropdown ref="dropdown" button={button} items={items}/>;
    }
}

ProfileDropdown.propTypes = {
    onLogout: PropTypes.func.isRequired
};
