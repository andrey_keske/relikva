import React from 'react';
import { Link } from 'react-router';

import Dropdown from './../Dropdown';

export default class AddDropdown extends React.Component {
    render() {
        return <Link className="header_add_button" state={{modal: true}} to="/new/relic"></Link>;
    }
}
