import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import classes from 'classnames';
import R from 'ramda';
import debounce from 'lodash.debounce';

import Dropdown from './../Dropdown';
import Preloader from './../Preloader';
import { activityFeedItemShape, paginatedShape, componentStateShape } from '../../utils/shapes';

const ACTIVITY_FEED_ITEMS_PER_PAGE = 20;
const ACTIVITY_FEED_COUNTER_UPDATE_INTERVAL = 60 * 1000;

export default class ActivityFeedDropdown extends React.Component {
    componentWillMount() {
        this.interval = setInterval(() => this.props.onUpdateCounter(), ACTIVITY_FEED_COUNTER_UPDATE_INTERVAL);
        this.props.onUpdateCounter();

        this.markedItemsIds = [];
        this.onMarkItemsDebounce = debounce(
            () => {
                this.props.onMarkItems(this.markedItemsIds);
                this.markedItemsIds = [];
            },
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    loadInitialItems() {
        this.props.onClearItems();
        this.props.onLoadItems(ACTIVITY_FEED_ITEMS_PER_PAGE, 1, false);
    }

    loadNextItems() {
        const nextPage = this.props.paginated.next_page;
        if (!this.props.isFetching && nextPage) {
            this.props.onLoadItems(ACTIVITY_FEED_ITEMS_PER_PAGE, nextPage, false);
        }
    }

    markItemsIds(items) {
        this.markedItemsIds = R.compose(
            R.uniq,
            R.concat(items)
        )(this.markedItemsIds);
    }

    onItemHover(item) {
        if (!!item.newest) {
            this.markItemsIds([item.id]);
            this.onMarkItemsDebounce();
        }
    }

    render() {
        //TODO Заменить кнопку подгрузки на Waypoint

        const { items, counter: { newest }, self: { isFetching }, paginated } = this.props;

        const empty = <li key="empty" className="activity-feed__no-items"><p>Нет уведомлений</p></li>;
        const loading = <li key="loading"><Preloader /></li>;
        const loadMore = <li className="activity-feed__more" key="more" onClick={() => this.loadNextItems()}>
            <span>Загрузить еще</span>
        </li>;

        const onItemClick = () => {
            this.refs.dropdown.toggleDropdown();
            return true;
        };

        let items_ = [];

        if (isFetching && items.length === 0) {
            items_ = loading;
        } else if (items.length === 0) {
            items_ = empty;
        } else if (items.length > 0) {
            items_ = items
                .map(({ description, sender, resource, newest, created_at, id }, key) =>
                    <li className={classes({ 'activity-feed__li': true, 'newest': newest })}
                        onClick={() => onItemClick()}
                        onMouseOut={() => this.onItemHover({id, newest})}
                        key={key}>
                        <Link to={resource.url}>
                            <img className="activity-feed__resource-teaser" src={resource.teaser}/>
                        </Link>
                        <Link to={`/@${sender.handle}`}>
                            <img className="activity-feed__sender-avatar" src={sender.avatar}/>
                        </Link>
                        <Link className="activity-feed__sender" to={`/@${sender.handle}`}>{sender.name}</Link>

                        <Link className="activity-feed__resource" to={resource.url}
                              dangerouslySetInnerHTML={{__html: description}}>
                        </Link>
                    </li>)
                .concat(isFetching ? loading : (paginated.next_page && loadMore))
        }

        const dropdownItems = <div className="dropdown-menu">
            <ul className="activity-feed__ul">
                {items_}
            </ul>
        </div>;

        const isActive = newest > 0;
        const classNames = classes({
            'dropdown-toggle': true,
            'icon-triangle': true,
            'newest': isActive
        });

        const onDropdownClick = () => {
            this.loadInitialItems();
            this.refs.dropdown.toggleDropdown();
        };

        const button = <div onClick={() => onDropdownClick()} className={classNames}>
            {isActive && <span className="count">{newest}</span>}
        </div>;

        return <Dropdown ref="dropdown" className="activity-feed" button={button} items={dropdownItems}/>;
    }
}

ActivityFeedDropdown.propTypes = {
    paginated: paginatedShape,
    self: componentStateShape.isRequired,
    counter: PropTypes.shape({
        newest: PropTypes.number,
        total: PropTypes.number
    }).isRequired,
    items: PropTypes.arrayOf(activityFeedItemShape).isRequired,
    onClearItems: PropTypes.func.isRequired,
    onLoadItems: PropTypes.func.isRequired,
    onMarkItems: PropTypes.func.isRequired,
    onUpdateCounter: PropTypes.func.isRequired
};
