import React, { PropTypes } from 'react';
import R from 'ramda';

import { FEED_TYPE_ALL, FEED_TYPE_SUBSCRIPTIONS, FEED_TYPE_POPULAR } from '../../reducers/timeline';

export default class FeedTypeTabs extends React.Component {
    static contextTypes = {
        history: PropTypes.object.isRequired
    };

    render() {
        const { feedType, onSetTimelineType, isTimeline } = this.props;

        const onItemClick = (key) => {
            document.querySelector('body').scrollTop = 0;
            this.context.history.pushState(null, '/');
            onSetTimelineType(key);
        };

        const feedTypes = {
            FEED_TYPE_POPULAR: 'Популярное',
            FEED_TYPE_SUBSCRIPTIONS: 'Подписки',
            FEED_TYPE_ALL: 'Все'
        };

        const items = R.compose(
            R.values,
            R.mapObjIndexed((title, key) =>
                    <li key={key}
                        onClick={() => onItemClick(key)}
                        className={(isTimeline && feedType === key) ? 'active' : ''}>
                        {title}
                    </li>
            )
        )(feedTypes);

        return <ul className="tabs">{items}</ul>
    }
}

FeedTypeTabs.propTypes = {
    feedType: PropTypes.oneOf([
        FEED_TYPE_ALL,
        FEED_TYPE_SUBSCRIPTIONS,
        FEED_TYPE_POPULAR
    ]).isRequired,
    onSetTimelineType: PropTypes.func.isRequired,
    isTimeline: PropTypes.bool.isRequired
};
