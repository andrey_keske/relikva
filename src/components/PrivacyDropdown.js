import React, { PropTypes } from 'react';
import R from 'ramda';
import classes from 'classnames';

import overlay from './FetchingOverlay';
import Dropdown from './Dropdown';

class PrivacyDropdown extends React.Component {
    static privacyValues = {
        public: 'Для всех',
        private: 'Для себя'
    };

    render() {
        const { privacy, disabled } = this.props;

        const onItemClick = (key) => {
            this.props.onSelect(key);
            this.refs.dropdown.toggleDropdown();
        };
        const onButtonClick = () => {
            if (!disabled) {
                this.refs.dropdown.toggleDropdown();
            }
        };

        const button = <span onClick={(e) => onButtonClick(e)} className="dropdown-toggle user-menu-button">
            <span>{PrivacyDropdown.privacyValues[privacy]}</span> <b className="caret"></b>
        </span>;

        const items = R.compose(
            R.values,
            R.mapObjIndexed((title, key) =>
                    <li key={key}
                        className={privacy === key ? 'active' : ''}
                        onClick={() => onItemClick(key)}>
                        {title}
                    </li>
            )
        )(PrivacyDropdown.privacyValues);

        const classNames = classes({
            [this.props.className]: true,
            'privacy-dropdown': true
        });

        return <Dropdown ref="dropdown" className={classNames} button={button} items={items}/>
    }
}

PrivacyDropdown.propTypes = {
    privacy: PropTypes.oneOf(R.keys(PrivacyDropdown.privacyValues)).isRequired,
    onSelect: PropTypes.func.isRequired
};

export default overlay(PrivacyDropdown);