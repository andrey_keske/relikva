import React, { PropTypes } from 'react';
import classes from 'classnames';

export default class LikeButton extends React.Component {
    static defaultProps = {
        isLiked: false,
        count: 0
    };

    render() {
        const { count, onLike, isLiked, isFetching } = this.props;

        const classNames = classes({
            [this.props.className]: true,
            'btn-like': true,
            'active': isLiked,
            'is-fetching': isFetching
        });

        return <span className={classNames} onClick={onLike}>
            <span className="text">Мне нравится</span>
            <span className="count">{count}</span>
        </span>
    }
}

LikeButton.propTypes = {
    count: PropTypes.number,
    isLiked: PropTypes.bool,
    onLike: PropTypes.func.isRequired
};