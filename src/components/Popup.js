import React, { PropTypes } from 'react';
import { findDOMNode } from 'react-dom';

import { getElementOffset } from '../utils/dom';

export default class Popup extends React.Component {
    componentDidMount() {
        this.forceUpdate();
    }

    componentDidUpdate() {
        if (this.props.scrollTo) {
            //FIXME Сделать нормальное определение что элемент загружен и можно скроллить
            //FIXME Если закрыть попап в течение 500 мс - вылетит ошибка
            setTimeout(() => {
                const popup = findDOMNode(this);
                const destinationElement = document.querySelector(this.props.scrollTo);
                if (popup && destinationElement) {
                    popup.scrollTop = getElementOffset(destinationElement, popup);
                }
            }, 500);
        }
    }

    overlayClick(event) {
        const { history, returnTo, onOverlayClick } = this.props;

        if (event.target != this.refs.bg) {
            return
        }

        if (onOverlayClick) {
            onOverlayClick();
        } else {
            if (returnTo) {
                history.pushState(null, returnTo);
            } else {
                history.go(-1);
            }
        }
    }

    render() {
        return <div>
            <div className="popup-wrapper__overlay"></div>
            <div className="popup-wrapper popup-wrapper__show" ref="bg" onClick={(e) => this.overlayClick(e)}>
                <div className="popup-wrapper__modal-page">
                    {this.props.children}
                </div>
            </div>
        </div>;
    }
}
Popup.propsType = {
    returnTo: PropTypes.string,
    scrollTo: PropTypes.string,
    children: PropTypes.oneOf([
        PropTypes.element,
        PropTypes.arrayOf(PropTypes.element)
    ]).isRequired,
    onOverlayClick: PropTypes.func,
    history: PropTypes.object.isRequired
};
