import React, { PropTypes } from 'react';
import R from 'ramda';
import classes from 'classnames';

import overlay from './FetchingOverlay';

class CommentReply extends React.Component {
    static defaultProps = {
        errors: []
    };

    static contextTypes = {
        currentUser: PropTypes.object.isRequired
    };

    onPostComment() {
        const comment = this.refs.comment;
        this.props
            .onPostComment(comment.value)
            .then(action => {
                if (!action.error) {
                    comment.value = '';
                }
            })
    }

    render() {
        const { currentUser } = this.context;
        const { isFetching } = this.props;

        const classNames = classes({
            [this.props.className]: true,
            comment: true
        });

        return <div className={classNames}>
            <img className="comment__avatar comment__avatar__pull-left" src={currentUser.avatar}/>

            <div className="comment__body">
                <textarea ref="comment"
                          rows="2"
                          disabled={isFetching}
                          className="form-control comment__textarea"
                          placeholder="Написать комментарий">
                </textarea>

                <ul className="comment__errors">
                    {this.props.errors.map((error, key) => <li key={key}>{error}</li>)}
                </ul>

                <span disabled={isFetching}
                      onClick={() => !isFetching && this.onPostComment()}
                      className="btn-comment__submit">
                    Отправить
                </span>
            </div>
        </div>
    }
}

CommentReply.propTypes = {
    errors: PropTypes.array.isRequired,
    isFetching: PropTypes.bool.isRequired,
    onPostComment: PropTypes.func.isRequired
};

export default overlay(CommentReply);
