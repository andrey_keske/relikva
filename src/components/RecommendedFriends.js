import React, { PropTypes, cloneElement } from 'react';
import { Link } from 'react-router';
import classes from 'classnames';

import SubscriptionButton from './SubscriptionButton';
import { userShortShape } from '../utils/shapes';

import overlay from './FetchingOverlay';
import Preloader from './Preloader';

class RecommendedFriends extends React.Component {
    static defaultProps = {
        count: 6
    };

    componentWillMount() {
        this.props.onLoadRecommendedFriends(this.props.count);
    }

    render() {
        const { onSubscribe, isFetching } = this.props;

        const items = this.props.users.map((user, key) => <div key={key} className="subscription_block__user">
                <Link to={`/@${user.handle}`}>
                    <img alt={user.name} src={user.avatar} />
                    <strong>{user.name}</strong>
                </Link>
                <SubscriptionButton onSubscribe={() => onSubscribe(user.handle, !user.follow)} user={user} />
            </div>
        );

        const classNames = classes({
            [this.props.className]: true,
            'timeline_item': true,
            'subscription_block__wrapper': true
        });

        return <article className={classNames}>
            <Link className="subscription_block__title" to="/find-friends">
                Возможно, вы их знаете:
            </Link>

            {isFetching ? <Preloader /> : items}

            <div className="subscription_block__buttons">
                <Link className="subscription_block__more_btn" to="/find-friends">Посмотреть еще</Link>
            </div>
        </article>;
    }
}

RecommendedFriends.propTypes = {
    onLoadRecommendedFriends: PropTypes.func.isRequired,
    onSubscribe: PropTypes.func.isRequired,
    users: PropTypes.arrayOf(userShortShape),
    count: PropTypes.number
};

export default overlay(RecommendedFriends);
