import React, { PropTypes } from 'react';
import { Link } from 'react-router';

import User from './../User';
import { investigationShortShape } from '../../utils/shapes';
import LikeButton from './../LikeButton';

export default class InvestigationFeedItem extends React.Component {
    render() {
        const { investigation: { url, id, title, teaser, childrens_count, user, like, likes_count, relic_title }, onLike } = this.props;

        const linkProps = {
            to: url,
            state: { modal: true, scrollTo: `#I_${id}` }
        };

        return <article className="investigation-preview timeline_item masonry-brick">
            <User className="investigation-preview__autor" user={user} />
            <header className="investigation-preview__header">
                <span className="investigation-preview__type">Расследование</span>
                <Link {...linkProps}
                    className="investigation-preview__title"
                    dangerouslySetInnerHTML={{__html: title}}></Link>
            </header>
            <div className="investigation-preview__relic">
                <Link {...linkProps}>
                    <img className="investigation-preview__relic__teaser" src={teaser}/>
                </Link>
                <div className="investigation-preview__relic__type">К реликве:</div>
                <div className="investigation-preview__relic__title"
                    dangerouslySetInnerHTML={{__html: relic_title}} ></div>

                <div className="investigation-preview__relic__answers">
                    Ответов: <span>{childrens_count}</span>
                </div>
            </div>

        </article>;
    }
}

InvestigationFeedItem.propTypes = {
    investigation: investigationShortShape.isRequired,
    onLike: PropTypes.func.isRequired
};
