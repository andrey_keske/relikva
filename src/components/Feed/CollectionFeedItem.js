import React, { PropTypes } from 'react';
import { Link } from 'react-router';

import User from './../User';
import { collectionShortShape } from '../../utils/shapes';
import LikeButton from './../LikeButton';

export default class CollectionFeedItem extends React.Component {
    renderCount() {
        return (
            <span className="collection-preview__relic-counter">
                <span className="icon" />
                <span className="count">
                    {this.props.collection.relics_count}
                </span>
            </span>
        )
    }

    render() {
        const { collection: { user, permalink, teaser, likes_count, title, like }, onLike } = this.props;

        const linkProps = {
            to: `/@${user.handle}/c/${permalink}`,
            state: {modal: false}
        };

        return <article className="collection-preview timeline_item masonry-brick">
            <div className="collection-preview__wrapper">
                <Link {...linkProps}
                    className="collection-preview__bg"
                    style={{backgroundImage: `url(${teaser})`}}>
                </Link>

                <div className="collection-preview__info">
                    <h5 className="collection-preview__type">
                        Коллекция
                    </h5>

                    <Link className="collection-preview__title"
                          dangerouslySetInnerHTML={{__html: title}}
                        {...linkProps} ></Link>
                    <div className="collection-preview__actions">
                        {this.renderCount()}
                        <LikeButton className="btn-like__in_preview"
                                    count={likes_count}
                                    isLiked={like}
                                    onLike={onLike}/>
                    </div>

                    <User className="collection-preview__autor" user={user}/>
                </div>
            </div>
        </article>
    }
}

CollectionFeedItem.propTypes = {
    collection: collectionShortShape.isRequired,
    onLike: PropTypes.func.isRequired
};
