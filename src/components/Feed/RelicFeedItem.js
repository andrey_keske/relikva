import React, { PropTypes } from 'react';
import { Link } from 'react-router';

import CategoryLinks from './../CategoryLinks';
import User from './../User';
import { relicShortShape } from '../../utils/shapes';
import LikeButton from './../LikeButton';

export default class RelicFeedItem extends React.Component {
    render() {
        const { relic: { like, permalink, url, categories, user, title, teaser,
            description, discussions_count, likes_count }, onLike } = this.props;

        const linkProps = {
            to: `/@${user.handle}/r/${permalink}`,
            state: { modal: true }
        };

        return <article className="relic-preview timeline_item masonry-brick">
            <CategoryLinks categories={categories}/>

            <h3 className="relic-preview__title">
                <Link dangerouslySetInnerHTML={{__html: title}} {...linkProps}></Link>
            </h3>

            <Link to={url} {...linkProps}>
                <img className="relic-preview__teaser" src={teaser}/>
            </Link>

            <div className="relic-preview__info">
                <Link className="relic-preview__description"
                      dangerouslySetInnerHTML={{__html: description}} {...linkProps}></Link>
                <div className="relic-preview__actions">
                    <LikeButton className="btn-like__in_preview"
                                count={likes_count}
                                isLiked={like}
                                onLike={onLike} />
                    <Link className="relic-preview__comments comments-icon" {...linkProps}>
                        {discussions_count}
                    </Link>
                </div>
                <User className="relic-preview__autor" user={user}/>
            </div>
        </article>;
    }
}

RelicFeedItem.propTypes = {
    relic: relicShortShape.isRequired,
    onLike: PropTypes.func.isRequired
};
