import React, { PropTypes } from 'react';
import classes from 'classnames';

import overlay from './FetchingOverlay';

class NewInvestigation extends React.Component {
    static defaultProps = {
        errors: []
    };

    onCreateInvestigation() {
        const investigation = this.refs.investigation;
        this.props
            .onCreateInvestigation(investigation.value)
            .then(action => {
                if (!action.error) {
                    investigation.value = '';
                }
            })
    }

    render() {
        const { isFetching } = this.props;

        const classNames = classes({
            [this.props.className]: true,
            'investigation': true,
            'investigation__new': true
        });

        return <div className={classNames}>
            <div className="investigation__new__header">Новое расследование</div>

            <textarea ref="investigation"
                      className="investigation__textarea"
                      placeholder="Ваше новое расследование"
                      disabled={isFetching}>
            </textarea>

            <ul className="investigation__new__errors">
                {this.props.errors.map((error, key) => <li key={key}>{error}</li>)}
            </ul>

            <span disabled={isFetching}
                  onClick={() => !isFetching && this.onCreateInvestigation()}
                  className="btn-comment__submit">
                Расследовать
            </span>
        </div>
    }
}

NewInvestigation.propTypes = {
    errors: PropTypes.arrayOf(PropTypes.string),
    isFetching: PropTypes.bool.isRequired,
    onCreateInvestigation: PropTypes.func.isRequired
};

export default overlay(NewInvestigation);