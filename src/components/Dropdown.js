import React, { PropTypes, cloneElement } from 'react';
import R from 'ramda';

import classnames from 'classnames';

export default class Dropdown extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false
        };
        this.hide = (e) => {
            const isCalledFromComponent = e === 'FromComponent';
            if (isCalledFromComponent || !R.contains(this.refs.dropdown, e.path)) {
                this.setState({ isOpen: false });
                document.removeEventListener('click', this.hide);
            }
        };
        this.show = () => {
            this.setState({ isOpen: true });
            document.addEventListener('click', this.hide);
        }
    }

    toggleDropdown() {
        if (this.state.isOpen) {
            this.hide('FromComponent');
        } else {
            this.show();
        }
    }

    render() {
        const { items, button } = this.props;

        const classNames = classnames({
            [this.props.className]: true,
            'dropdown': true,
            'open': this.state.isOpen
        });
        const list = Array.isArray(items) ? <ul>{items}</ul> : items;

        return <div ref="dropdown" className={classNames}>
            {cloneElement(button, {
                onClick: (e) => {
                    if (button.props.onClick) {
                        button.props.onClick(e)
                    } else {
                        this.toggleDropdown();
                    }
                }
            })}
            {cloneElement(list, {
                className: list.className + ' dropdown-menu',
                onClose: () => this.toggleDropdown()
            })}
        </div>;
    }
}

Dropdown.propTypes = {
    className: PropTypes.string,
    items: PropTypes.oneOfType([
        PropTypes.element,
        PropTypes.arrayOf(PropTypes.element)
    ]).isRequired,
    button: PropTypes.element.isRequired
};