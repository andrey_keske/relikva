import React, { PropTypes } from 'react';
import R from 'ramda';

import overlay from './FetchingOverlay';

class NewPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            password: '',
            newPassword: '',
            newPasswordConfirm: ''
        }
    }

    onPasswordSaveClick() {
        this.props.onPasswordSave(this.state.password, this.state.newPassword, this.state.newPasswordConfirm);
    }

    onInputChange(key, e) {
        this.setState(R.assoc(key, e.target.value, this.state));
    }

    render() {
        const { errors, isFetching } = this.props;

        const renderInput = (key, label) => <div className="form-group">
            <p className="edit-profile__label">{label}</p>
            <input value={this.state[key]}
                   disabled={isFetching}
                   onChange={(e) => !isFetching && this.onInputChange(key, e)}
                   className="form-control"
                   maxLength="255"
                   required=""
                   type="password"/>
        </div>;

        return <div>
            {errors.length > 0 && errors.map((item, key) => <p key={key}>{item}</p>)}

            <div className="form-group">
                <p className="edit-profile__label">Текущий пароль</p>
                <input value={this.state['password']}
                       disabled={isFetching}
                       onChange={(e) => !isFetching && this.onInputChange('password', e)}
                       className="form-control"
                       maxLength="255"
                       required=""
                       type="password"/>
            </div>
            <div className="form-group">
                <p className="edit-profile__label">Новый пароль</p>
                <input value={this.state['newPassword']}
                       disabled={isFetching || !this.state['password']}
                       onChange={(e) => !isFetching && this.onInputChange('newPassword', e)}
                       className="form-control"
                       maxLength="255"
                       required=""
                       type="password"/>
            </div>
            <div className="form-group">
                <p className="edit-profile__label">Подтвердите новый пароль</p>
                <input value={this.state['newPasswordConfirm']}
                       disabled={isFetching || !this.state['newPassword']}
                       onChange={(e) => !isFetching && this.onInputChange('newPasswordConfirm', e)}
                       className="form-control"
                       maxLength="255"
                       required=""
                       type="password"/>
            </div>

            <div className="form-group">
                <button disabled={isFetching}
                        className="hand btn-rectangle"
                        onClick={() => !isFetching && this.onPasswordSaveClick()}>
                    Изменить пароль
                </button>
            </div>
        </div>
    }
}

NewPassword.propTypes = {
    onPasswordSave: PropTypes.func.isRequired,
    isFetching: PropTypes.bool.isRequired
};

export default overlay(NewPassword);
