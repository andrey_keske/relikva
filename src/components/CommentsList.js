import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import R from 'ramda';

import Comment from './Comment';
import CommentReply from './CommentReply';
import { userShortShape, commentShape, componentStateShape } from '../utils/shapes';

export default class CommentsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            replyToId: null
        };
    }

    render() {
        const { users, comments, replyState, onPostReply, onPostComment } = this.props;
        const { replyToId } = this.state;

        const commentReply = <CommentReply
            {...replyState}
            key="reply"
            onPostComment={(body) => R.isNil(replyToId) ? onPostComment(body) : onPostReply(replyToId, body)}/>;

        const setReplyId = (id) => this.setState({replyToId: id});
        const getAuthor = (commentatorId) => R.find(R.propEq('id', commentatorId), users);
        const getNestedComments = (parentId) => R.filter(R.propEq('parent_id', parentId), comments);

        const commentsList = comments
            .filter(R.propEq('parent_id', null))
            .map((item, key) =>
                <Comment key={key}
                         user={getAuthor(item.commentator_id)}
                         comment={item}
                         onReplyClick={() => setReplyId(item.id)}>
                    {getNestedComments(item.id)
                        .map((nestedComment, nestedKey) =>
                            <Comment key={nestedKey}
                                     user={getAuthor(nestedComment.commentator_id)}
                                     comment={nestedComment}
                                     onReplyClick={() => setReplyId(item.id)}/>)
                        .concat(replyToId === item.id ? commentReply : null)}
                </Comment>
        );

        const newCommentButton = <span className="btn-comment__submit" onClick={() => setReplyId(null)}>
            Новый комментарий
        </span>;

        return <div className="relic_discussions_list">
            <div className="relic_discussions">
                {commentsList}
                {replyToId !== null ? newCommentButton : commentReply}
            </div>
        </div>
    }
}
CommentsList.propTypes = {
    users: PropTypes.arrayOf(userShortShape),
    comments: PropTypes.arrayOf(commentShape),
    replyState: componentStateShape,
    onPostComment: PropTypes.func.isRequired,
    onPostReply: PropTypes.func.isRequired
};
