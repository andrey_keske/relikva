import React, { PropTypes } from 'react';
import formatDate from '../utils/date';
import classes from 'classnames';

export default class Datetime extends React.Component {
    render() {
        const classNames = classes({
            [this.props.className]: this.props.className,
            'datetime': true
        });

        return <div className={classNames}>
            <time>{formatDate(this.props.datetime)}</time>
        </div>
    }
}
Datetime.propTypes = {
    datetime: PropTypes.string.isRequired
};
