import React, { PropTypes } from 'react';
import { Link } from 'react-router';

import { categoryLinkShape } from '../utils/shapes';

export default class CategoryLinks extends React.Component {
    render() {
        const { categories } = this.props;

        return <ul className="categorie-links">
            {categories.map(({ url, title }, key) =>
                <li className="categorie-links__li" key={key}>
                    <Link to={url} className="categorie_link" 
                        dangerouslySetInnerHTML={{__html: title}}></Link>
                    { key === categories.length - 1 ? null : ","}
                </li>
            )}
        </ul>;
    }
}

CategoryLinks.propTypes = {
    categories: PropTypes.arrayOf(categoryLinkShape)
};