import React, { PropTypes, findDOMNode } from 'react';
import R from 'ramda';
import moment from 'moment';

export default class YearSelect extends React.Component {
    render() {
        const century = this.props.century;

        const addPrefix = (year) => Number(
            (century - 1).toString() +
            ('0' + year).substr(-2)
        );

        const currentYear = Number(moment().format('YY'));
        const maxYear = century == 21 ? currentYear : 99;
        let years = R
            .range(0, maxYear + 1)
            .map(addPrefix);

        return <div className="date-select">
            <select {...this.props}
                value={years[this.props.value]}
                onChange={(e) => this.props.onChange(e.target.value)}>
                <option value="">Год</option>
                {century !== '' && years.map((year, key) =>
                    <option key={key}>
                        {year}
                    </option>)}
            </select>
            <b className="caret"></b>
        </div>
    }
}
YearSelect.propTypes = {
    century: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
    onChange: PropTypes.func.isRequired
};