import React, { PropTypes, findDOMNode } from 'react';
import R from 'ramda';

export default class CenturySelect extends React.Component {
    render() {
        const centuries = ['Век', 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII', 'XIII', 'XIV', 'XV', 'XVI', 'XVII', 'XVIII', 'XIX', 'XX', 'XXI'];

        return <div className="date-select">
            <select {...this.props}
                value={centuries[this.props.value] || ''}
                onChange={(e) => this.props.onChange(e.target.selectedIndex || '')}>
                {centuries.map((century, key) =>
                    <option key={key}>
                        {century}
                    </option>)}
            </select>
            <b className="caret"></b>
        </div>
    }
}

CenturySelect.propTypes = {
    value: PropTypes.any.isRequired,
    onChange: PropTypes.func.isRequired
};