import React, { PropTypes, findDOMNode } from 'react';
import R from 'ramda';
import moment from 'moment';

export default class DaySelect extends React.Component {
    getDaysInMonth(props) {
        const year = (props.century - 1).toString() + ('0' + props.year).substr(-2);
        const month = props.month - 1;
        return moment([year, month, 1]).daysInMonth();
    }

    componentWillReceiveProps(nextProps) {
        const daysInMonth = this.getDaysInMonth(nextProps);
        if (nextProps.value && nextProps.value > daysInMonth) {
            nextProps.onChange(daysInMonth);
        }
    }

    render() {
        const days = R.prepend('День', R.range(1, this.getDaysInMonth(this.props) + 1));

        return <div className="date-select">
            <select {...this.props}
                value={days[this.props.value] || ''}
                onChange={(e) => this.props.onChange(e.target.selectedIndex || '')}>
                {days.map((day, key) =>
                    <option key={key}>
                        {day}
                    </option>)}
            </select>
            <b className="caret"></b>
        </div>;
    }
}
DaySelect.propTypes = {
    century: PropTypes.any.isRequired,
    year: PropTypes.any.isRequired,
    month: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
    onChange: PropTypes.func.isRequired
};