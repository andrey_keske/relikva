import React, { PropTypes, findDOMNode } from 'react';
import R from 'ramda';
import moment from 'moment';

export default class MonthSelect extends React.Component {
    render() {
        const months = R.prepend('Месяц', moment.months());

        return <div className="date-select">
            <select {...this.props}
                value={months[this.props.value] || ''}
                onChange={(e) => this.props.onChange(e.target.selectedIndex || '')}>
                {months.map((month, key) =>
                    <option key={key}>
                        {month}
                    </option>)}
            </select>
            <b className="caret"></b>
        </div>
    }
}
MonthSelect.propTypes = {
    value: PropTypes.any.isRequired,
    onChange: PropTypes.func.isRequired
};