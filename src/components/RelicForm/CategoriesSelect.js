import React, { PropTypes, findDOMNode } from 'react';
import R from 'ramda';
import debounce from 'lodash.debounce';
import Preloader from '../Preloader';

import Autosuggest from 'react-autosuggest/dist/Autosuggest.js';

import '../../autosuggest.scss';

export default class CategoriesSelect extends React.Component {
    componentWillMount() {
        this.prevQuery = '';
        this.onGetSuggestionsDebounce = debounce((query, cb) => {
            if (!query || query === this.prevQuery) {
                return null;
            }

            cb(null, [{title: <Preloader />}]);

            const appendCategory = {
                isNew: true,
                query,
                title: `Создать "${query}"`
            };

            this.props
                .searchCategories(query)
                .then(({ categories }) => {
                    if (categories.length > 0) {
                        const isContainsQuery = R.compose(R.contains(query), R.pluck('title'))(categories);

                        if (!isContainsQuery) {
                            categories.push(appendCategory);
                        }

                        cb(null, categories);
                    } else {
                        cb(null, [appendCategory]);
                    }
                    this.prevQuery = query;
                })

        }, 300);
    }

    render() {
        const {
            onChangeSearch,
            selectedCategories,
            categorySearch,
            onSelect,
            onDelete,
            onCreate
            } = this.props;

        const inputAttributesFrom = {
            id: 'categories-input',
            onChange: (value) => onChangeSearch(value),
            onKeyPress: (e) => e.key === 'Enter' && onCreate(categorySearch)
        };

        const suggestionRender = (category) => <span>{category.title}</span>;

        const onSuggestionSelected = (suggestion, e) => {
            e.preventDefault();
            if (suggestion.isNew) {
                onCreate(suggestion.query)
            } else {
                onSelect(suggestion);
            }
        };

        return <div>
            <div>
                {selectedCategories.map((item, key) =>
                    <span key={key} className="label label-default">
                        {item.title}<span onClick={() => onDelete(item.id)}>[x]</span>
                    </span>
                )}
            </div>
            <Autosuggest suggestions={this.onGetSuggestionsDebounce}
                         suggestionRenderer={suggestionRender}
                         suggestionValue={(category) => category.title}
                         onSuggestionSelected={onSuggestionSelected}
                         inputAttributes={inputAttributesFrom}
                         scrollBar={true}
                         value={categorySearch}
                         id="categories"/>
        </div>;
    }
}
