import React, { PropTypes, findDOMNode } from 'react';
import R from 'ramda';

import Autosuggest from 'react-autosuggest/dist/Autosuggest.js';

import '../../autosuggest.scss';

export default class PeoplesSelect extends React.Component {
    render() {
        const {
            selectedUsers,
            selectedBlankUsers,
            peopleSearch,
            onGetSuggestions,
            onChangeSearch,
            onCreate,
            onSelect,
            onDelete,
            onDeleteBlank
            } = this.props;

        const inputAttributesFrom = {
            id: 'peoples-input',
            onChange: (value) => onChangeSearch(value),
            onKeyPress: (e) => e.key === 'Enter' && onCreate()
        };

        const suggestionRender = (people) => <span>{people.name}</span>;

        return <div>
            <div>
                {selectedUsers.map((item, key) =>
                    <span key={key} className="label label-default">
                        <strong>{item.name}</strong><span onClick={() => onDelete(item.id)}>[x]</span>
                    </span>
                )}
                {selectedBlankUsers.map((item, key) =>
                    <span key={key} className="label label-default">
                        {item.name}<span onClick={() => onDeleteBlank(item.id)}>[x]</span>
                    </span>
                )}
            </div>
            <Autosuggest suggestions={onGetSuggestions}
                         suggestionRenderer={suggestionRender}
                         suggestionValue={(people) => people.name}
                         onSuggestionSelected={(people, event) => {event.preventDefault(); onSelect(people)}}
                         inputAttributes={inputAttributesFrom}
                         value={peopleSearch}
                         id="peoples"/>
        </div>;
    }
}
