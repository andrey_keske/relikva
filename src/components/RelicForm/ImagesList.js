import React, { PropTypes, findDOMNode } from 'react';
import R from 'ramda';
import Dropzone from 'react-dropzone';

export default class ImagesList extends React.Component {
    onDrop(files) {
        if (FileReader) {
            files.forEach(file => {
                if (file && (/^image\/(png|jpg|jpeg)$/i).test(file.type)) {
                    let reader = new FileReader();
                    reader.onload = (e) => this.props.onDropFile(e.target.result);
                    reader.readAsDataURL(file);
                }
            })
        }
    }

    render() {
        const { images, onDeleteImage, onMoveImage, onRotateImage } = this.props;

        return <div>
            <Dropzone ref="dropzone"
                      className="wrapper"
                      onDrop={(files) => this.onDrop(files)}
                      multiple={true}
                      disableClick={true}>
                <span className="btn" onClick={() => this.refs.dropzone.open()}>Добавить фотографию</span>
                <div>
                    {images.map((item, key) =>
                        <div key={key} className="image-block">
                            <img className="image-block__image" src={item.x600}/>
                            <div className="image-block__buttons">
                                <span className="glyphicon glyphicon-arrow-left" onClick={() => onMoveImage(item, -1)}>
                                </span>
                                <span className="glyphicon glyphicon-arrow-right" onClick={() => onMoveImage(item, 1)}>
                                </span>
                                <span className="glyphicon glyphicon-repeat" onClick={() => onRotateImage(item.id, 90)}>
                                </span>
                                <span className="glyphicon glyphicon-ban-circle" onClick={() => onDeleteImage(item.id)}>
                                </span>
                            </div>
                        </div>
                    )}
                </div>
            </Dropzone>
        </div>;
    }
}
