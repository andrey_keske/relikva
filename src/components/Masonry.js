import Masonry from 'masonry/masonry';
import imagesloaded from 'imagesloaded';
import React, { PropTypes } from 'react';
import { findDOMNode } from 'react-dom';

export default class MasonryComponent extends React.Component {
    constructor(props) {
        super(props);
        this.masonry = false;
        this.domChildren = [];
        this.state = {};
    }

    static defaultProps = {
        disableImagesLoaded: false,
        options: {},
        className: '',
        elementType: 'div'
    };

    initializeMasonry(force) {
        if (!this.masonry || force) {
            this.masonry = new Masonry(findDOMNode(this.refs.masonryContainer), this.props.options);
            this.domChildren = this.getNewDomChildren();
        }
    }

    getNewDomChildren() {
        var node = findDOMNode(this.refs.masonryContainer);
        var children = this.props.options.itemSelector ? node.querySelectorAll(this.props.options.itemSelector) : node.children;
        return Array.prototype.slice.call(children);
    }

    diffDomChildren() {
        //take only elements attached to DOM
        //(aka the parent is the masonry container, not null)
        var oldChildren = this.domChildren.filter((element) => !!element.parentNode);
        var newChildren = this.getNewDomChildren();

        var removed = oldChildren.filter((oldChild) => !~newChildren.indexOf(oldChild));
        var domDiff = newChildren.filter((newChild) => !~oldChildren.indexOf(newChild));

        var beginningIndex = 0;

        // get everything added to the beginning of the DOMNode list
        var prepended = domDiff.filter((newChild, i) => {
            var prepend = (beginningIndex === newChildren.indexOf(newChild));

            if (prepend) {
                beginningIndex++;
            }

            return prepend;
        });

        // we assume that everything else is appended
        var appended = domDiff.filter((el) => prepended.indexOf(el) === -1);

        /*
         * otherwise we reverse it because so we're going through the list picking off the items that
         * have been added at the end of the list. this complex logic is preserved in case it needs to be
         * invoked
         *
         * var endingIndex = newChildren.length - 1;
         *
         * domDiff.reverse().filter(function(newChild, i){
         *     var append = endingIndex == newChildren.indexOf(newChild);
         *
         *     if (append) {
         *         endingIndex--;
         *     }
         *
         *     return append;
         * });
         */

        // get everything added to the end of the DOMNode list
        var moved = [];

        if (removed.length === 0) {
            moved = oldChildren.filter((child, index) =>  index !== newChildren.indexOf(child));
        }

        this.domChildren = newChildren;

        return {
            old: oldChildren,
            new: newChildren,
            removed: removed,
            appended: appended,
            prepended: prepended,
            moved: moved
        };
    }

    performLayout() {
        var diff = this.diffDomChildren();

        if (diff.removed.length > 0) {
            this.masonry.remove(diff.removed);
            this.masonry.reloadItems();
        }

        if (diff.appended.length > 0) {
            this.masonry.appended(diff.appended);
            this.masonry.reloadItems();
        }

        if (diff.prepended.length > 0) {
            this.masonry.prepended(diff.prepended);
        }

        if (diff.moved.length > 0) {
            this.masonry.reloadItems();
        }

        this.masonry.layout();
    }

    imagesLoaded() {
        if (this.props.disableImagesLoaded) return;

        imagesloaded(
            findDOMNode(this.refs.masonryContainer),
            (instance) => this.masonry.layout()
        );
    }

    componentDidMount() {
        this.initializeMasonry();
        this.imagesLoaded();
    }

    componentDidUpdate() {
        this.performLayout();
        this.imagesLoaded();
    }

    componentWillReceiveProps() {
        //this._timer = setTimeout(() => {
            this.masonry.reloadItems();
            this.forceUpdate();
        //}, 0);
    }

    componentWillUnmount() {
        clearTimeout(this._timer);
    }

    render() {
        const { elementType, className, children } = this.props;

        return React.createElement(elementType, {className, ref: 'masonryContainer' }, children);
    }
}

MasonryComponent.propTypes = {
    disableImagesLoaded: PropTypes.bool,
    options: PropTypes.object
};