import React from 'react';

export default class Preloader extends React.Component {
    render() {
        return <span className="preloader">
            <span className="preloader__img"></span>
        </span>
    }
}

Preloader.propTypes = {};
