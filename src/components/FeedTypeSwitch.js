import React, { PropTypes } from 'react';

export const USER_FEED_TYPE_RELICS = 'USER_FEED_TYPE_RELICS';
export const USER_FEED_TYPE_COLLECTIONS = 'USER_FEED_TYPE_COLLECTIONS';

export default class FeedTypeSwitch extends React.Component {
    render() {
        const { feedType, user, onRelicsClick, onCollectionsClick } = this.props;

        if (!user || !user.relics_count && !user.collections_count) {
            return null;
        }

        const relics = <li className={feedType === USER_FEED_TYPE_RELICS && 'active'} onClick={onRelicsClick}>
            Реликвы
        </li>;

        const cols= <li className={feedType === USER_FEED_TYPE_COLLECTIONS && 'active'} onClick={onCollectionsClick}>
            Коллекции
        </li>;

        return <ul className="profile_tabs">
            {user.relics_count > 0 && relics}
            {user.collections_count > 0 && cols}
        </ul>
    }
}

FeedTypeSwitch.propTypes = {
    feedType: PropTypes.oneOf([
        USER_FEED_TYPE_RELICS,
        USER_FEED_TYPE_COLLECTIONS
    ]),
    user: PropTypes.object,
    onCollectionsClick: PropTypes.func.isRequired,
    onRelicsClick: PropTypes.func.isRequired
};
