import React, { PropTypes } from 'react';
import { Link } from 'react-router';

import FeedTypeTabs from './Header/FeedTypeTabs';
import AddDropdown from './Header/AddDropdown';
import ActivityFeedDropdown from './Header/ActivityFeedDropdown';
import ProfileDropdown from './Header/ProfileDropdown';

export default class Header extends React.Component {
    render() {
        const {
            feedType,
            isTimeline,
            onSetTimelineType,
            onLogout,
            activityFeed,
            activityFeedState,
            activityFeedActions: {
                loadActivityFeedItems,
                markActivityFeedItems,
                updateActivityFeedCounter,
                clearActivityFeedItems }
            } = this.props;

        return <header className="main-header">
            <div className="container main-header__container">
                <Link className="main-header__logo" to="/">
                    <span className="siteLogo">Relikva</span>
                </Link>

                <nav className="main-header__nav">
                    <FeedTypeTabs isTimeline={isTimeline}
                                  feedType={feedType}
                                  onSetTimelineType={onSetTimelineType}/>
                </nav>

                <ul className="main-header__profile">
                    <li className="main-header__create_relic">
                        <AddDropdown />
                    </li>
                    <li className="main-header__search-link">
                        <Link role="button" className="icon-search" to="/search"></Link>
                    </li>
                    <li className="main-header__notification">
                        <ActivityFeedDropdown {...activityFeed}
                            {...activityFeedState}
                            onClearItems={clearActivityFeedItems}
                            onLoadItems={loadActivityFeedItems}
                            onMarkItems={markActivityFeedItems}
                            onUpdateCounter={updateActivityFeedCounter}/>
                    </li>
                    <li className="main-header__user-menu-button">
                        <ProfileDropdown onLogout={onLogout} />
                    </li>
                </ul>
            </div>
        </header>;
    }
}
