import React, { PropTypes } from 'react';

export default class CollectionsList extends React.Component {
    render() {
        const { number } = this.props;

        return <div className="relic-number">
            {number}
        </div>
    }
}
Comment.propTypes = {
    number: PropTypes.string
};