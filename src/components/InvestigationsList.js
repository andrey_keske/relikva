import React, { PropTypes } from 'react';
import R from 'ramda';

import Investigation from './Investigation';
import { investigationShortShape, userShortShape, commentShape } from '../utils/shapes';

export default class InvestigationsList extends React.Component {
    render() {
        const { users, comments, investigations, investigationsStatuses,
            onPostComment, onDeleteInvestigation, onToggleInvestigationLike } = this.props;

        const items = investigations.map((inv, key) => {
            const invStatus = investigationsStatuses[inv.id] || {};
            const invComments = comments.filter(comment => comment.parent_id === inv.id);
            return <Investigation {...invStatus.deletion}
                key={key}
                investigation={inv}
                users={users}
                comments={invComments}
                investigationStatus={invStatus}
                onToggleInvestigationLike={() => onToggleInvestigationLike(inv.id, inv.like)}
                onPostComment={(body) => onPostComment(inv.id, body)}
                onDeleteInvestigation={() => onDeleteInvestigation(inv.id)}/>
        });

        return <div className="investigation_grid">
            {items}
        </div>
    }
}
InvestigationsList.propTypes = {
    users: PropTypes.arrayOf(userShortShape),
    comments: PropTypes.arrayOf(commentShape),
    investigations: PropTypes.arrayOf(investigationShortShape),
    investigationsStatuses: PropTypes.object,
    onToggleInvestigationLike: PropTypes.func.isRequired,
    onPostComment: PropTypes.func.isRequired,
    onDeleteInvestigation: PropTypes.func.isRequired
};
