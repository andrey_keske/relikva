import React, { PropTypes } from 'react';
import R from 'ramda';
import classes from 'classnames';

import { userShortShape } from '../utils/shapes';

export default class SubscriptionButton extends React.Component {
    render() {
        const { user: { can_follow, follow }, onSubscribe, isFetching } = this.props;

        if (!can_follow) {
            return null;
        }

        const classNames = classes({
            [this.props.className]: this.props.className,
            'toggle-button': true,
            'btn': true,
            'btn-success': can_follow && follow === true,
            'subscription-btn': true,
            'is-fetching': isFetching
        });

        return <button onClick={onSubscribe} className={classNames}>
            <span className="popover">{follow ? 'Отписаться' : 'Подписаться'}</span>
        </button>
    }
}
SubscriptionButton.propTypes = {
    user: userShortShape.isRequired,
    onSubscribe: PropTypes.func.isRequired
};