import React, { PropTypes } from 'react';
import R from 'ramda';
import classes from 'classnames';

import overlay from './FetchingOverlay';
import LikeButton from './LikeButton';
import Comment from './Comment';
import CommentReply from './CommentReply';
import { investigationShortShape, userShortShape, commentShape, componentStateShape } from '../utils/shapes';
import Datetime from './Datetime';

class Investigation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            replyToId: null
        }
    }

    onDeleteClick(e) {
        if (confirm('Вы уверены что хотите удалить это расследование?')) {
            this.props.onDeleteInvestigation();
        }
    }

    render() {
        const { users, comments, investigationStatus, investigation, onPostComment, onToggleInvestigationLike } = this.props;

        const commentsItems = comments.map((item, key) => {
                const author = R.find(R.propEq('id', item.commentator_id), users);

                return <Comment key={key} user={author} comment={item}/>
            }
        );

        const classNames = classes({
            [this.props.className]: true,
            'investigation': true
        });

        //TODO стили для временной метки
        return <div className={classNames} id={`I_${investigation.id}`}>
            <Datetime className="investigation__date" datetime={investigation.created_at} />
            <div className="investigation__title"
                dangerouslySetInnerHTML={{__html: investigation.body}}></div>
            <div className="comment__children investigation__children">
                {commentsItems}
                <CommentReply {...investigationStatus.reply} onPostComment={onPostComment}/>
            </div>
        </div>;
    }
}

export default overlay(Investigation);

Investigation.propTypes = {
    users: PropTypes.arrayOf(userShortShape),
    comments: PropTypes.arrayOf(commentShape),
    replyErrors: PropTypes.object,
    investigationStatus: PropTypes.shape({
        deletion: componentStateShape,
        reply: componentStateShape,
        like: componentStateShape
    }),
    investigation: investigationShortShape.isRequired,
    onToggleInvestigationLike: PropTypes.func.isRequired,
    onPostComment: PropTypes.func.isRequired,
    onDeleteInvestigation: PropTypes.func.isRequired
};
