import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import classes from 'classnames';

import { userShortShape } from '../utils/shapes';

export default class User extends React.Component {
    render() {
        const { user: { avatar, handle, name } } = this.props;
        const classNames = classes({
            [this.props.className]: true,
            'preview_author': true
        });

        return <Link className={classNames} to={`/@${handle}`}>
            <img src={avatar} alt={handle} />
            <strong>{name}</strong>
        </Link>
    }
}

User.propTypes = {
    user: userShortShape.isRequired
};
