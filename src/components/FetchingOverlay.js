import React from 'react';
import classes from 'classnames';

export default ComposedComponent => class extends React.Component {
    static defaultProps = {
        isFetching: false
    };

    render() {
        const classNames = classes({
            'is-fetching': this.props.isFetching
        });
        return <ComposedComponent className={classNames} {...this.props} />;
    }
};