import React, { PropTypes } from 'react';
import R from 'ramda';

import overlay from './FetchingOverlay';
import Preloader from './Preloader';
import { collectionShortShape } from '../utils/shapes';

class CollectionsList extends React.Component {
    onAddClick() {
        this.props.onAddToSelected(this.state.selectedIds);
    }

    render() {
        const { collections, isFetching } = this.props;

        const items = collections.map((item, key) =>
                <li className="checkbox" key={key} onClick={() => this.toggleSelection(item.id)}>
                    <label className={R.contains(item.id, this.state.selectedIds) ? 'active' : ''}>
                        <span className="title">{item.title}</span>
                    </label>
                </li>
        );

        const loading = <li><Preloader /></li>;

        return <div>
            <h4>Добавить в коллекцию</h4>
            <ul className="form-group">
                {isFetching ? loading : items}
            </ul>
            <span className="btn btn-rectangle big" onClick={() => this.onAddClick()}>Добавить</span>
        </div>
    }
}

CollectionsList.propTypes = {
    errors: PropTypes.arrayOf(PropTypes.string),
    collections: PropTypes.arrayOf(collectionShortShape),
    isFetching: PropTypes.bool.isRequired,
    onAddToSelected: PropTypes.func.isRequired
};

export default overlay(CollectionsList);