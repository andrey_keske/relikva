import React from 'react';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';
import moment from 'moment';
import 'moment/locale/ru';

if (!__PROD__) {
    //require('./stylesheets/application.css.scss');
    require('./styles.scss');
}

moment.locale('ru', {
    longDateFormat : {
        LT : 'HH:mm',
        LTS : 'HH:mm:ss',
        L : 'DD.MM.YYYY',
        LL : 'D MMMM YYYY',
        LLL : 'D MMMM YYYY, HH:mm',
        LLLL : 'dddd, D MMMM YYYY, HH:mm'
    }
});

import store from './store';

import { Router } from './router';

ReactDOM.render(
    <Provider store={store}>
        <Router />
    </Provider>,
    document.getElementById('root')
);
