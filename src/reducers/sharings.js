import R from 'ramda';
import { createReducer } from '../utils/reducer.js';

import { apiLoadSharingCounter } from '../utils/api';

export const loadSharingCounter = (url) => ({
    type: 'SHARING_COUNTER',
    payload: {
        counter: apiLoadSharingCounter(location.origin + url)
    },
    meta: {
        url
    }
});

const initialState = {};

export default createReducer({
    SHARING_COUNTER_SUCCESS: (state, { payload: { counter }, meta: { url } }) =>
        R.assoc(url, counter, state)

}, initialState);