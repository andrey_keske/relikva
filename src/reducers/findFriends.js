import R from 'ramda';
import { createReducer } from '../utils/reducer.js';

import { apiGetRecommendedFriends, apiSearchFriends } from '../utils/api';

export const loadRecommended = (count = 10) => ({
    type: 'USERS_SEARCH_LOAD_RECOMMENDED',
    payload: {
        users: apiGetRecommendedFriends(count)
    }
});
export const searchUsers = (query, page) => ({
    type: 'USERS_SEARCH',
    payload: {
        users: apiSearchFriends(query, page)
    }
});

const mapFollow = (username, follow) => R.map(
    R.ifElse(
        R.propEq('handle', username),
        R.assoc('follow', follow),
        R.identity
    )
);

const updateFollowFlag = (lens, username, follow) => R.over(lens, mapFollow(username, follow));

const LUsers = R.lensProp('users');

const initialState = {
    users: []
};

export default createReducer({
    USERS_SEARCH_LOAD_RECOMMENDED_REQUEST: (state, action) =>
        R.set(LUsers, [], state),

    USERS_SEARCH_LOAD_RECOMMENDED_SUCCESS: (state, { payload: { users } }) =>
        R.set(LUsers, users, state),

    USERS_SEARCH_SUCCESS: (state, { payload: { users }}) =>
        R.set(LUsers, users, state),

    SET_SUBSCRIBE_SUCCESS: (state, { payload: { subscribe: { follow }}, meta: { username }}) =>
        updateFollowFlag(LUsers, username, follow)(state)

}, initialState);