import R from 'ramda';
import { createReducer } from '../utils/reducer.js';

import {
    apiGetRelic,
    apiGetInvestigation,
    apiPostCommentReply,
    apiPostRelicComment,
    apiPostInvestigationComment,
    apiCreateInvestigation,
    apiDeleteInvestigation,
    apiSetLike,
    apiAddRelicToCollection,
    apiDeleteRelicFromCollection,
    apiUpdateRelic
} from '../utils/api';

export const loadRelic = id => ({
    type: 'RELIC',
    payload: {
        relic: apiGetRelic(id)
    }
});
export const postRelicComment = (id, parentId, body) => ({
    type: 'RELIC_POST_DISCUSSION_REPLY',
    payload: {
        comment: parentId ? apiPostCommentReply(parentId, body) : apiPostRelicComment(id, body)
    },
    meta: {
        id,
        parentId
    }
});
export const postInvestigationComment = (invId, body) => ({
    type: 'INVESTIGATION_POST_COMMENT',
    payload: {
        comment: apiPostInvestigationComment(invId, body)
    },
    meta: {
        investigationId: invId
    }
});
export const createInvestigation = (id, body) => ({
    type: 'INVESTIGATION_CREATE',
    payload: {
        investigation: apiCreateInvestigation(id, body)
    },
    meta: {
        id
    }
});
export const deleteInvestigation = (id) => ({
    type: 'INVESTIGATION_DELETE',
    payload: {
        investigation: apiDeleteInvestigation(id)
    },
    meta: {
        investigationId: id
    }
});
export const addRelicToCollection = (permalink, collectionId) => ({
    type: 'RELIC_ADD_TO_COLLECTION',
    payload: {
        relic: apiAddRelicToCollection(permalink, collectionId)
    },
    meta: {
        permalink,
        collectionId
    }
});
export const deleteRelicFromCollection = (permalink, collectionId) => ({
    type: 'RELIC_DELETE_FROM_COLLECTION',
    payload:{
        relic: apiDeleteRelicFromCollection(permalink, collectionId)
    },
    meta: {
        permalink,
        collectionId
    }
});
export const setRelicPrivacy = (id, privacy) => ({
    type: 'RELIC_SET_PRIVACY',
    payload: {
        relic: apiUpdateRelic(id, { privacy })
    },
    meta: {
        id
    }
});

const initialState = {};

const LRelic = R.lensProp('relic');
const LDiscus = R.lensProp('discussions');
const LInvs = R.lensProp('investigations');
const LInvComments = R.lensProp('investigation_comments');
const LCollectionIds = R.lensProp('collection_ids');
const getInvIndexLensById = (invId, invs = []) => R.lensIndex(R.findIndex(R.propEq('id', invId), invs));

export default createReducer({
    RELIC_REQUEST: (state, action) => R.identity({}),
    RELIC_FAILURE: (state, action) => R.identity({}),
    RELIC_SUCCESS: (state, { payload: { relic } }) => R.identity(relic),

    RELIC_SET_PRIVACY_SUCCESS: (state, { payload: { relic }}) =>
        R.clone(relic),

    RELIC_ADD_TO_COLLECTION_SUCCESS: (state, { meta: { collectionId }}) =>
        R.over(LCollectionIds, R.append(collectionId), state),

    RELIC_DELETE_FROM_COLLECTION_SUCCESS: (state, { meta: { collectionId }}) =>
        R.over(LCollectionIds, R.filter(R.complement(R.equals(collectionId))), state),

    SET_LIKE_SUCCESS: (state, { payload: { like: { like, count } }, meta: { id, type }}) =>
        R.cond([
            [
                R.always(R.equals('relic', type)),
                R.compose(R.assoc('like', like), R.assoc('likes_count', count))
            ],
            [
                R.always(R.equals('investigation', type)),
                R.over(
                    R.compose(LInvs, getInvIndexLensById(id, R.view(LInvs, state))),
                    R.compose(R.assoc('like', like), R.assoc('likes_count', count))
                )
            ],
            [
                R.T,
                R.identity
            ]
        ])(state),

    RELIC_POST_DISCUSSION_REPLY_SUCCESS: (state, { payload: { comment }, meta: { id }}) =>
        R.over(LDiscus, R.append(comment), state),

    INVESTIGATION_POST_COMMENT_SUCCESS: (state, { payload: { comment }, meta: { investigationId }}) =>
        R.over(LInvComments, R.append(comment), state),

    INVESTIGATION_CREATE_SUCCESS: (state, { payload: { investigation }, meta: { id }}) =>
        R.over(LInvs, R.prepend(investigation), state),

    INVESTIGATION_DELETE_SUCCESS: (state, { meta: { investigationId }}) =>
        R.over(LInvs, R.filter(R.complement(R.propEq('id', investigationId))), state)
}, initialState)
