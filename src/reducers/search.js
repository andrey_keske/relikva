import R from 'ramda';
import { createReducer } from '../utils/reducer.js';

import { apiSearchRelics, apiSearchCollections } from '../utils/api';

export const searchRelics = (query, count, page, friends, from, to) => ({
    type: 'SEARCH_RELICS',
    payload: {
        relics: apiSearchRelics(query, count, page, friends, from, to)
    }
});
export const searchCollections = (query, count, page, friends) => ({
    type: 'SEARCH_COLLECTIONS',
    payload: {
        collections: apiSearchCollections(query, count, page, friends)
    }
});
export const search = (query, count, page, friends = false) => ({
    type: 'SEARCH',
    payload: {
        relics: apiSearchRelics(query, count, page, friends),
        collections: apiSearchCollections(query, count, page, friends)
    },
    meta: {
        page
    }
});

const LRelics = R.lensProp('relics');
const LCollections = R.lensProp('collections');

const initialState = {
    relics: [],
    collections: []
};

export default createReducer({
    SEARCH_REQUEST: (state, { meta: { page }}) =>
        R.ifElse(
            R.always(R.equals(page, 1)),
            R.compose(
                R.set(LRelics, []),
                R.set(LCollections, [])
            ),
            R.identity
        )(state),

    SEARCH_SUCCESS: (state, { payload: { relics: { relics }, collections: { collections }}}) =>
        R.compose(
            R.over(LRelics, R.flip(R.concat)(relics)),
            R.over(LCollections, R.flip(R.concat)(collections))
        )(state)
}, initialState)
