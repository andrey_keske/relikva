import R from 'ramda';
import { createReducer } from '../utils/reducer.js';

import { apiCreateRelic } from '../utils/api.js'

export const createRelic = (params) => ({
    type: 'RELIC_CREATE',
    payload: {
        relic: apiCreateRelic(params)
    }
});
export const clearRelic = (query) => ({
    type: 'RELIC_CLEAR'
});

const LRelic = R.lensProp('relic');

const initialState = {
    relic: {}
};

export default createReducer({
    RELIC_CREATE_SUCCESS: (state, { payload: { relic }}) =>
        R.set(LRelic, relic, state),

    RELIC_CLEAR: (state, action) =>
        R.clone(initialState)

}, initialState);
