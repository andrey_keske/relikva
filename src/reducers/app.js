import R from 'ramda';
import { createReducer } from '../utils/reducer.js';

import { apiGetRecommendedFriends, apiSearchFriends, apiSetSubscribe, apiSetLike, apiLogout } from '../utils/api';

export const loadRecommendedFriends = (count = 10) => ({
    type: 'RECOMMENDED_FRIENDS',
    payload: {
        users: apiGetRecommendedFriends(count)
    }
});
export const loadCurrentUser = () => ({
    type: 'USER_CURRENT_LOAD',
    payload: {
        user: window.currentUser
    }
});
export const openModal = (component) => ({
    type: 'MODAL_OPEN',
    payload: {
        component
    }
});
export const closeModal = () => ({type: 'MODAL_CLOSE'});
export const setLike = (type, id, like) => ({
    type: 'SET_LIKE',
    payload: {
        like: apiSetLike(type, id, like)
    },
    meta: {
        type,
        id,
        like
    }
});
export const setSubscribe = (username, subscribe) => ({
    type: 'SET_SUBSCRIBE',
    payload: {
        subscribe: apiSetSubscribe(username, subscribe)
    },
    meta: {
        username,
        subscribe
    }
});
export const logout = () => ({
    type: 'LOGOUT',
    payload: {
        logout: apiLogout()
    }
});

const mapFollow = (username, follow) => R.map(
    R.ifElse(
        R.propEq('handle', username),
        R.assoc('follow', follow),
        R.identity
    )
);

const updateFollowFlag = (lens, username, follow) => R.over(lens, mapFollow(username, follow));

const initialState = {
    recommendedFriends: [],
    currentUser: {},
    modals: []
};

export default createReducer({
    MODAL_OPEN: (state, { payload: { component } }) =>
        R.over(
            R.lensProp('modals'),
            R.append(component),
            state
        ),

    MODAL_CLOSE: (state, action) =>
        R.over(
            R.lensProp('modals'),
            R.dropLast(1),
            state
        ),
    
    RECOMMENDED_FRIENDS_REQUEST: (state, action) =>
        R.assoc('recommendedFriends', [], state),

    RECOMMENDED_FRIENDS_SUCCESS: (state, { payload: { users } }) =>
        R.assoc('recommendedFriends', users, state),

    USER_CURRENT_LOAD: (state, { payload: { user } }) =>
        R.assoc('currentUser', user, state),

    SET_SUBSCRIBE_SUCCESS: (state, { payload: { subscribe: { follow }}, meta: { username }}) =>
        updateFollowFlag(R.lensProp('recommendedFriends'), username, follow)(state)

}, initialState);