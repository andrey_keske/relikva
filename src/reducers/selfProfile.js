import R from 'ramda';
import { createReducer } from '../utils/reducer.js';

import { apiGetSelfProfile, apiUpdateSelfProfile, apiUpdatePassword, apiUpdateAvatar, apiDeleteAvatar } from '../utils/api';

export const loadSelfProfile = () => ({
    type: 'SELF_PROFILE_LOAD',
    payload: {
        user: apiGetSelfProfile()
    }
});
export const updateSelfProfile = (user) => ({
    type: 'SELF_PROFILE_UPDATE',
    payload: {
        user: apiUpdateSelfProfile(user)
    }
});
export const updatePassword = (password, newPassword, newPasswordConfirm) => ({
    type: 'PASSWORD_UPDATE',
    payload: {
        password: apiUpdatePassword(password, newPassword, newPasswordConfirm)
    }
});
export const updateAvatar = (file) => ({
    type: 'AVATAR_UPDATE',
    payload: {
        user: apiUpdateAvatar(file)
    }
});
export const deleteAvatar = () => ({
    type: 'AVATAR_DELETE',
    payload: {
        user: apiDeleteAvatar()
    }
});

const initialState = {};

export default createReducer({
    SELF_PROFILE_LOAD_REQUEST: (state, action) => R.identity({}),
    SELF_PROFILE_LOAD_SUCCESS: (state, { payload: { user }}) => user,
    SELF_PROFILE_UPDATE_SUCCESS: (state, { payload: { user }}) => user,
    AVATAR_UPDATE_SUCCESS: (state, { payload: { user }}) => user,
    AVATAR_DELETE_SUCCESS: (state, { payload: { user }}) => user

}, initialState);