import R from 'ramda';
import { createReducer } from '../utils/reducer.js';

import { apiGetRelics } from '../utils/api';

export const loadRelics = (count, page) => ({
    type: 'NOT_FOUND_LOAD_RELICS',
    payload: {
        relics: apiGetRelics(count, page)
    }
});

const initialState = {
    relics: []
};

const LRelics = R.lensProp('relics');

export default createReducer({
    NOT_FOUND_LOAD_RELICS_REQUEST: (state, action) =>
        R.set(LRelics, [], state),

    NOT_FOUND_LOAD_RELICS_SUCCESS: (state, { payload: { relics }}) =>
        R.set(LRelics, relics, state)
}, initialState)
