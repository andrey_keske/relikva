import R from 'ramda';
import { createReducer } from '../utils/reducer.js';

import { apiGetRelics, apiGetCollections, apiGetInvestigations } from '../utils/api';

export const FEED_TYPE_POPULAR = 'FEED_TYPE_POPULAR';
export const FEED_TYPE_SUBSCRIPTIONS = 'FEED_TYPE_SUBSCRIPTIONS';
export const FEED_TYPE_ALL = 'FEED_TYPE_ALL';

const RELICS_COUNT = 6;
const COLLECTIONS_COUNT = 2;
const INVESTIGATION_COUNT = 3;

export const setTimelineType = (feedType) => ({type: 'TIMELINE_SET_FEED_TYPE', payload: { feedType }});
export const clearTimelineItems = () => ({type: 'TIMELINE_CLEAR'});
export const loadTimelineItems = (page, friends, popular) => ({
    type: 'TIMELINE',
    payload: {
        relics: apiGetRelics(RELICS_COUNT, page, friends, popular),
        collections: apiGetCollections(COLLECTIONS_COUNT, page, friends, popular),
        investigations: apiGetInvestigations(INVESTIGATION_COUNT, page, friends, popular)
    },
    meta: {
        page
    }
});

const setType = (type, items) => R.map(R.set(R.lensProp('_type'), type), items);
const LItems = R.lensProp('items');
const LFeedType = R.lensProp('feedType');
const getIndexLensByItemId = (id, items) => R.lensIndex(R.findIndex(R.propEq('id', id), items));

const initialState = {
    feedType: FEED_TYPE_ALL,
    items: []
};

export default createReducer({
    SET_LIKE_SUCCESS: (state, { payload: { like: { like, count } }, meta: { id, type }}) =>
        R.over(
            R.compose(LItems, getIndexLensByItemId(id, R.view(LItems, state))),
            R.compose(
                R.assoc('like', like),
                R.assoc('likes_count', count)
            )
        )(state),

    TIMELINE_SUCCESS: (state, { payload: { relics, collections, investigations }, meta: { page }}) => {
        const items = R.reduce(
            R.concat,
            [],
            [
                setType('relic', relics),
                setType('collection', collections),
                setType('investigation', investigations)
            ]
        );

        if (page === 1) {
            return R.set(LItems, items, state);
        } else {
            return R.over(LItems, R.flip(R.concat)(items), state);
        }
    },

    TIMELINE_CLEAR: (state, action) => R.assoc('items', [], state),
    TIMELINE_SET_FEED_TYPE: (state, { payload: { feedType } }) =>
        R.set(LFeedType, feedType, state)
}, initialState);