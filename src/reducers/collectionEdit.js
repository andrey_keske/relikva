import R from 'ramda';
import { createReducer } from '../utils/reducer.js';

import { apiGetCollection, apiUpdateCollection, apiDeleteCollection } from '../utils/api.js'

export const loadCollection = (id, relics_count = 20, page = 1) => ({
    type: 'COLLECTION_EDIT_LOAD',
    payload: {
        collection: apiGetCollection(id, relics_count, page)
    }
});
export const loadCollectionRelics = (relics_count = 20, page = 1) => ({
    type: 'COLLECTION_EDIT_RELICS',
    payload: {
        relics: apiGetCollection(relics_count, page)
    }
});
export const updateCollection = (id, params) => ({
    type: 'COLLECTION_UPDATE',
    payload: {
        collection: apiUpdateCollection(id, params)
    },
    meta: {
        id
    }
});
export const deleteCollection = (id) => ({
    type: 'COLLECTION_DELETE',
    payload: {
        collection: apiDeleteCollection(id)
    },
    meta: {
        id
    }
});

const LCollection = R.lensProp('collection');
const LRelics = R.lensProp('relics');

const initialState = {
    collection: {}
};

export default createReducer({
    COLLECTION_EDIT_LOAD_SUCCESS: (state, { payload: { collection: { collection, relics }}}) =>
        R.compose(
            R.over(LRelics, R.flip(R.concat)(relics)),
            R.set(LCollection, collection)
        )(state),

    COLLECTION_UPDATE_SUCCESS: (state, { payload: { collection }}) =>
        R.set(LCollection, collection, state)

}, initialState);
