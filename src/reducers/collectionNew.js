import R from 'ramda';
import { createReducer } from '../utils/reducer.js';

import { apiGetSelfRelics, apiUpdateCollection, apiDeleteCollection, apiCreateCollection } from '../utils/api.js'

export const createCollection = (params) => ({
    type: 'COLLECTION_CREATE',
    payload: {
        collection: apiCreateCollection(params)
    }
});

const LCollection = R.lensProp('collection');

const initialState = {
    collection: {}
};

export default createReducer({
    COLLECTION_CREATE_SUCCESS: (state, { payload: { collection }}) =>
        R.set(LCollection, collection, state)

}, initialState);
