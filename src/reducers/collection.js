import R from 'ramda';
import { createReducer } from '../utils/reducer.js';

import { apiGetCollection, apiSetLike, apiUpdateCollection } from '../utils/api';

export const loadCollection = (id, count, page) => ({
    type: 'COLLECTION',
    payload: {
        collection: apiGetCollection(id, count, page)
    },
    meta: {
        page
    }
});
export const setCollectionPrivacy = (id, privacy) => ({
    type: 'COLLECTION_SET_PRIVACY',
    payload: {
        collection: apiUpdateCollection(id, { privacy })
    },
    meta: {
        id
    }
});

const initialState = {
    collection: {},
    relics: []
};

const LCollection = R.lensProp('collection');
const LRelics = R.lensProp('relics');
const getRelicIndexLensById = (id, items = []) => R.lensIndex(R.findIndex(R.propEq('id', id), items));

export default createReducer({
    COLLECTION_REQUEST: (state, { meta: { page }}) =>
        R.ifElse(
            R.always(R.equals(page, 1)),
            R.always(R.clone(initialState)),
            R.identity
        )(state),
    COLLECTION_SUCCESS: (state, { payload: { collection: { collection, relics }}, meta: { page }}) =>
        R.ifElse(
            R.always(R.equals(page, 1)),
            R.compose(
                R.set(LCollection, collection),
                R.set(LRelics, R.defaultTo([])(relics))
            ),
            R.over(LRelics, R.flip(R.concat)(relics))
        )(state),

    COLLECTION_SET_PRIVACY_SUCCESS: (state, { payload: { collection }}) =>
        R.set(LCollection, collection, state),

    SET_LIKE_SUCCESS: (state, { payload: { like: { like, count } }, meta: { id, type }}) =>
        R.cond([
            [
                R.always(R.equals('collection', type)),
                R.over(LCollection, R.compose(R.assoc('like', like), R.assoc('likes_count', count)))
            ],
            [
                R.always(R.equals('relic', type)),
                R.over(
                    R.compose(LRelics, getRelicIndexLensById(id, R.view(LRelics, state))),
                    R.compose(R.assoc('like', like), R.assoc('likes_count', count))
                )
            ],
            [
                R.T,
                R.identity
            ]
        ])(state)

}, initialState)
