import R from 'ramda';
import { createReducer } from '../utils/reducer.js';

const initialStatus = {
    isFetching: false,
    errors: []
};

const initialInv = {
    deletion: {},
    reply: {}
};

//TODO пагинация
const initialState = {
    addToCollection: {
        list: R.clone(initialStatus),
        form: R.clone(initialStatus),
        paginated: {}
    },
    relic: {
        self: R.clone(initialStatus),
        newInvestigation: R.clone(initialStatus),
        investigations: {},
        discussionReply: R.clone(initialStatus),
        addToCollection: R.clone(initialStatus)
    },
    collection: {
        self: R.clone(initialStatus),
        paginated: {}
    },
    timeline: {
        recommendedFriends: R.clone(initialStatus),
        feed: R.clone(initialStatus)
    },
    userFeed: {
        user: R.clone(initialStatus),
        relics: {
            self: R.clone(initialStatus),
            paginated: {}
        },
        collections: {
            self: R.clone(initialStatus),
            paginated: {}
        }
    },
    subscribe: {},
    like: {},
    collectionNew: {
        self: R.clone(initialStatus)
    },
    collectionEdit: {
        self: R.clone(initialStatus),
        relics: R.clone(initialStatus)
    },
    selfProfile: {
        self: R.clone(initialStatus),
        password: R.clone(initialStatus)
    },
    activityFeed: {
        self: R.clone(initialStatus),
        paginated: {}
    },
    notFound: {
        relics: R.clone(initialStatus)
    },
    search: {
        self: R.clone(initialStatus),
        relics: {
            paginated: {}
        },
        collections: {
            paginated: {}
        }
    },
    relicNew: {
        self: R.clone(initialStatus)
    },
    relicEdit: {
        self: R.clone(initialStatus)
    }
};

const makeError = (key, err) => `${key}: ${err}`;
const reduceErrorsList = (key) => R.reduce(
    (errsAcc, err) => R.append(
        makeError(key, err),
        errsAcc
    ),
    []
);
const reduceErrorsObj = (obj) => R.reduce(
    (acc, key) => R.append(
        R.ifElse(
            R.isArrayLike,
            reduceErrorsList(key),
            (err) => makeError(key, err)
        )(obj[key]),
        acc
    ),
    [],
    R.keys(obj)
);

const validationErrorsPath = ['body', 'errors'];
const getErrors = R.cond([
    [ //TODO Сохранение названия полей содержащих ошибку. Переделать массив ошибок на объект.
        R.pipe(R.path(validationErrorsPath), R.values, R.isEmpty, R.not),
        R.pipe(R.path(validationErrorsPath), reduceErrorsObj, R.flatten)
    ],
    [R.propSatisfies(R.complement(R.isNil), 'message'), e => [R.prop('message', e)]],
    [R.propSatisfies(R.complement(R.isEmpty), 'message'), e => [R.prop('message', e)]],
    [R.T, R.always(['Неизвестная ошибка'])]
]);

const statusRequest = R.compose(R.assoc('errors', []), R.assoc('isFetching', true));
const statusSuccess = R.compose(R.assoc('errors', []), R.assoc('isFetching', false));
const statusFailure = (err) => R.compose(R.assoc('errors', getErrors(err)), R.assoc('isFetching', false));

const LRelic = R.lensProp('relic');
const LSelf = R.lensProp('self');
const LLike = R.lensProp('like');
const LDiscussionReply = R.lensProp('discussionReply');
const LInvestigations = R.lensProp('investigations');
const LNewInvestigation = R.lensProp('newInvestigation');
const LDeletion = R.lensProp('deletion');
const LReply = R.lensProp('reply');
const LCollection = R.lensProp('collection');
const LRelics = R.lensProp('relics');
const LAddToCollection = R.lensProp('addToCollection');
const LForm = R.lensProp('form');
const LList = R.lensProp('list');
const LTimeline = R.lensProp('timeline');
const LFeed = R.lensProp('feed');
const LRecommendedFriends = R.lensProp('recommendedFriends');
const LUserFeed = R.lensProp('userFeed');
const LUser = R.lensProp('user');
const LCollections = R.lensProp('collections');
const LSubscribe = R.lensProp('subscribe');
const LCollectionNew = R.lensProp('collectionNew');
const LSelfProfile = R.lensProp('selfProfile');
const LPassword = R.lensProp('password');
const LPaginated = R.lensProp('paginated');
const LActivityFeed = R.lensProp('activityFeed');
const LCollectionEdit = R.lensProp('collectionEdit');
const LNotFound = R.lensProp('notFound');
const LSearch = R.lensProp('search');
const LRelicNew = R.lensProp('relicNew');
const LRelicEdit = R.lensProp('relicEdit');

const LSubscribeUser = (username) => R.compose(LSubscribe, R.lensProp(username));
const LInv = (id) => R.compose(LRelic, LInvestigations, R.lensProp(id));
const createSubsUser = R.ifElse(R.isNil, R.always(initialStatus), R.identity);
const createInv = R.ifElse(R.isNil, R.always(initialInv), R.identity);

const invReply = (invId, status) => R.compose(
    R.over(R.compose(LInv(invId), LReply), status),
    R.over(LInv(invId), createInv)
);

const invDelete = (invId, status) => R.compose(
    R.over(R.compose(LInv(invId), LDeletion), status),
    R.over(LInv(invId), createInv)
);

const invLike = (invId, status) => R.compose(
    R.over(R.compose(LInv(invId), LLike), status),
    R.over(LInv(invId), createInv)
);

const createLike = (type, id) => R.ifElse(
    R.compose(R.isNil, R.path([type, id])),
    R.assocPath([type, id], initialStatus),
    R.identity
);

const setLike = (type, id, status) => R.compose(
    R.over(R.compose(LLike, R.lensProp(type), R.lensProp(id)), status),
    R.over(LLike, createLike(type, id))
);

export default createReducer({
    RELIC_UPDATE_REQUEST: (state, action) => R.over(R.compose(LRelicEdit, LSelf), statusRequest, state),
    RELIC_UPDATE_SUCCESS: (state, action) => R.over(R.compose(LRelicEdit, LSelf), statusSuccess, state),
    RELIC_UPDATE_FAILURE: (state, { payload }) => R.over(R.compose(LRelicEdit, LSelf), statusFailure(payload), state),

    RELIC_CLEAR: (state, action) => R.over(R.compose(LRelicNew, LSelf), R.always(R.clone(initialStatus)), state),

    RELIC_CREATE_REQUEST: (state, action) => R.over(R.compose(LRelicNew, LSelf), statusRequest, state),
    RELIC_CREATE_SUCCESS: (state, action) => R.over(R.compose(LRelicNew, LSelf), statusSuccess, state),
    RELIC_CREATE_FAILURE: (state, { payload }) => R.over(R.compose(LRelicNew, LSelf), statusFailure(payload), state),

    SEARCH_REQUEST: (state, action) => R.over(R.compose(LSearch, LSelf), statusRequest, state),
    SEARCH_SUCCESS: (state, { payload: { relics, collections }}) =>
        R.compose(
            R.over(R.compose(LSearch, LSelf), statusSuccess),
            R.set(R.compose(LSearch, LRelics, LPaginated), relics.paginated),
            R.set(R.compose(LSearch, LCollections, LPaginated), collections.paginated)
        )(state),
    SEARCH_FAILURE: (state, { payload }) => R.over(R.compose(LSearch, LSelf), statusFailure(payload), state),

    NOT_FOUND_LOAD_RELICS_REQUEST: (state, action) => R.over(R.compose(LNotFound, LRelics), statusRequest, state),
    NOT_FOUND_LOAD_RELICS_SUCCESS: (state, action) => R.over(R.compose(LNotFound, LRelics), statusSuccess, state),
    NOT_FOUND_LOAD_RELICS_FAILURE: (state, { payload }) => R.over(R.compose(LNotFound, LRelics), statusFailure(payload), state),

    COLLECTION_UPDATE_REQUEST: (state, action) => R.over(R.compose(LCollectionEdit, LSelf), statusRequest, state),
    COLLECTION_UPDATE_SUCCESS: (state, action) => R.over(R.compose(LCollectionEdit, LSelf), statusSuccess, state),
    COLLECTION_UPDATE_FAILURE: (state, { payload }) => R.over(R.compose(LCollectionEdit, LSelf), statusFailure(payload), state),

    COLLECTION_DELETE_REQUEST: (state, action) => R.over(R.compose(LCollectionEdit, LSelf), statusRequest, state),
    COLLECTION_DELETE_SUCCESS: (state, action) => R.over(R.compose(LCollectionEdit, LSelf), statusSuccess, state),
    COLLECTION_DELETE_FAILURE: (state, { payload }) => R.over(R.compose(LCollectionEdit, LSelf), statusFailure(payload), state),

    ACTIVITY_FEED_LOAD_REQUEST: (state, action) => R.over(R.compose(LActivityFeed, LSelf), statusRequest, state),
    ACTIVITY_FEED_LOAD_SUCCESS: (state, { payload: { items: { paginated }}}) => R.compose(
        R.over(R.compose(LActivityFeed, LSelf), statusSuccess),
        R.set(R.compose(LActivityFeed, LPaginated), paginated)
    )(state),
    ACTIVITY_FEED_LOAD_FAILURE: (state, { payload }) => R.over(R.compose(LActivityFeed, LSelf), statusFailure(payload), state),

    SELF_PROFILE_LOAD_REQUEST: (state, action) => R.over(R.compose(LSelfProfile, LSelf), statusRequest, state),
    SELF_PROFILE_LOAD_SUCCESS: (state, action) => R.over(R.compose(LSelfProfile, LSelf), statusSuccess, state),
    SELF_PROFILE_LOAD_FAILURE: (state, { payload }) => R.over(R.compose(LSelfProfile, LSelf), statusFailure(payload), state),

    SELF_PROFILE_UPDATE_REQUEST: (state, action) => R.over(R.compose(LSelfProfile, LSelf), statusRequest, state),
    SELF_PROFILE_UPDATE_SUCCESS: (state, action) => R.over(R.compose(LSelfProfile, LSelf), statusSuccess, state),
    SELF_PROFILE_UPDATE_FAILURE: (state, { payload }) => R.over(R.compose(LSelfProfile, LSelf), statusFailure(payload), state),

    PASSWORD_UPDATE_REQUEST: (state, action) => R.over(R.compose(LSelfProfile, LPassword), statusRequest, state),
    PASSWORD_UPDATE_SUCCESS: (state, action) => R.over(R.compose(LSelfProfile, LPassword), statusSuccess, state),
    PASSWORD_UPDATE_FAILURE: (state, { payload }) => R.over(R.compose(LSelfProfile, LPassword), statusFailure(payload), state),

    COLLECTION_CREATE_REQUEST: (state, action) => R.over(R.compose(LCollectionNew, LSelf), statusRequest, state),
    COLLECTION_CREATE_SUCCESS: (state, action) => R.over(R.compose(LCollectionNew, LSelf), statusSuccess, state),
    COLLECTION_CREATE_FAILURE: (state, { payload }) => R.over(R.compose(LCollectionNew, LSelf), statusFailure(payload), state),

    SET_LIKE_REQUEST: (state, { meta: { type, id }}) => setLike(type, id, statusRequest)(state),
    SET_LIKE_SUCCESS: (state, { meta: { type, id }}) => setLike(type, id, statusSuccess)(state),
    SET_LIKE_FAILURE: (state, { payload, meta: { type, id }}) => setLike(type, id, statusFailure(payload))(state),

    SET_SUBSCRIBE_REQUEST: (state, { meta: { username }}) => R.over(LSubscribeUser(username), statusRequest, state),
    SET_SUBSCRIBE_SUCCESS: (state, { meta: { username }}) => R.over(LSubscribeUser(username), statusSuccess, state),
    SET_SUBSCRIBE_FAILURE: (state, { payload, meta: { username }}) => R.over(LSubscribeUser(username), statusFailure(payload), state),

    USER_REQUEST: (state, action) => R.over(R.compose(LUserFeed, LUser), statusRequest, state),
    USER_SUCCESS: (state, action) => R.over(R.compose(LUserFeed, LUser), statusSuccess, state),
    USER_FAILURE: (state, { payload }) => R.over(R.compose(LUserFeed, LUser), statusFailure(payload), state),

    USER_RELICS_REQUEST: (state, action) => R.over(R.compose(LUserFeed, LRelics, LSelf), statusRequest, state),
    USER_RELICS_SUCCESS: (state, { payload: { relics: { paginated }}}) => R.compose(
        R.over(R.compose(LUserFeed, LRelics, LSelf), statusSuccess),
        R.set(R.compose(LUserFeed, LRelics, LPaginated), paginated)
    )(state),
    USER_RELICS_FAILURE: (state, { payload }) => R.over(R.compose(LUserFeed, LRelics, LSelf), statusFailure(payload), state),

    USER_COLLECTIONS_REQUEST: (state, action) => R.over(R.compose(LUserFeed, LCollections, LSelf), statusRequest, state),
    USER_COLLECTIONS_SUCCESS: (state, { payload: { collections: { paginated }}}) => R.compose(
        R.over(R.compose(LUserFeed, LCollections, LSelf), statusSuccess),
        R.set(R.compose(LUserFeed, LCollections, LPaginated), paginated)
    )(state),
    USER_COLLECTIONS_FAILURE: (state, { payload }) => R.over(R.compose(LUserFeed, LCollections, LSelf), statusFailure(payload), state),

    TIMELINE_REQUEST: (state, action) => R.over(R.compose(LTimeline, LFeed), statusRequest, state),
    TIMELINE_SUCCESS: (state, action) => R.over(R.compose(LTimeline, LFeed), statusSuccess, state),
    TIMELINE_FAILURE: (state, { payload }) => R.over(R.compose(LTimeline, LFeed), statusFailure(payload), state),

    RECOMMENDED_FRIENDS_REQUEST: (state, action) => R.over(R.compose(LTimeline, LRecommendedFriends), statusRequest, state),
    RECOMMENDED_FRIENDS_SUCCESS: (state, action) => R.over(R.compose(LTimeline, LRecommendedFriends), statusSuccess, state),
    RECOMMENDED_FRIENDS_FAILURE: (state, { payload }) => R.over(R.compose(LTimeline, LRecommendedFriends), statusFailure(payload), state),

    COLLECTIONS_SELF_REQUEST: (state, action) => R.over(R.compose(LAddToCollection, LList), statusRequest, state),
    COLLECTIONS_SELF_SUCCESS: (state, { payload: { collections: { paginated }}}) => R.compose(
        R.over(R.compose(LAddToCollection, LList), statusSuccess),
        R.set(R.compose(LAddToCollection, LPaginated), paginated)
    )(state),
    COLLECTIONS_SELF_FAILURE: (state, { payload }) => R.over(R.compose(LAddToCollection, LList), statusFailure(payload), state),

    COLLECTION_REQUEST: (state, action) => R.over(R.compose(LCollection, LSelf), statusRequest, state),
    COLLECTION_SUCCESS: (state, { payload: { collection: { paginated }}}) => R.compose(
        R.over(R.compose(LCollection, LSelf), statusSuccess),
        R.set(R.compose(LCollection, LPaginated), R.defaultTo({})(paginated))
    )(state),
    COLLECTION_FAILURE: (state, { payload }) => R.over(R.compose(LCollection, LSelf), statusFailure(payload), state),

    RELIC_REQUEST: (state, action) => R.over(R.compose(LRelic, LSelf), statusRequest, state),
    RELIC_SUCCESS: (state, action) => R.over(R.compose(LRelic, LSelf), statusSuccess, state),
    RELIC_FAILURE: (state, { payload }) => R.over(R.compose(LRelic, LSelf), statusFailure(payload), state),

    RELIC_LIKE_TOGGLE_REQUEST: (state, action) => R.over(R.compose(LRelic, LLike), statusRequest, state),
    RELIC_LIKE_TOGGLE_SUCCESS: (state, action) => R.over(R.compose(LRelic, LLike), statusSuccess, state),
    RELIC_LIKE_TOGGLE_FAILURE: (state, { payload }) => R.over(R.compose(LRelic, LLike), statusFailure(payload), state),

    INVESTIGATION_LIKE_TOGGLE_REQUEST: (state, { meta: { investigationId }}) =>
        invLike(investigationId, statusRequest)(state),
    INVESTIGATION_LIKE_TOGGLE_SUCCESS: (state, { meta: { investigationId }}) =>
        invLike(investigationId, statusSuccess)(state),
    INVESTIGATION_LIKE_TOGGLE_FAILURE: (state, { payload, meta: { investigationId }}) =>
        invLike(investigationId, statusFailure(payload))(state),

    RELIC_POST_DISCUSSION_REPLY_REQUEST: (state, { meta: { id }}) => R.over(R.compose(LRelic, LDiscussionReply), statusRequest, state),
    RELIC_POST_DISCUSSION_REPLY_SUCCESS: (state, { meta: { id }}) => R.over(R.compose(LRelic, LDiscussionReply), statusSuccess, state),
    RELIC_POST_DISCUSSION_REPLY_FAILURE: (state, { payload, meta: { id }}) => R.over(R.compose(LRelic, LDiscussionReply), statusFailure(payload), state),

    INVESTIGATION_CREATE_REQUEST: (state, { meta: { id } }) => R.over(R.compose(LRelic, LNewInvestigation), statusRequest, state),
    INVESTIGATION_CREATE_SUCCESS: (state, { meta: { id } }) => R.over(R.compose(LRelic, LNewInvestigation), statusSuccess, state),
    INVESTIGATION_CREATE_FAILURE: (state, { payload, meta: { id } }) => R.over(R.compose(LRelic, LNewInvestigation), statusFailure(payload), state),

    INVESTIGATION_DELETE_REQUEST: (state, { meta: { investigationId }}) =>
        invDelete(investigationId, statusRequest)(state),
    INVESTIGATION_DELETE_SUCCESS: (state, { meta: { investigationId }}) =>
        invDelete(investigationId, statusSuccess)(state),
    INVESTIGATION_DELETE_FAILURE: (state, { payload, meta: { investigationId }}) =>
        invDelete(investigationId, statusFailure(payload))(state),

    INVESTIGATION_POST_COMMENT_REQUEST: (state, { meta: { investigationId }}) =>
        invReply(investigationId, statusRequest)(state),
    INVESTIGATION_POST_COMMENT_SUCCESS: (state, { meta: { investigationId }}) =>
        invReply(investigationId, statusSuccess)(state),
    INVESTIGATION_POST_COMMENT_FAILURE: (state, { payload, meta: { investigationId }}) =>
        invReply(investigationId, statusFailure(payload))(state)

}, initialState);