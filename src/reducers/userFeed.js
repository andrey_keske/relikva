import R from 'ramda';
import { createReducer } from '../utils/reducer.js';

import { apiGetUserRelics, apiGetUserCollections, apiLoadUser } from '../utils/api';

export const loadUser = (username) => ({
    type: 'USER',
    payload: {
        user: apiLoadUser(username)
    }
});
export const loadUserRelics = (username, count, page) => ({
    type: 'USER_RELICS',
    payload: {
        relics: apiGetUserRelics(username, count, page)
    }
});
export const loadUserCollections = (username, count, page) => ({
    type: 'USER_COLLECTIONS',
    payload: {
        collections: apiGetUserCollections(username, count, page)
    }
});
export const clearUserFeed = () => ({
    type: 'USER_FEED_CLEAR'
});

const LUser = R.lensProp('user');
const LRelics = R.lensProp('relics');
const LCollections = R.lensProp('collections');

const setType = (type, items) => R.map(R.set(R.lensProp('_type'), type), items);
const getIndexLensByItemId = (id, items) => R.lensIndex(R.findIndex(R.propEq('id', id), items));

const initialState = {
    user: {},
    relics: [],
    collections: []
};

export default createReducer({
    SET_LIKE_SUCCESS: (state, { payload: { like: { like, count } }, meta: { id, type }}) =>
            R.cond([
                [
                    R.always(R.equals('relic', type)),
                    R.over(
                        R.compose(LRelics, getIndexLensByItemId(id, R.view(LRelics, state))),
                        R.compose(
                            R.assoc('like', like),
                            R.assoc('likes_count', count)
                        )
                    )
                ],
                [
                    R.always(R.equals('collection', type)),
                    R.over(
                        R.compose(LCollections, getIndexLensByItemId(id, R.view(LCollections, state))),
                        R.compose(
                            R.assoc('like', like),
                            R.assoc('likes_count', count)
                        )
                    )
                ],
                [
                    R.T,
                    R.identity
                ]
            ])(state),

    USER_FEED_CLEAR: (state, action) =>
        R.clone(initialState),

    SET_SUBSCRIBE_SUCCESS: (state, { payload: { subscribe: { follow }}, meta: { username }}) =>
        R.over(LUser, R.assoc('follow', follow), state),

    USER_SUCCESS: (state, { payload: { user }}) =>
        R.set(LUser, user, state),

    USER_RELICS_SUCCESS: (state, { payload: { relics: { relics }}, meta: { username }}) =>
        R.over(LRelics, R.flip(R.concat)(setType('relic', R.defaultTo([])(relics))), state),

    USER_COLLECTIONS_SUCCESS: (state, { payload: { collections: { collections }}, meta: { username }}) =>
        R.over(LCollections, R.flip(R.concat)(setType('collection', R.defaultTo([])(collections))), state)

}, initialState);