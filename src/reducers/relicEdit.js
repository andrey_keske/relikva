import R from 'ramda';
import { createReducer } from '../utils/reducer.js';

import { apiGetRelic, apiUpdateRelic, apiDeleteRelic } from '../utils/api.js'

export const loadRelic = (id) => ({
    type: 'RELIC_EDIT_LOAD',
    payload: {
        relic: apiGetRelic(id)
    }
});
export const updateRelic = (id, params) => ({
    type: 'RELIC_UPDATE',
    payload: {
        relic: apiUpdateRelic(id, params)
    },
    meta: {
        id
    }
});
export const deleteRelic = (id) => ({
    type: 'RELIC_DELETE',
    payload: {
        relic: apiDeleteRelic(id)
    },
    meta: {
        id
    }
});

const LRelic = R.lensProp('relic');

const initialState = {
    relic: {}
};

export default createReducer({
    RELIC_EDIT_LOAD_REQUEST: (state, action) =>
        R.clone(initialState),

    RELIC_EDIT_LOAD_SUCCESS: (state, { payload: { relic }}) =>
        R.set(LRelic, relic, state),

    RELIC_UPDATE_SUCCESS: (state, { payload: { relic }}) =>
        R.set(LRelic, relic, state)

}, initialState);
