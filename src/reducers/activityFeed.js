import R from 'ramda';
import { createReducer } from '../utils/reducer.js';

import { apiLoadActivityFeedItems, apiMarkActivityFeedItems, apiUpdateActivityFeedCounter } from '../utils/api';

export const loadActivityFeedItems = (count, page, newest) => ({
    type: 'ACTIVITY_FEED_LOAD',
    payload: {
        items: apiLoadActivityFeedItems(count, page, newest)
    },
    meta: {
        count,
        page,
        newest
    }
});
export const markActivityFeedItems = (ids) => ({
    type: 'ACTIVITY_FEED_MARK',
    payload: {
        items: apiMarkActivityFeedItems(ids)
    },
    meta: {
        ids
    }
});
export const updateActivityFeedCounter = () => ({
    type: 'ACTIVITY_FEED_UPDATE_COUNTER',
    payload: {
        counter: apiUpdateActivityFeedCounter()
    }
});
export const clearActivityFeedItems = () => ({
    type: 'ACTIVITY_FEED_CLEAR'
});

const initialState = {
    items: [],
    counter: {
        newest: 0,
        total: 0
    }
};

const LItems = R.lensProp('items');
const LCounter = R.lensProp('counter');

export default createReducer({
    ACTIVITY_FEED_CLEAR: (state, action) =>
        R.set(LItems, [], state),

    ACTIVITY_FEED_LOAD_SUCCESS: (state, { payload: { items: { items } }}) =>
        R.over(LItems, R.compose(R.reverse, R.sortBy(R.prop('id')), R.concat(items)), state),

    ACTIVITY_FEED_MARK_SUCCESS: (state, { payload: { items: { paginated: { newest_items }}}, meta: { ids }}) =>
        R.compose(
            R.over(
                LItems,
                R.map(
                    R.ifElse(
                        (item) => R.contains(R.prop('id', item), ids),
                        R.assoc('newest', false),
                        R.identity
                    )
                )
            ),
            R.over(LCounter, R.assoc('newest', newest_items))
        )(state),

    ACTIVITY_FEED_UPDATE_COUNTER_SUCCESS: (state, { payload: { counter: { newest_items, all_items }}}) =>
        R.over(LCounter, R.compose(R.assoc('newest', newest_items), R.assoc('total', all_items)), state)

}, initialState)
