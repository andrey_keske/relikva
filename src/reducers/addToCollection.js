import R from 'ramda';
import { createReducer } from '../utils/reducer.js';

import { apiGetSelfCollections, apiAddRelicToCollection } from '../utils/api';

export const loadSelfCollections = (count = 20, page = 1) => ({
    type: 'COLLECTIONS_SELF',
    payload: {
        collections: apiGetSelfCollections(count, page)
    }
});
export const clearSelfCollections = () => ({
    type: 'COLLECTIONS_SELF_CLEAR'
});

const initialState = {
    collections: []
};

const LCollections = R.lensProp('collections');

export default createReducer({
    COLLECTIONS_SELF_CLEAR: (state, action) =>
        R.set(LCollections, [], state),

    COLLECTIONS_SELF_SUCCESS: (state, { payload: { collections: { collections }}}) =>
        R.over(LCollections, R.flip(R.concat)(collections), state),

    COLLECTION_CREATE_SUCCESS: (state, { payload: { collection }}) =>
        R.over(LCollections, R.append(collection), state)
}, initialState)