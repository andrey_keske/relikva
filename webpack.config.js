var path = require('path');
var webpack = require('webpack');

var definePlugin = new webpack.DefinePlugin({
    __PROD__: process.env.NODE_ENV === 'production' || false
});

module.exports = {
    //devtool: '#cheap-module-eval-source-map',
    devtool: 'eval',
    resolve: {
        root: [path.join(__dirname, "bower_components")]
    },
    entry: [
        './src/index'
    ],
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js',
        publicPath: 'http://localhost:8080/static/'
    },
    plugins: [
        new webpack.ResolverPlugin(
            new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin("bower.json", ["main"])
        ),
        new webpack.NoErrorsPlugin(),
        definePlugin
    ],
    module: {
        loaders: [
            {
                test: /\.js$/,
                loaders: ['babel'],
                include: path.join(__dirname, 'src')
            },
            {
                test: /\.scss$/,
                loader: 'style!raw!sass?' +
                [
                    'includePaths[]=' + encodeURIComponent(path.resolve(__dirname, './stylesheets')),
                    'includePaths[]=' + encodeURIComponent(path.resolve(__dirname, './bower_components'))
                ].join('&')
            }
            //{
            //    test: /\.woff(\?v=[0-9]\.[0-9]\.[0-9])?$/,
            //    loader: "url-loader?limit=100000&mimetype=application/font-woff"
            //},
            //{
            //    test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
            //    loader: "file-loader",
            //    exclude: /(assets|images)/
            //},
            //{
            //    test: /\.(png|jpg|svg)$/,
            //    loader: "file-loader",
            //    exclude: /(assets|images)/
            //}
        ]
    }
};
