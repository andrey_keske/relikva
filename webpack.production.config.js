var path = require('path');
var webpack = require('webpack');

var definePlugin = new webpack.DefinePlugin({
    __PROD__: process.env.NODE_ENV === 'production' || true
});

module.exports = {
    resolve: {
        root: [path.join(__dirname, "bower_components")]
    },
    entry: [
        './src/index'
    ],
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    plugins: [
        definePlugin,
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                sequences: true,
                properties: true,
                dead_code: true,
                drop_debugger: true,
                unsafe: false,
                conditionals: true,
                comparisons: true,
                evaluate: true,
                booleans: true,
                loops: true,
                unused: true,
                hoist_funs: true,
                hoist_vars: false,
                if_return: true,
                join_vars: true,
                cascade: true,
                side_effects: true,
                warnings: true
            },
            output: {
                comments: false
            }
        }),
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.DedupePlugin(),
        new webpack.ResolverPlugin(
            new webpack.ResolverPlugin.DirectoryDescriptionFilePlugin("bower.json", ["main"])
        )
    ],
    module: {
        loaders: [
            {
                test: /\.js$/,
                loaders: ['babel'],
                include: path.join(__dirname, 'src')
            },
            {
                test: /\.scss$/,
                loader: 'style!raw!sass?' +
                [
                    'includePaths[]=' + encodeURIComponent(path.resolve(__dirname, './stylesheets')),
                    'includePaths[]=' + encodeURIComponent(path.resolve(__dirname, './bower_components'))
                ].join('&')
            }
        ]
    }
};
