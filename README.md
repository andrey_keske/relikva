# relikva-react

### Установка
`npm install`

### Запуск dev-версии
`npm run dev`  
Добавить `<script src="http://localhost:8080/static/bundle.js"></script>` в шаблон `app/views/react/index.slim`

### Сборка prod-версии
`npm run prod`  
Результат записывается в папку `./dist/`
